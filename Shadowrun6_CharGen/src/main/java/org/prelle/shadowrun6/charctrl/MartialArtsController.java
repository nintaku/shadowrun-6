/**
 *
 */
package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.MartialArts;
import org.prelle.shadowrun6.MartialArtsValue;
import org.prelle.shadowrun6.Technique;
import org.prelle.shadowrun6.TechniqueValue;
import org.prelle.shadowrun6.proc.CharacterProcessor;

/**
 * @author prelle
 *
 */
public interface MartialArtsController extends Controller, CharacterProcessor {

	//-------------------------------------------------------------------
	public List<MartialArts> getAvailableMartialArts();

	//-------------------------------------------------------------------
	public List<Technique> getAvailableTechniques(MartialArtsValue style);

	//-------------------------------------------------------------------
	public boolean canBeSelected(MartialArts data);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(MartialArtsValue data);

	//-------------------------------------------------------------------
	public MartialArtsValue select(MartialArts data);

	//-------------------------------------------------------------------
	public boolean deselect(MartialArtsValue data);


	//-------------------------------------------------------------------
	public boolean canBeSelected(MartialArtsValue learnedIn, Technique data);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(TechniqueValue data);

	//-------------------------------------------------------------------
	public TechniqueValue select(MartialArtsValue learnedIn, Technique data);

	//-------------------------------------------------------------------
	public boolean deselect(TechniqueValue data);

}
