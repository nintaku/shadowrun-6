package org.prelle.shadowrun6.charctrl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.AttributeController.PerAttributePoints;
import org.prelle.shadowrun6.charctrl.AttributeController.RaiseCost;
import org.prelle.shadowrun6.charctrl.AttributeController.RaiseCost.State;
import org.prelle.shadowrun6.chargen.PrioritySettings;

/**
 * @author Stefan Prelle
 *
 */
class AttributePointsOptimizer {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen.attrib");
	
	private ShadowrunCharacter model;
	private Map<Attribute, AttributeController.PerAttributePoints> perAttrib;
	private List<Attribute> metatypeAttribute;
	
	//-------------------------------------------------------------------
	public AttributePointsOptimizer(ShadowrunCharacter model) {
		this.model = model;
		perAttrib = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib;
	}
	
	//--------------------------------------------------------------------
	public void optimize(List<Attribute> racial, int pointsSpecial, int pointsAllowed) {
		metatypeAttribute = racial;
		
//		int pointsToPayWithKarma = (onlyAttrib+onlyAdjust+both) - pointsAllowed - pointsSpecial;
//		logger.debug(String.format("Will need to pay %d points with karma", pointsToPayWithKarma));
		
		/* 
		 * Build a list of raise cost per attribute
		 */
		for (Attribute key : Attribute.primaryAndSpecialValues()) {
			PerAttributePoints per = perAttrib.get(key);
			for (int i=per.base+1; i<=model.getAttribute(key).getGenerationValue(); i++) {
				RaiseCost cost = new RaiseCost(key);
				cost.karma = i*5;
				if (key.isPrimary())
					cost.attrib=1;
				if (key.isSpecial() || metatypeAttribute.contains(key))
					cost.adjust = 1;
				per.listCost.add(cost);
			}
			if (logger.isDebugEnabled())
				logger.debug(String.format("  %9s(%d):", key.name(), model.getAttribute(key).getGenerationValue())+" "+per.listCost);
		}
		
		/*
		 * Sort all attribute values, so that the attributes with the highest
		 * values are first. By doing so priority points are spent for high
		 * attributes (which would be expensive with karma) first.
		 */
		List<AttributeValue> allValues = new ArrayList<>();
		for (Attribute key : Attribute.primaryAndSpecialValues()) {
			allValues.add(model.getAttribute(key));
		}
		Collections.sort(allValues, new Comparator<AttributeValue>() {
			public int compare(AttributeValue o1, AttributeValue o2) {
				return -((Integer)o1.getGenerationValue()).compareTo(o2.getGenerationValue());
			}
		});
		logger.trace("Sorted: "+allValues);
		
		/*
		 * Step 1: Pay adjustment points for MAGIC, RESONANCE, EDGE
		 */
		List<AttributeValue> adjustVals = allValues.stream().filter(v -> ( v.getAttribute().isSpecial() )).collect(Collectors.toList());
		while (pointsSpecial>0) {
			RaiseCost raised = investOneAdjustmentPoint(adjustVals);
			if (raised==null)
				break;
			else
				pointsSpecial--;
		}
		logger.debug("Adjustment points left 1: "+pointsSpecial);
//		dump(Level.DEBUG, "1: After investing adjustment points");
		
		/*
		 * Step 1: Pay adjustment points for MAGIC, RESONANCE, EDGE and metatype attributes
		 */
		adjustVals = allValues.stream().filter(v -> ( v.getAttribute().isSpecial() || metatypeAttribute.contains(v.getAttribute()))).collect(Collectors.toList());
		while (pointsSpecial>0) {
			RaiseCost raised = investOneAdjustmentPoint(adjustVals);
			if (raised==null)
				break;
			else
				pointsSpecial--;
		}
		logger.debug("Adjustment points left 2: "+pointsSpecial);
//		dump(Level.DEBUG, "1: After investing adjustment points");
		
		/*
		 * Step: Pay with attribute points 
		 */
		List<AttributeValue> regularVals = allValues.stream().filter(v -> ( v.getAttribute().isPrimary())).collect(Collectors.toList());
		while (pointsAllowed>0) {
			RaiseCost raised = investOneRegularPoint(regularVals);
			if (raised==null)
				break;
			else
				pointsAllowed--;
		}
		logger.debug("Attribute points left: "+pointsAllowed);
//		dump(Level.DEBUG, "2: After investing attribute points");
		
		/*
		 * Find out how many points are not paid yet.
		 * For every unpaid point, find the cheapest way to redistribute
		 */
		int unpaid = 0;
		for (Attribute key : Attribute.primaryAndSpecialValues()) {
			unpaid += perAttrib.get(key).listCost.stream().filter(cost -> cost.state==State.UNPAID).collect(Collectors.toList()).size();
		}
		logger.debug("Unpaid = "+unpaid);
		
		for (int i=0; i<unpaid; i++) {
			RaiseCost needsPayment = getNextUnpaid();
			// Depending on the attribute (non-metatype attribute, metatype attribute, special)
			// find a cheapest RaiseCost possible
			RaiseCost changeWith = findCheapestKarmaCost(needsPayment.key, needsPayment.karma);
			logger.debug("  change "+changeWith+" with "+needsPayment);
			if (changeWith!=null) {
				needsPayment.state = changeWith.state;
				changeWith.state = State.KARMA;
				logger.debug("   pay "+changeWith.karma+" karma to raise "+changeWith.key+" to "+(changeWith.karma/5));
				model.setKarmaFree( model.getKarmaFree() -changeWith.karma);
			} else {
				needsPayment.state = State.KARMA;
				logger.debug("   pay "+needsPayment.karma+" karma to raise "+needsPayment.key+" to "+(needsPayment.karma/5));
				model.setKarmaFree( model.getKarmaFree() -needsPayment.karma);
			}
		}
		
		// Write values
		int magicBoughtWithKarma = 0;
		for (Attribute key : Attribute.primaryAndSpecialValues()) {
			PerAttributePoints per = perAttrib.get(key);
			per.adjust  = 0;
			per.regular = 0;
			per.karma   = 0;
			for (RaiseCost cost : perAttrib.get(key).listCost) {
				switch (cost.state) {
				case ADJUST: per.adjust++; break;
				case KARMA : 
					if (key==Attribute.MAGIC)
						magicBoughtWithKarma++;
					per.karma+=cost.karma; break;
				case REGULAR: per.regular++; break;
				case UNPAID:
					logger.error("Did not pay this: "+cost);
					System.err.println("Did not pay this: "+cost);
				}
			}
		}
	}
	
	//--------------------------------------------------------------------
	private RaiseCost investOneRegularPoint(List<AttributeValue> regularVals) {
		List<RaiseCost> mostKarma = new ArrayList<>();
		int karmaCostList = 0;
		for (AttributeValue val : regularVals) {
			PerAttributePoints payed = perAttrib.get(val.getAttribute());
			
			for (RaiseCost cost : payed.listCost) {
				if (cost.state != State.UNPAID)
					continue;
				
				if (karmaCostList<cost.karma) {
					// Found something more expensive
					mostKarma.clear();
					mostKarma.add(cost);
					karmaCostList = cost.karma;
				} else if (karmaCostList==cost.karma) {
					// Equally expensive
					mostKarma.add(cost);
				}
				break;
			}			
		}
		
		// From all possible equally expensive attributes to invest a regular point
		// prefer those that cannot be raised with adjustment points
		RaiseCost preferred = mostKarma.isEmpty()?null:mostKarma.get(0);
		for (RaiseCost tmp : mostKarma) {
			if (metatypeAttribute.contains(preferred.key) && !metatypeAttribute.contains(tmp.key))
				preferred = tmp;
		}
		if (logger.isTraceEnabled())
			logger.trace("  i could raise "+mostKarma+" and decide for "+preferred);
		
		if (preferred!=null) {
			preferred.state = State.REGULAR;
			if (logger.isTraceEnabled())
				logger.trace("Pay with attribute point: "+preferred.key);
		}
		return preferred;
	}
	
	//--------------------------------------------------------------------
	private RaiseCost investOneAdjustmentPoint(List<AttributeValue> regularVals) {
		List<RaiseCost> mostKarma = new ArrayList<>();
		int karmaCostList = 0;
		for (AttributeValue val : regularVals) {
			PerAttributePoints payed = perAttrib.get(val.getAttribute());
			
			for (RaiseCost cost : payed.listCost) {
				if (cost.state != State.UNPAID)
					continue;
				
				if (karmaCostList<cost.karma) {
					// Found something more expensive
					mostKarma.clear();
					mostKarma.add(cost);
					karmaCostList = cost.karma;
					break;
				} else if (karmaCostList==cost.karma) {
					// Equally expensive
					mostKarma.add(cost);
					break;
				}
			}			
		}
		
		// From all possible equally expensive attributes pick the first
		RaiseCost preferred = mostKarma.isEmpty()?null:mostKarma.get(0);
		if (preferred!=null) {
			preferred.state = State.ADJUST;
			logger.debug("Pay with adjustment point: "+preferred.key);
		}
		return preferred;
	}
	
	//--------------------------------------------------------------------
	private RaiseCost getNextUnpaid() {
		for (Attribute key : Attribute.primaryAndSpecialValues()) {
			for (RaiseCost cost : perAttrib.get(key).listCost) {
				if (cost.state==State.UNPAID)
					return cost;
			}
		}
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * From every attribute find the 
	 */
	private RaiseCost findCheapestKarmaCost(Attribute attributeToPay, int notMoreThan) {
		RaiseCost cheapest = null;
		// Compare with the highest
		for (Attribute key : Attribute.primaryAndSpecialValues()) {
			// Ignore identical attribute
			if (key==attributeToPay)
				continue;
			RaiseCost cost = perAttrib.get(key).getHighestPaid();
			// Nothing found
			if (cost==null) {
				continue;
			}
			// If it isn't cheaper, don't offer it for exchange
			if (cost.karma>=notMoreThan)
				continue;
			if (cheapest==null || cost.karma<=cheapest.karma) {
				// It would be cheaper.
				// Now check if the type of point it would free, matches the requested point
//				if (attributeToPay.isPrimary() && !metatypeAttribute.contains(attributeToPay) && cost.state!=State.KARMA) { 
//					// cost needs to free a regular point
//					cheapest = cost;
//				} else 
				if (attributeToPay.isSpecial() && cost.state==State.ADJUST) { 
					// cost needs to free an adjustment point
					cheapest = cost;
				} else if (attributeToPay.isPrimary()) {
					cheapest = cost;
				}
				
			}
		}
		return cheapest;
	}

}
