
/**
 * 
 */
package org.prelle.shadowrun6.gen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;

import de.rpgframework.ConfigContainer;
import de.rpgframework.character.RulePlugin;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class CharacterGeneratorRegistry {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen");
	
	private static List<Class<? extends  CharacterGenerator>> generatorClasses;
	private static Map<Class<? extends  CharacterGenerator>, RulePlugin> pluginsByClass;
	private static ConfigContainer addBelow;

	private static transient List<CharacterGenerator> generators;
	private static transient CharacterGenerator selected;
	
	//-------------------------------------------------------------------
	static {
		generatorClasses = new ArrayList<>();
		generators = new ArrayList<CharacterGenerator>();
		pluginsByClass = new HashMap<Class<? extends  CharacterGenerator>, RulePlugin>();
	}
	
	//-------------------------------------------------------------------
	public static void attachConfigurationTree(ConfigContainer value) {
		addBelow = value;
		SR6ConfigOptions.attachConfigurationTree(value);
//		for (CharacterGenerator gen : generators) {
//			logger.info("Call attachConfigurationTree on "+gen);
//			if (addBelow!=null)
//				gen.attachConfigurationTree(addBelow);
//		}
	}

	//-------------------------------------------------------------------
	private static void makeFreshGenerators() {
		logger.warn("Make fresh generators");
		generators.clear();
		
		selected = null;
		for (Class<? extends CharacterGenerator> genClass : generatorClasses) {
			try {
				CharacterGenerator gen = genClass.getConstructor().newInstance();
				gen.attachConfigurationTree(addBelow);
				gen.setPlugin(pluginsByClass.get(genClass));
				generators.add(gen);
//				if (selected==null)
//					selected = gen;
			} catch (Exception e) {
				logger.fatal("Failed instantiating "+genClass,e);
			}
		}
	}

	//-------------------------------------------------------------------
	public static void register(Class<? extends CharacterGenerator> charGen, RulePlugin<?> plugin) {
		for (Class<? extends  CharacterGenerator> tmp : generatorClasses) {
			if (tmp==charGen)
				return;
		}
		
		generatorClasses.add(charGen);
		pluginsByClass.put(charGen, plugin);
	}

	//-------------------------------------------------------------------
	public static List<CharacterGenerator> getGenerators() {
		if (generators.isEmpty())
			makeFreshGenerators();
		return generators;
	}

	//-------------------------------------------------------------------
	public static void select(CharacterGenerator charGen, ShadowrunCharacter model) {
		logger.warn("Switch used generator from "+selected+" to "+charGen);
		
		if (charGen==selected) {
			logger.warn("Keep same generator - model is generation mode = "+model.isGenerationMode());
			
			if (!model.isGenerationMode())
				charGen.continueCreation(model);
			else
				charGen.start(model);
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(
							GenerationEventType.CONSTRUCTIONKIT_CHANGED, 
							charGen));
			return;
		}
		
		CharacterGeneratorRegistry.selected = charGen;
		if (charGen!=null) {
			logger.info(charGen+" as generator selected");
			if (!model.isGenerationMode()) {
				logger.debug("Call "+charGen.getClass()+".continueCreation");
				charGen.continueCreation(model);
			} else {
				logger.debug("Call "+charGen.getClass()+".start");
				charGen.start(model);
			}
			
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(
							GenerationEventType.CONSTRUCTIONKIT_CHANGED, 
							charGen));
			logger.warn("------4--------------");
			logger.warn("getSelected() returns "+CharacterGeneratorRegistry.getSelected());
		} else {
			throw new NullPointerException("Generator set to NULL");
		}
	}

	//-------------------------------------------------------------------
	public static void selectAndContinue(CharacterGenerator charGen, ShadowrunCharacter model) {
		if (charGen==null)
			throw new NullPointerException("Generator may not be null");
		selected = charGen;
		if (charGen!=null) {
			logger.info("Continue "+model.getName()+" with "+charGen.getClass()+" as generator "+charGen);
			logger.info("Settings are: "+model.getChargenSettings());
			logger.debug("Call "+charGen.getClass()+".continueCreation");
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(
							GenerationEventType.CONSTRUCTIONKIT_CHANGED, 
							charGen));
			charGen.continueCreation(model);
		}
	}

	//--------------------------------------------------------------------
	public static CharacterGenerator getSelected() {
		logger.warn("RETURN "+selected);
		return selected;
	}

	//--------------------------------------------------------------------
	public static CharacterController get(String chargenUsed) {
		makeFreshGenerators();
		for (CharacterGenerator gen : generators) {
			if (gen.getID().equalsIgnoreCase(chargenUsed))
				return gen;
		}
		return null;
	}

}
