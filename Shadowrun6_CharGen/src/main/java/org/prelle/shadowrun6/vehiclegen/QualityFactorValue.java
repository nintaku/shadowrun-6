package org.prelle.shadowrun6.vehiclegen;

import org.prelle.shadowrun6.vehicle.QualityFactor;

/**
 * @author prelle
 *
 */
public class QualityFactorValue {
	
	private QualityFactor ref;

	//-------------------------------------------------------------------
	public QualityFactorValue(QualityFactor mod) {
		if (mod==null)
			throw new NullPointerException();
		this.ref = mod;
	}

	//-------------------------------------------------------------------
	public QualityFactor getQualityFactor() {
		return ref;
	}

	//-------------------------------------------------------------------
	public float getMultiplier() {
		return ref.getMultiplier();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return ref.getName();
	}

}
