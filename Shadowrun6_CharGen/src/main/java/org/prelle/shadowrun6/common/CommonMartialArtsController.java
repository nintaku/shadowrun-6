package org.prelle.shadowrun6.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MartialArts;
import org.prelle.shadowrun6.MartialArtsValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Technique;
import org.prelle.shadowrun6.Technique.Category;
import org.prelle.shadowrun6.TechniqueValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.MartialArtsController;
import org.prelle.shadowrun6.levelling.MartialArtsLeveller;
import org.prelle.shadowrun6.modifications.MartialArtsModification;
import org.prelle.shadowrun6.modifications.TechniqueModification;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CommonMartialArtsController implements MartialArtsController {

	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen.martial");

	protected CharacterController parent; 
	protected ShadowrunCharacter model;

	protected List<ToDoElement> todos;
	protected List<MartialArts> available;

	//-------------------------------------------------------------------
	/**
	 */
	public CommonMartialArtsController(CharacterController parent) {
		this.parent = parent;
		this.model = parent.getCharacter();
		todos      = new ArrayList<>();
		available  = new ArrayList<MartialArts>();
		
		updateAvailable();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<ConfigOption<?>>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	protected void updateAvailable() {
		available.clear();
		available.addAll(ShadowrunTools.filterByPluginSelection(ShadowrunCore.getMartialArt(),model));
		if (model!=null) {
			for (MartialArtsValue val : model.getMartialArts()) {
				available.remove(val.getMartialArt());
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Get a list of all known martial arts, remove those that are already
	 * selected and those where requirements are not met yet
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#getAvailableMartialArts()
	 */
	@Override
	public List<MartialArts> getAvailableMartialArts() {		
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#getAvailableTechniques(org.prelle.shadowrun6.MartialArtsValue)
	 */
	@Override
	public List<Technique> getAvailableTechniques(MartialArtsValue style) {
		List<Technique> ret = new ArrayList<Technique>();
		// Add all theoretically possible
		for (Technique tech : ShadowrunTools.filterByPluginSelection(ShadowrunCore.getTechniques(),model)) {
			if (tech==style.getMartialArt().getSignatureTechnique())
				continue;
			for (Technique.Category cat : tech.getCategories()) {
				if (style.getMartialArt().getCategories().contains(cat) || cat==Category.GENERAL) {
					// match by category
					ret.add(tech);
				}
			}
		}

		// Now remove those already learned
		for (Technique tmp : new ArrayList<Technique>(ret)) {
			if (model.hasTechnique(tmp))
				ret.remove(tmp);
		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#canBeSelected(org.prelle.shadowrun6.MartialArts)
	 */
	@Override
	public boolean canBeSelected(MartialArts data) {
		// Must be available
		if (!available.contains(data))
			return false;
		
		// Requires karma 
		if (model.getKarmaFree()<7)
			return false;
		
		// Requires money
		if (this instanceof MartialArtsLeveller) {
			boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
			if (payGear && model.getNuyen()<2500)
				return false;
		}
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#canBeDeselected(org.prelle.shadowrun6.MartialArtsValue)
	 */
	@Override
	public boolean canBeDeselected(MartialArtsValue data) {
		return model.getMartialArts().contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#select(org.prelle.shadowrun6.MartialArts)
	 */
	@Override
	public MartialArtsValue select(MartialArts data) {
		if (!canBeSelected(data))
			return null;
		
		MartialArtsValue val = new MartialArtsValue(data);
		model.addMartialArt(val);
		
		return val;
	}

	//-------------------------------------------------------------------
	protected MartialArtsModification getModification(MartialArtsValue key) {
		MartialArtsModification ret = null;
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof MartialArtsModification))
				continue;
			MartialArtsModification amod = (MartialArtsModification)mod;
			if (amod.getMartialArts()!=key.getMartialArt())
				continue;

			if (mod.getSource()!=this && mod.getSource()!=null)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	protected TechniqueModification getModification(TechniqueValue key) {
		TechniqueModification ret = null;
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof TechniqueModification))
				continue;
			TechniqueModification amod = (TechniqueModification)mod;
			if (amod.getTechnique()!=key.getTechnique())
				continue;

			if (mod.getSource()!=this && mod.getSource()!=null)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#deselect(org.prelle.shadowrun6.MartialArtsValue)
	 */
	@Override
	public boolean deselect(MartialArtsValue data) {
		if (!canBeDeselected(data))
			return false;
		model.removeMartialArt(data);
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#canBeSelected(org.prelle.shadowrun6.Technique)
	 */
	@Override
	public boolean canBeSelected(MartialArtsValue learnIn, Technique data) {
		if (data==null)
			return false;
		// Must be available
		if (!getAvailableTechniques(learnIn).contains(data))
			return false;
		
		// Requires karma 
		if (model.getKarmaFree()<5)
			return false;
		
		// Requires money 
		if (this instanceof MartialArtsLeveller) {
			boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
			if (payGear && model.getNuyen()<1500)
				return false;
		}
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#canBeDeselected(org.prelle.shadowrun6.TechniqueValue)
	 */
	@Override
	public boolean canBeDeselected(TechniqueValue data) {
		return model.getTechniques().contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#select(org.prelle.shadowrun6.Technique)
	 */
	@Override
	public TechniqueValue select(MartialArtsValue learnIn, Technique data) {
		if (!canBeSelected(learnIn, data))
			return null;
		
		logger.warn("Select "+data+" in style "+learnIn);
		logger.warn("Select "+data.getId()+" in style "+learnIn.getMartialArt());
		TechniqueValue val = new TechniqueValue(data, learnIn.getMartialArt());
		model.addTechnique(val);
		
		return val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#deselect(org.prelle.shadowrun6.TechniqueValue)
	 */
	@Override
	public boolean deselect(TechniqueValue data) {
		if (canBeDeselected(data)) {
			model.removeTechnique(data);
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		this.model = model;
		try {			
			// Clear all attribute modifications in character
			todos.clear();
			updateAvailable();
		} finally {
			
		}
		return unprocessed;
	}

}
