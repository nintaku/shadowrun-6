/**
 * 
 */
package org.prelle.shadowrun6.common;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.charctrl.LifestyleController;
import org.prelle.shadowrun6.chargen.cost.LifestyleCostCalculator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.modifications.LifestyleCostModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public abstract class CommonLifestyleController implements LifestyleController, CharacterProcessor {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen");
	protected EquipmentController equip;
	protected ShadowrunCharacter model;
	protected CharacterController parent;
	
	protected LifestyleCostCalculator costs;
	
	//-------------------------------------------------------------------
	public CommonLifestyleController(CharacterController parent) {
		this.parent = parent;
		this.model = parent.getCharacter();
		this.equip = parent.getEquipmentController();
		costs = new LifestyleCostCalculator();
	}

	//-------------------------------------------------------------------
	public LifestyleCostCalculator getCostCalculator() {
		return costs;
	}

	//-------------------------------------------------------------------
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		List<ToDoElement> ret = new ArrayList<ToDoElement>();
		if (model.getLifestyle().isEmpty())
			ret.add(new ToDoElement(Severity.WARNING, Resource.get(RES, "lifestylegen.todo.missing_primary_lifestyle")));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#addLifestyle(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void addLifestyle(LifestyleValue ref) {
		model.addLifestyle(ref);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#removeLifestyle(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void removeLifestyle(LifestyleValue lifestyle) {
		model.removeLifestyle(lifestyle);
		
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#canIncreaseMonths(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public boolean canIncreaseMonths(LifestyleValue data) {		
		// Is it affordable?
		if (data.getCostPerMonth()>model.getNuyen())
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#canDecreaseMonths(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public boolean canDecreaseMonths(LifestyleValue data) {
		// Not below 1
		if (data.getPaidMonths()<=1)
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#increaseMonths(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void increaseMonths(LifestyleValue data) {
		logger.debug("increaseMonths");
		if (!canIncreaseMonths(data))
			return;
		
		data.setPaidMonths(data.getPaidMonths()+1);

		int cost = data.getCostPerMonth();
		model.setNuyen(model.getNuyen() - cost);
		logger.info("Lifestyle "+data.getName()+" increased to "+data.getPaidMonths()+" months for "+cost+" nuyen");
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.LIFESTYLE_CHANGED, data));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, new int[]{model.getNuyen(), -1}));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#decreaseMonths(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void decreaseMonths(LifestyleValue data) {
		if (!canDecreaseMonths(data))
			return;
		
		data.setPaidMonths(data.getPaidMonths()-1);

		int cost = data.getCostPerMonth();
		model.setNuyen(model.getNuyen() + cost);
		logger.info("Lifestyle "+data.getName()+" decreased to "+data.getPaidMonths()+" months for "+cost+" nuyen");
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.LIFESTYLE_CHANGED, data));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, new int[]{model.getNuyen(), -1}));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#getLifestyleCost(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public int getLifestyleCost(LifestyleValue lifestyle) {
		int percentToAdd = 0;
		int nuyenToAdd = 0;
		// Check for lifestyle multipliers
		for (LifestyleCostModification mod : costs.getLifestyleModifications()) {
				percentToAdd += mod.getPercent();
				nuyenToAdd   += mod.getFixed();
		}
		/*
		 * Include modifications
		 */
		int total = lifestyle.getCost() + nuyenToAdd + (lifestyle.getCost()*percentToAdd/100);
		return total;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		
		ArrayList<Modification> unprocessed = new ArrayList<>();
		try {
			/*
			 * Clear all modifications on lifestyles
			 */
			model.getLifestyle().forEach(life -> life.clearModification());
			
			for (Modification tmp : previous) {
				if (tmp instanceof LifestyleCostModification) {
					LifestyleCostModification mod = (LifestyleCostModification)tmp;
					logger.debug("process lifestyle mod: "+mod);
					List<LifestyleValue> lifes = (mod.getSIN()!=null)? model.getLifestyles(model.getSIN(mod.getSIN())):model.getLifestyle();
					logger.debug("Found "+lifes.size()+" lifestyles");
					for (LifestyleValue life : lifes) {
						life.addModification(mod);
						logger.info("Added modification "+mod+" to lifestyle "+life);
					}
				} else {
					unprocessed.add(tmp);
				}
			}
			
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
