/**
 *
 */
package org.prelle.shadowrun6.chargen.proc;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class LatePlausabilityCheck implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen.attrib");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR6CharacterGenerator parent;

	//-------------------------------------------------------------------
	/**
	 */
	public LatePlausabilityCheck(CommonSR6CharacterGenerator parent) {
		this.parent = parent;
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		try {
			/*
			 * Special test: Impaired only possible to a level, that maximum attribute is 2
			 */
			for (QualityValue val : model.getQualities()) {
				if (!val.getModifyable().getId().equals("impaired"))
					continue;
				Attribute attr = (Attribute) val.getChoice();
				if (attr!=null && model.getAttribute(attr).getMaximum()<2) {
					parent.getQualityController().addToDo(new ToDoElement(Severity.STOPPER, Resource.format(RES, "priogen.todo.qualities.impaired_too_high", attr.getName())));
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
