package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MartialArts;
import org.prelle.shadowrun6.MartialArtsValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.Technique;
import org.prelle.shadowrun6.TechniqueValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.MartialArtsController;
import org.prelle.shadowrun6.common.CommonMartialArtsController;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class MartialArtsGenerator extends CommonMartialArtsController implements MartialArtsController {

	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen.martial");

	//-------------------------------------------------------------------
	/**
	 */
	public MartialArtsGenerator(CharacterController parent) {
		super(parent);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<ConfigOption<?>>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#select(org.prelle.shadowrun6.MartialArts)
	 */
	@Override
	public MartialArtsValue select(MartialArts data) {
		MartialArtsValue val = super.select(data);
		if (val==null) {
			logger.warn("Attempt to select martial art, which is not selectable");
			return null;
		}
		
		logger.info("Selected martial art style "+data.getId());
		parent.runProcessors();
		
		return val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#deselect(org.prelle.shadowrun6.MartialArtsValue)
	 */
	@Override
	public boolean deselect(MartialArtsValue data) {
		if (!super.deselect(data)) {
			logger.warn("Attempt to deselect martial art, which is not deselectable");
			return false;
		}
		
		logger.info("Deselected martial art style "+data.getMartialArt().getId());
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#select(org.prelle.shadowrun6.Technique)
	 */
	@Override
	public TechniqueValue select(MartialArtsValue learnIn, Technique data) {
		TechniqueValue val = super.select(learnIn, data);
		if (val==null) {
			logger.warn("Attempt to select technique in a martial art style, which is not selectable");
			return null;
		}
		
		logger.info("Selected technique '"+data.getId()+"' in martial art style '"+learnIn.getMartialArt().getId()+"'");
		parent.runProcessors();
		
		return val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MartialArtsController#deselect(org.prelle.shadowrun6.TechniqueValue)
	 */
	@Override
	public boolean deselect(TechniqueValue data) {
		if (!super.deselect(data)) {
			logger.warn("Attempt to deselect technique in a martial art style, which is not deselectable");
			return false;
		}
		
		logger.info("Deselected technique '"+data.getTechnique().getId()+"' in martial art style '"+data.getMartialArt().getId()+"'");
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		this.model = model;
		try {			
			// Clear all attribute modifications in character
			todos.clear();
			updateAvailable();
			
			// Pay karma and nuyen
			for (MartialArtsValue style : model.getMartialArts()) {
//				model.setNuyen(model.getNuyen()  - 2500);
				model.setKarmaFree(model.getKarmaFree() -7);
				logger.debug("pay 7 Karma for martial arts style '"+style.getMartialArt().getId()+"'");
			}
			for (TechniqueValue style : model.getTechniques()) {
//				model.setNuyen(model.getNuyen()  - 1500);
				model.setKarmaFree(model.getKarmaFree() -5);
				logger.debug("pay 15 Karma for technique '"+style.getTechnique().getId()+"'");
			}
			
		} finally {
			
		}
		return unprocessed;
	}

}
