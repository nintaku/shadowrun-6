package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun6.MetamagicOrEcho;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan Prelle
 *
 */
public class CommonMetamagicOrEchoGenerator extends CommonMetaMagicOrEchoController {

	public CommonMetamagicOrEchoGenerator(CommonSR6CharacterGenerator parent) {
		super(parent, CharGenMode.CREATING);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			todos.clear();
			
			// Pay metamagics / echoes with karma
			int cost = 11;
			for (MetamagicOrEchoValue tmp : model.getMetamagicOrEchoes()) {
				MetamagicOrEcho real = tmp.getModifyable();
				if (real.hasLevels()) {
					for (int i=1; i<=tmp.getLevel(); i++) {
						logger.info("Pay "+cost+" karma for "+real.getName()+" "+i);
						model.setKarmaFree(model.getKarmaFree()-cost);
						cost++;
					}
				} else {
					logger.info("Pay "+cost+" karma for "+tmp.getName());
					model.setKarmaFree(model.getKarmaFree()-cost);
					cost++;
				}
			}
			
			updateAvailable();
			logger.debug("Available metamagic/echoes = "+available);
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
