/**
 * 
 */
package org.prelle.shadowrun6.chargen.cost;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaMagicOrResonanceCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");

	//-------------------------------------------------------------------
	public KarmaMagicOrResonanceCostCalculator() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		
		try {		
			if (model.getMagicOrResonanceType()!=null) {
				int cost = model.getMagicOrResonanceType().getCost();
				logger.info("Pay "+cost+" karma for MagicOrResonance option "+model.getMetatype());
				
				model.setKarmaFree( model.getKarmaFree() - cost);
			}

		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
