package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.modifications.KarmaModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplyKarmaModifications implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen.karma");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process Modifications
			for (Modification tmp : previous) {
				if (tmp instanceof KarmaModification) {
					KarmaModification mod = (KarmaModification)tmp;
					int karma = mod.getValue();
					logger.info("Apply "+karma+" Karma from "+tmp.getSource());
				} else {
					unprocessed.add(tmp);
				}
			} 
		} catch (Exception e) {
			logger.error("Failed on modifications",e);
		}
		return unprocessed;
	}

}
