/**
 * @author Stefan Prelle
 *
 */
module shadowrun6.chargen {
	exports org.prelle.shadowrun6.chargen.cost;
	exports org.prelle.shadowrun6.chargen.proc;
	exports org.prelle.shadowrun6.chargen;
	exports org.prelle.shadowrun6.common;
	exports org.prelle.shadowrun6.charctrl;
	exports org.prelle.shadowrun6.gen;
	exports org.prelle.shadowrun6.gen.event;
	exports org.prelle.shadowrun6.levelling;
	exports org.prelle.shadowrun6.vehiclegen;
	
	opens org.prelle.shadowrun6.chargen to com.google.gson;

	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.chars;
	requires transitive shadowrun6.core;
	requires shadowrun6.data;
	requires simple.persist;
	requires java.prefs;
	requires com.google.gson;
}