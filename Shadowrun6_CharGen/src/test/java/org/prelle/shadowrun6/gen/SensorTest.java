package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOptionType;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.common.CommonEquipmentController;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;

import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SensorTest {

	private CommonEquipmentController control;
	private ShadowrunCharacter model;
	private List<Modification> mods;
	private int baseKarma;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		model.setNuyen(999999);
		mods = new ArrayList<>();
		ProcessorRunner charGen = new ProcessorRunner() {
			public void runProcessors() {
				model.setKarmaFree(baseKarma);
			}
		};
		model.getAttribute(Attribute.MAGIC).clearModifications();
		control = new CommonEquipmentController(charGen, model, CharGenMode.CREATING) {
			
			@Override
			public List<ConfigOption<?>> getConfigOptions() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean increaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public int getBoughtNuyen() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public boolean decreaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean canIncreaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean canDecreaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
		};
	}

	//-------------------------------------------------------------------
	@Test
	public void testSensorLimitation() {
		ItemTemplate template = ShadowrunCore.getItem("wall-mounted_housing");
		assertNotNull(template);
		CarriedItem housing = control.select(template, new SelectionOption(SelectionOptionType.RATING, 6));
		assertNotNull(housing);
		assertEquals(1500, housing.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(4, housing.getAsValue(ItemAttribute.MAX_SENSOR_RATING).getModifiedValue());
		
		List<ItemTemplate> embeddable = control.getEmbeddableIn(housing, ItemHook.SENSOR_HOUSING);
		assertNotNull(embeddable); 
		assertTrue(embeddable.contains(ShadowrunCore.getItem("single_sensor"))); 
		assertTrue(embeddable.contains(ShadowrunCore.getItem("sensor_array"))); 
		
		// Max sensor rating is 4, so the  maximum rating of a single sensor also 4
		ItemTemplate single = ShadowrunCore.getItem("single_sensor");
		assertTrue(control.canBeEmbedded(housing, single, ItemHook.SENSOR_HOUSING, new SelectionOption(SelectionOptionType.RATING, 4)));
		assertFalse(control.canBeEmbedded(housing, single, ItemHook.SENSOR_HOUSING, new SelectionOption(SelectionOptionType.RATING, 5)));
		assertFalse(control.canBeEmbedded(housing, single, ItemHook.SENSOR_HOUSING, new SelectionOption(SelectionOptionType.RATING, 6)));
		assertFalse(control.canBeEmbedded(housing, single, ItemHook.SENSOR_HOUSING, new SelectionOption(SelectionOptionType.RATING, 7)));
		assertNull(control.embed(housing, single, new SelectionOption(SelectionOptionType.RATING, 6)));
		CarriedItem sensor = control.embed(housing, single, new SelectionOption(SelectionOptionType.RATING, 4));
		assertNotNull(sensor);
		// Limit should carry over
		assertEquals(4, sensor.getAsValue(ItemAttribute.MAX_SENSOR_RATING).getModifiedValue());
		
		// Now try to add sensor function to single sensor
		ItemTemplate camera = ShadowrunCore.getItem("camera_function");
		ItemTemplate atmos  = ShadowrunCore.getItem("atmosphere_sensor");
		assertTrue (control.canBeEmbedded(sensor, atmos, ItemHook.SENSOR_FUNCTION));
		assertTrue (control.canBeEmbedded(sensor, camera, ItemHook.SENSOR_FUNCTION));
		// Should be possible
		System.out.println("----------------");
		CarriedItem funct = control.embed(sensor, camera);
		assertNotNull(funct);
		assertEquals(4, housing.getAsValue(ItemAttribute.MAX_SENSOR_RATING).getModifiedValue());
		assertEquals(4, funct.getSlot(ItemHook.OPTICAL).getCapacity(), 0);

		//		ItemRecalculation.recalculate(null, housing);
		assertEquals(4, sensor.getAsValue(ItemAttribute.MAX_SENSOR_RATING).getModifiedValue());
		assertEquals(4,funct.getRating());
		// After one sensor is embedded, another one should not be possible for single sensor
		assertFalse(control.canBeEmbedded(sensor, atmos, ItemHook.SENSOR_FUNCTION));		
		CarriedItem funct2 = control.embed(sensor, atmos);
		assertNull(funct2);
		
		// Now remove camera and add atmosphere sensor
		assertTrue(control.remove(sensor, funct));
		assertTrue(control.canBeEmbedded(sensor, atmos, ItemHook.SENSOR_FUNCTION));		

		funct2 = control.embed(sensor, atmos);
		assertNotNull(funct2);
		assertEquals(4,funct2.getRating());
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void testExample1() {
//		ItemTemplate template = ShadowrunCore.getItem("wall-mounted_housing");
//		assertNotNull(template);
//		CarriedItem housing = control.select(template, new SelectionOption(SelectionOptionType.RATING, 6));
//		assertNotNull(housing);
//		assertEquals(1500, housing.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		assertEquals(4, housing.getAsValue(ItemAttribute.MAX_SENSOR_RATING).getModifiedValue());
//		
//		List<ItemTemplate> embeddable = control.getEmbeddableIn(housing, ItemHook.SENSOR_HOUSING);
//		assertNotNull(embeddable); 
//		assertTrue(embeddable.contains(ShadowrunCore.getItem("single_sensor"))); 
//		assertTrue(embeddable.contains(ShadowrunCore.getItem("sensor_array"))); 
//		
//		// Max sensor rating is 4, but adding a higher single sensor should still work
//		ItemTemplate single = ShadowrunCore.getItem("single_sensor");
//		CarriedItem single1 = control.embed(housing, single, new SelectionOption(SelectionOptionType.RATING, 6));
//		CarriedItem single2 = control.embed(housing, single, new SelectionOption(SelectionOptionType.RATING, 6));
//	}
	
}
