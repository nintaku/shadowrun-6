package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.VehicleData;
import org.prelle.shadowrun6.vehiclegen.DesignModValue;
import org.prelle.shadowrun6.vehiclegen.DesignOptionValue;
import org.prelle.shadowrun6.vehiclegen.VehicleGenerator;
import org.prelle.simplepersist.Persister;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class VehicleGenTest {

	private VehicleGenerator control;
	private ShadowrunCharacter model;
	private List<Modification> mods;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		control = new VehicleGenerator();
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() {
		assertEquals(0, control.getBuildPoints());
	}

	//-------------------------------------------------------------------
	@Test
	public void testExample1() {
		// First thing he does is go through the AR menu and puts
		// together the options he wants. He starts with picking a basic car
		// chassis (7 build points).
		control.selectChassis(ShadowrunCore.getChassisType("car"));
		assertEquals(7, control.getBuildPoints());
		// Next he goes with a standard wheeled power train, since there 
		// is no need for something fancy (10 build points)
		control.selectPowertrain(ShadowrunCore.getPowertrain("wheeled"));
		assertEquals(17, control.getBuildPoints());
		// Johnny wants better sensors so goes for the intermediate
		// console package (5 points).
		control.selectConsole(ShadowrunCore.getConsoleType("intermediate"));
		assertEquals(22, control.getBuildPoints());
		
		ItemTemplate model = control.getVehicle();
		assertNotNull(model);
		assertEquals(ItemType.VEHICLES, model.getDefaultUsage().getType());
		assertEquals(ItemSubType.CARS, model.getDefaultUsage().getSubtype());
		VehicleData vehicle = model.getVehicleData();
		assertNotNull(vehicle);
		
		/**
		 * This means his new car has the following baseline attributes: 
		 * Body 10, Armor 4, Handling 3, Seating 4, Size Class 6, 
		 * Acceleration 15, Speed Interval 20, and Top Speed 250.
		 */
		assertEquals(10, vehicle.getBody());
		assertEquals( 4, vehicle.getArmor());
		assertEquals( 3, vehicle.getHandling().getOnRoad());
		assertEquals( 3, vehicle.getHandling().getOffRoad());
		assertEquals( 4, vehicle.getSeats());
		//assertEquals( 6, size);
		//assertEquals(15, vehicle.getAcceleration().getOnRoad());
		assertEquals(20, vehicle.getSpeedInterval().getOnRoad());
		//assertEquals(250, vehicle.getTopSpeed());
		
		// ...so he decides to lighten up the chassis by reducing the 
		// Body by 1 point (reduces build points by 4)
		DesignOptionValue doVal = control.selectDesignOption(ShadowrunCore.getDesignOption("improved_body"),-1);
		assertNotNull(doVal);
		assertEquals(-1, doVal.getLevel());
		assertEquals(-4, doVal.getCost());
		assertNotNull(doVal.getDesignOption());
		assertEquals("improved_body", doVal.getDesignOption().getId());
		assertEquals(9, vehicle.getBody());
		assertEquals(18, control.getBuildPoints());

		// improved speed interval (2 levels for 10 Build Points),
		doVal = control.selectDesignOption(ShadowrunCore.getDesignOption("improved_speed_interval"),2);
		assertNotNull(doVal);
		assertEquals(10, doVal.getCost());
		// and a racing engine (25 Build Points).
		DesignModValue dmVal = control.selectDesignMod(ShadowrunCore.getDesignMod("racing_engines"));
		assertNotNull(dmVal);
		assertNotNull(dmVal.getDesignMod());
		assertEquals(25, dmVal.getCost());
		assertEquals("racing_engines", dmVal.getDesignMod().getId());
		assertEquals(9, vehicle.getBody());
		
		// All that makes the final Build Point Value 53.
		assertEquals(53, control.getBuildPoints());

		// Final calculations
		assertEquals( 9, vehicle.getBody());
		assertEquals( 4, vehicle.getArmor());
		/* 
		 * The speed modifier is (9/2+4) = 8, which drops the base 
		 * Acceleration to 11 and the Top Speed to 160.
		 * The racing engine brings that back up to 17 and 260
		 */
		assertEquals(17, vehicle.getAcceleration().getOnRoad());
		assertEquals(17, vehicle.getAcceleration().getOffRoad());
		assertEquals(270, vehicle.getTopSpeed()); // 250 -8x10 = 170 + 100 = 270

		assertEquals( 3, vehicle.getHandling().getOnRoad());
		assertEquals( 3, vehicle.getHandling().getOffRoad());
		assertEquals( 4, vehicle.getSeats());
		assertEquals( 6, vehicle.getSize());
		// The adjusted Speed Interval is 30
		assertEquals(30, vehicle.getSpeedInterval().getOnRoad());
		assertEquals(30, vehicle.getSpeedInterval().getOffRoad());
		
		Persister persist = new Persister();
		try {
			persist.write(model, System.out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Now create an CarriedItem to add regular modifications
		 */
		CarriedItem item = new CarriedItem(model);
//		// Johnny decides he needs 2 standard weapon mounts just in case,
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("weapon_mount_standard"));
//		assertNotNull(doVal);
//		assertEquals(2, doVal.getCost());
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("weapon_mount_standard"));
//		// and body contour (distinctive features modification, no build point 
//		// or mod slot cost but adds 4,000 nuyen to the final cost)
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("distinctive_features"));
//		assertNotNull(doVal);
//		assertEquals(2, doVal.getCost());
//		// run-flat racing tires (1,800 nuyen)
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("tires_large_racing_rf"));
//		assertNotNull(doVal);
//		assertEquals(2, doVal.getCost());
//		// a spoof kit (1,500 nuyen)
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("spoof_kit"));
//		assertNotNull(doVal);
//		assertEquals(2, doVal.getCost());

	}

	//-------------------------------------------------------------------
	@Test
	public void testExample2() {
		// First thing he does is go through the AR menu and puts
		// together the options he wants. He starts with picking a basic car
		// chassis (7 build points).
		control.selectChassis(ShadowrunCore.getChassisType("vtol_vstol"));
		assertEquals(15, control.getBuildPoints());
		// Next he goes with a standard wheeled power train, since there 
		// is no need for something fancy (10 build points)
		control.selectPowertrain(ShadowrunCore.getPowertrain("vectored_thrust"));
		assertEquals(115, control.getBuildPoints());
		// Johnny wants better sensors so goes for the intermediate
		// console package (5 points).
		control.selectConsole(ShadowrunCore.getConsoleType("expert"));
		assertEquals(123, control.getBuildPoints());
		
		ItemTemplate model = control.getVehicle();
		assertNotNull(model);
		assertEquals(ItemType.VEHICLES, model.getDefaultUsage().getType());
		assertEquals(ItemSubType.VTOL, model.getDefaultUsage().getSubtype());
		VehicleData vehicle = model.getVehicleData();
		assertNotNull(vehicle);
		
		/**
		 * This means his new car has the following baseline attributes: 
		 * Body 10, Armor 4, Handling 3, Seating 4, Size Class 6, 
		 * Acceleration 15, Speed Interval 20, and Top Speed 250.
		 */
		assertEquals(14, vehicle.getBody());
		assertEquals(12, vehicle.getArmor());
		assertEquals( 4, vehicle.getHandling().getOnRoad());
		assertEquals( 4, vehicle.getHandling().getOffRoad());
		assertEquals( 2, vehicle.getSeats());
		assertEquals( 8, vehicle.getSize());
		assertEquals(30, vehicle.getAcceleration().getOffRoad());
		assertEquals(60, vehicle.getSpeedInterval().getOnRoad());
		assertEquals(310, vehicle.getTopSpeed());
		
		DesignModValue dmVal = control.selectDesignMod(ShadowrunCore.getDesignMod("high_performance_engines"));
		assertNotNull(dmVal);
		assertEquals(0, dmVal.getLevel());
		assertEquals(15, dmVal.getCost());
		assertEquals(360, vehicle.getTopSpeed());
		assertEquals(138, control.getBuildPoints());

		dmVal = control.selectDesignMod(ShadowrunCore.getDesignMod("multi_engine"));
		assertNotNull(dmVal);
		assertNotNull(dmVal.getDesignMod());
		assertEquals(25, dmVal.getCost());
		assertEquals("multi_engine", dmVal.getDesignMod().getId());
		assertEquals(163, control.getBuildPoints());
		assertEquals(720, vehicle.getTopSpeed());
		
		Persister persist = new Persister();
		try {
			persist.write(model, System.out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Now create an CarriedItem to add regular modifications
		 */
		CarriedItem item = new CarriedItem(model);
//		// Johnny decides he needs 2 standard weapon mounts just in case,
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("weapon_mount_standard"));
//		assertNotNull(doVal);
//		assertEquals(2, doVal.getCost());
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("weapon_mount_standard"));
//		// and body contour (distinctive features modification, no build point 
//		// or mod slot cost but adds 4,000 nuyen to the final cost)
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("distinctive_features"));
//		assertNotNull(doVal);
//		assertEquals(2, doVal.getCost());
//		// run-flat racing tires (1,800 nuyen)
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("tires_large_racing_rf"));
//		assertNotNull(doVal);
//		assertEquals(2, doVal.getCost());
//		// a spoof kit (1,500 nuyen)
//		doVal = control.selectModificationAsDesignOption(ShadowrunCore.getItem("spoof_kit"));
//		assertNotNull(doVal);
//		assertEquals(2, doVal.getCost());

	}

	//-------------------------------------------------------------------
	@Test
	public void testNegativeMods() {
		// First thing he does is go through the AR menu and puts
		// together the options he wants. He starts with picking a basic car
		// chassis (7 build points).
		control.selectChassis(ShadowrunCore.getChassisType("vtol_vstol"));
		assertEquals(15, control.getBuildPoints());
		// Next he goes with a standard wheeled power train, since there 
		// is no need for something fancy (10 build points)
		control.selectPowertrain(ShadowrunCore.getPowertrain("vectored_thrust"));
		assertEquals(115, control.getBuildPoints());
		// Johnny wants better sensors so goes for the intermediate
		// console package (5 points).
		control.selectConsole(ShadowrunCore.getConsoleType("expert"));
		assertEquals(123, control.getBuildPoints());
		
		ItemTemplate model = control.getVehicle();
		assertNotNull(model);
		assertEquals(ItemType.VEHICLES, model.getDefaultUsage().getType());
		assertEquals(ItemSubType.VTOL, model.getDefaultUsage().getSubtype());
		VehicleData vehicle = model.getVehicleData();
		assertNotNull(vehicle);
		
		/**
		 * This means his new car has the following baseline attributes: 
		 * Body 10, Armor 4, Handling 3, Seating 4, Size Class 6, 
		 * Acceleration 15, Speed Interval 20, and Top Speed 250.
		 */
		assertEquals(14, vehicle.getBody());
		assertEquals(12, vehicle.getArmor());
		assertEquals( 4, vehicle.getHandling().getOnRoad());
		assertEquals( 4, vehicle.getHandling().getOffRoad());
		assertEquals( 2, vehicle.getSeats());
		assertEquals( 8, vehicle.getSize());
		assertEquals(30, vehicle.getAcceleration().getOffRoad());
		assertEquals(60, vehicle.getSpeedInterval().getOnRoad());
		assertEquals(310, vehicle.getTopSpeed());
		assertEquals(123, control.getBuildPoints());
		
		DesignModValue dmVal = control.selectDesignMod(ShadowrunCore.getDesignMod("light_weight"));
		assertNotNull(dmVal);
		assertEquals(113, control.getBuildPoints());
		assertEquals(7, vehicle.getBody());
		// Undo
		control.deselect(dmVal);
		assertEquals(123, control.getBuildPoints());
		assertEquals(14, vehicle.getBody());

		// Try Cargo Hauler
		dmVal = control.selectDesignMod(ShadowrunCore.getDesignMod("cargo_hauler"));
		assertNotNull(dmVal);
		assertEquals(143, control.getBuildPoints());
		assertEquals(14, vehicle.getBody());
		assertEquals( 2, vehicle.getSeats());
	}
	
}
