/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.BodytechQuality;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemAttributeObjectValue;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemEnhancementValue;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.proc.ItemRecalculation;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.HasOptionalCondition;
import org.prelle.shadowrun6.modifications.ItemHookModification;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CarriedItemTest {

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Locale.setDefault(Locale.ENGLISH);
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleWeapon() {
		ItemTemplate template = ShadowrunCore.getItem("colt_america_l36");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(ShadowrunCore.getItem("colt_america_l36"));
		ItemRecalculation.recalculate("", item);
		assertEquals(1, item.getCount());
		assertEquals(template.getAvailability(), item.getAvailability());
		assertEquals(template.getWeaponData().getDamage(), item.getAsValue(ItemAttribute.DAMAGE));
		assertTrue(Arrays.equals(template.getWeaponData().getAttackRating(), (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()));
		assertEquals("SA", item.getAsObject(ItemAttribute.MODE).getValue());
		assertEquals("11(c)", item.getAsObject(ItemAttribute.AMMUNITION).getValue());
		assertEquals("2 (L)", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		
		assertEquals(3, item.getSlots().size());
		assertNotNull(item.getSlot(ItemHook.TOP));
		assertEquals(0, item.getSlot(ItemHook.TOP).getCapacity(), 0);
		assertNotNull(item.getSlot(ItemHook.TOP).getAllEmbeddedItems());
		assertTrue(item.getSlot(ItemHook.TOP).getAllEmbeddedItems().isEmpty());
		assertNotNull(item.getSlot(ItemHook.BARREL));
		assertEquals(0, item.getSlot(ItemHook.BARREL).getCapacity(), 0);
		assertNull(item.getSlot(ItemHook.UNDER));
		
		assertEquals(0, item.getModifications().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponWithBuiltinAccessory() {
		ItemTemplate template = ShadowrunCore.getItem("ares_light_fire_75");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		ItemRecalculation.recalculate("", item);
		assertEquals(1, item.getCount());
		assertEquals(template.getAvailability(), item.getAvailability());
		assertEquals(template.getWeaponData().getDamage(), item.getAsValue(ItemAttribute.DAMAGE));
		assertTrue(Arrays.equals(template.getWeaponData().getAttackRating(), (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()));
		assertEquals("SA", item.getAsObject(ItemAttribute.MODE).getValue());
		assertEquals("16(c)", item.getAsObject(ItemAttribute.AMMUNITION).getValue());
		assertEquals("3 (L)", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		
		assertEquals(4, item.getSlots().size());
		assertNotNull(item.getSlot(ItemHook.TOP));
		assertEquals(0, item.getSlot(ItemHook.TOP).getCapacity(), 0);
		assertNotNull(item.getSlot(ItemHook.TOP).getAllEmbeddedItems());
		assertEquals(1, item.getSlot(ItemHook.TOP).getAllEmbeddedItems().size());
		assertEquals("laser_sight", item.getSlot(ItemHook.TOP).getAllEmbeddedItems().get(0).getItem().getId());
		assertNotNull(item.getSlot(ItemHook.BARREL));
		assertNotNull(item.getSlot(ItemHook.BARREL).getAllEmbeddedItems());
		assertEquals(0, item.getSlot(ItemHook.BARREL).getAllEmbeddedItems().size());
		assertNull(item.getSlot(ItemHook.INTERNAL));
		assertNull(item.getSlot(ItemHook.UNDER));
		assertTrue( Arrays.equals(new int[]{10,7,6,0,0}, (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()));
		
		assertEquals(0, item.getModifications().size());
		// Expect included +1 modifier from laser-sight, +1 from smartgun_system_internal and +2 modifier from smartgun_system_internal 
		assertEquals(3, item.getAsObject(ItemAttribute.ATTACK_RATING).getModifications().size());
		
		System.out.println("--------------------------------");
		
		// Now remove the barrel, so there is no laser-sight
		ItemHookModification removeBarrelMod = new ItemHookModification(ItemHook.TOP, 0);
		removeBarrelMod.setRemove(true);
		ItemEnhancement removeBarrel = new ItemEnhancement();
		removeBarrel.addModification(removeBarrelMod);
		item.addEnhancement(new ItemEnhancementValue(removeBarrel));
		item.getAsObject(ItemAttribute.ATTACK_RATING);
		assertTrue("Result was "+Arrays.toString( (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()), 
				Arrays.equals(new int[] {9,6,5,0,0}, (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()));
		assertEquals(0, item.getModifications().size());
//		// Expect -1 modifier from missing laser-sight and +2 modifier from smartgun_system_internal 
//		assertEquals(2, item.getAsObject(ItemAttribute.ATTACK_RATING).getModifications().size());
		// Expect  +1 and +2 modifier from smartgun_system_internal 
		assertEquals(2, item.getAsObject(ItemAttribute.ATTACK_RATING).getModifications().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testBuiltInInternalSmartgun() {
		ItemTemplate template = ShadowrunCore.getItem("ares_predator_vi");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		System.out.println("--------------------------------");
		ItemRecalculation.recalculate("", item);
		
		assertTrue("Missing OPTICAL slot",item.getSlots().stream().filter(as -> as.getSlot()==ItemHook.OPTICAL).count()==1);
	}


	//-------------------------------------------------------------------
	@Test
	public void testCyberlimbAttributeIncrease() {
		ItemTemplate template = ShadowrunCore.getItem("attribute_increase_agility");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template, 4);
		assertEquals(20000, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals("4", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		assertEquals( 4 , item.getAsFloat(ItemAttribute.CAPACITY).getModifiedValue(), 0);
		assertEquals(1, item.getModifications().size());
		assertSame(AttributeModification.class, item.getModifications().get(0).getClass());
		assertEquals(Attribute.AGILITY, ((AttributeModification)item.getModifications().get(0)).getAttribute());
		assertEquals(4, ((AttributeModification)item.getModifications().get(0)).getValue());
		assertNotNull(((AttributeModification)item.getModifications().get(0)).getSource());
		
		assertEquals(0, item.getRequirements().size());
	}	

	//-------------------------------------------------------------------
	@Test
	public void testCyberlimb() {
		ItemTemplate template = ShadowrunCore.getItem("cyberleg_obvious");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(ShadowrunCore.getItem("cyberleg_obvious"));
		ItemRecalculation.recalculate("", item);
		assertEquals("4", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		assertNotNull(item.getSlot(ItemHook.CYBERLIMB_IMPLANT));
		assertEquals( 20, item.getSlot(ItemHook.CYBERLIMB_IMPLANT).getCapacity(), 0);
		assertEquals(3, item.getModifications().size());
		assertSame(AttributeModification.class, item.getModifications().get(0).getClass());
		assertEquals(Attribute.AGILITY, ((AttributeModification)item.getModifications().get(0)).getAttribute());
		assertEquals(Attribute.STRENGTH, ((AttributeModification)item.getModifications().get(1)).getAttribute());
		assertEquals(Attribute.PHYSICAL_MONITOR, ((AttributeModification)item.getModifications().get(2)).getAttribute());
		assertEquals(2, ((AttributeModification)item.getModifications().get(1)).getValue());
		
		assertEquals(1000, item.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue());
		assertEquals(15000, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		item.setQuality(BodytechQuality.BETA);
		assertEquals(700, item.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue());
		assertEquals(22500, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
	}	

	//-------------------------------------------------------------------
	@Test
	public void testCyberlimbWithAttributeIncrease() {
		ItemTemplate template = ShadowrunCore.getItem("cyberleg_obvious");
		assertNotNull(template);
		CarriedItem leg = new CarriedItem(ShadowrunCore.getItem("cyberleg_obvious"));
		CarriedItem inc = new CarriedItem(ShadowrunCore.getItem("attribute_increase_agility"), 4);
		ItemRecalculation.recalculate("", inc);
		ItemRecalculation.recalculate("", leg);
		leg.addAccessory(ItemHook.CYBERLIMB_IMPLANT, inc);

		assertEquals(1000, leg.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue());
		assertEquals(35000, leg.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertTrue(leg.getToDos().isEmpty());
		leg.setQuality(BodytechQuality.BETA);
		ItemRecalculation.recalculate("", leg);
		assertEquals("Warning because of invalid accessory grade expected",1, leg.getToDos().size());
		
		// Correct accessory implant grade
		inc.setQuality(BodytechQuality.BETA);
		ItemRecalculation.recalculate("", leg);
		assertTrue(leg.getToDos().isEmpty());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleVehicle() {
		ItemTemplate template = ShadowrunCore.getItem("chrysler-nissan_jackrabbit");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(ShadowrunCore.getItem("chrysler-nissan_jackrabbit"));
		ItemRecalculation.recalculate("", item);
		assertEquals(1, item.getCount());
		assertEquals("2", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		assertEquals(11000, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(template.getAvailability(), item.getAvailability());
		assertEquals(template.getVehicleData().getHandling(), item.getAsObject(ItemAttribute.HANDLING).getModifiedValue());
		assertEquals(template.getVehicleData().getAcceleration() , item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue());
		assertEquals(template.getVehicleData().getSpeedInterval(), item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue());
		assertEquals(template.getVehicleData().getTopSpeed()     , item.getAsValue(ItemAttribute.SPEED).getModifiedValue());
		assertEquals(template.getVehicleData().getBody()         , item.getAsValue(ItemAttribute.BODY).getModifiedValue());
		assertEquals(template.getVehicleData().getArmor()        , item.getAsValue(ItemAttribute.ARMOR).getModifiedValue());
		assertEquals(template.getVehicleData().getPilot()        , item.getAsValue(ItemAttribute.PILOT).getModifiedValue());
		assertEquals(template.getVehicleData().getSensor()       , item.getAsValue(ItemAttribute.SENSORS).getModifiedValue());
		assertEquals(template.getVehicleData().getSeats()        , item.getAsValue(ItemAttribute.SEATS).getModifiedValue());
		
//		assertEquals(2, item.getSlots().size());
//		assertNotNull(item.getSlot(ItemHook.TOP));
//		assertEquals(0, item.getSlot(ItemHook.TOP).getCapacity());
//		assertNotNull(item.getSlot(ItemHook.TOP).getAllEmbeddedItems());
//		assertTrue(item.getSlot(ItemHook.TOP).getAllEmbeddedItems().isEmpty());
//		assertNotNull(item.getSlot(ItemHook.BARREL));
//		assertEquals(0, item.getSlot(ItemHook.BARREL).getCapacity());
//		assertNull(item.getSlot(ItemHook.UNDER));
		
		assertEquals(0, item.getModifications().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testDermalPlating() {
		ItemTemplate template = ShadowrunCore.getItem("dermal_plating");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template, 3);
		ItemRecalculation.recalculate("", item);
		assertEquals(3, item.getRating());
		assertEquals(2, item.getModifications().size()); // Add item dermal_plating_armor, remove dermal deposits
	}

	//-------------------------------------------------------------------
	@Test
	public void testCyberlimbWithWeapon() {
		ItemTemplate weaponI = ShadowrunCore.getItem("fichetti_tiffani_needler");
		ItemTemplate accessI = ShadowrunCore.getItem("cyber_holdout");
		ItemTemplate limbI = ShadowrunCore.getItem("cyberarm_obvious");
		assertNotNull(weaponI);
		assertNotNull(accessI);
		assertNotNull(limbI);
		CarriedItem limb = new CarriedItem(limbI);
		assertEquals(15000, limb.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		CarriedItem access = new CarriedItem(accessI);
//		access.setUsedAsType(ItemType.ACCESSORY);
//		access.setUsedAsSubType(ItemSubType.CYBER_LIMB_ACCESSORY);
//		access.setSlot(ItemHook.CYBERLIMB_IMPLANT);
		ItemRecalculation.recalculate("", access);
		ItemRecalculation.recalculate("", limb);
		assertTrue(limb.getWeaponDataRecursive().isEmpty());
		assertEquals(15000, limb.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(2000, access.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		// Add holdout space into the arm
		limb.addAccessory(ItemHook.CYBERLIMB_IMPLANT, access);
		ItemRecalculation.recalculate("", limb);
		assertEquals(17000, limb.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertTrue(limb.getWeaponDataRecursive().isEmpty());
		
		// Now add weapon to holdout space
		CarriedItem weapon = new CarriedItem(weaponI);
		assertEquals(435, weapon.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		access.addAccessory(ItemHook.MOUNTED_HOLDOUT, weapon);
		assertEquals(2435, access.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(17435, limb.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		ItemRecalculation.recalculate("", limb);
		assertFalse(limb.getWeaponDataRecursive().isEmpty());
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void testAresAlpha() {
//		ItemTemplate template = ShadowrunCore.getItem("ares_alpha");
//		assertNotNull(template);
//		CarriedItem item = new CarriedItem(template);
//		System.out.println(item.dump());
//		Collection<CarriedItem> accessories = item.getEffectiveAccessories();	
//		System.out.println("WEAP = "+accessories.iterator().next());
//		assertEquals(ItemType.WEAPON, accessories.iterator().next().getUsedAsType());
//	}

	//-------------------------------------------------------------------
	@Test
	public void testSmartgun() {
		ItemTemplate template = ShadowrunCore.getItem("ares_light_fire_75");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		ItemRecalculation.recalculate("", item);
		assertEquals(1, item.getCount());
		assertEquals(template.getAvailability(), item.getAvailability());
		assertEquals(template.getWeaponData().getDamage(), item.getAsValue(ItemAttribute.DAMAGE));
		assertTrue(Arrays.equals(template.getWeaponData().getAttackRating(), (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()));
		assertEquals("SA", item.getAsObject(ItemAttribute.MODE).getValue());
		assertEquals("16(c)", item.getAsObject(ItemAttribute.AMMUNITION).getValue());
		assertEquals("3 (L)", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		
		assertEquals(4, item.getSlots().size()); // TOP, UNDER, FIREARMS_EXTERNAL
		assertNotNull(item.getSlot(ItemHook.TOP));
		assertEquals(0, item.getSlot(ItemHook.TOP).getCapacity(), 0);
		assertNotNull(item.getSlot(ItemHook.TOP).getAllEmbeddedItems());
		assertEquals(1, item.getSlot(ItemHook.TOP).getAllEmbeddedItems().size());
		assertEquals("laser_sight", item.getSlot(ItemHook.TOP).getAllEmbeddedItems().get(0).getItem().getId());
		assertNotNull(item.getSlot(ItemHook.BARREL));
		assertNotNull(item.getSlot(ItemHook.BARREL).getAllEmbeddedItems());
		assertEquals(0, item.getSlot(ItemHook.BARREL).getAllEmbeddedItems().size());
		assertNotNull(item.getSlot(ItemHook.FIREARMS_EXTERNAL));
		assertNull(item.getSlot(ItemHook.INTERNAL));
		assertNull(item.getSlot(ItemHook.UNDER));
		assertNotNull(item.getSlot(ItemHook.OPTICAL));
		assertTrue("Result was "+Arrays.toString( (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue()),  Arrays.equals(new int[]{10,7,6,0,0}, (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue()));
		
		assertEquals(0, item.getModifications().size());
		// Expect included +1 modifier from laser-sight and +1/+2 modifier from smartgun_system_internal 
		assertEquals(3, item.getAsObject(ItemAttribute.ATTACK_RATING).getModifications().size());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void condititionTest1() {
		ItemTemplate template = ShadowrunCore.getItem("ares_light_fire_75");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		ItemRecalculation.recalculate("", item);
		
		ItemAttributeObjectValue ar = item.getAsObject(ItemAttribute.ATTACK_RATING);
		assertEquals(10, ((int[])ar.getModifiedValue())[0]);
		
		List<HasOptionalCondition> conds = item.getAvailableConditions();
		assertFalse(conds.isEmpty());
		assertEquals("itemmod.smartgun_system_internal.cond.1",item.getItem().getConditionKey(conds.get(0)));
		assertTrue(item.getConditions().isEmpty());
		System.err.println("conds = "+item.getItem().getConditionName(conds.get(0)));
		System.err.println("conds = "+((Modification)conds.get(0)).getSource());
		
		// Now enable the condition that grants +1
		item.addCondition(conds.get(1));
		assertFalse(item.getConditions().isEmpty());
		assertEquals("itemmod.smartgun_system_internal.cond.2",item.getConditions().get(0));
		
		ar = item.getAsObject(ItemAttribute.ATTACK_RATING);
		assertEquals(11, ((int[])ar.getModifiedValue())[0]);
		
	}

	//-------------------------------------------------------------------
	@Test
	public void testBasePriceMultiplier() {
		ItemTemplate template = ShadowrunCore.getItem("anti-vehicle_missile");
		
		CarriedItem item = new CarriedItem(template, 2);
		assertEquals(3800, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void testIncludedEnhancements() {
		// Krime Tradition includes a foregrip, whichs AR modifications 
		// should already be included
		ItemTemplate template = ShadowrunCore.getItem("krime_tradition");
		
		CarriedItem item = new CarriedItem(template);
		int[] expected = new int[] {9,9,6,0,0};
		int[] found    = (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue();
		assertTrue("Exp. "+Arrays.toString(expected)+" but got "+Arrays.toString(found), Arrays.equals(expected, found));
		
		assertEquals(500, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void test2ndNonAccessory() {
		ItemTemplate template = ShadowrunCore.getItem("erika_elite");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		item.setUsedAsType(ItemType.CYBERWARE);
		item.setUsedAsSubType(ItemSubType.COMMLINK);
		ItemRecalculation.recalculate("", item);
		assertEquals(ItemType.CYBERWARE, item.getUsedAsType());
	}

	//-------------------------------------------------------------------
	@Test
	public void testCapacityChangeSR6_696() {
		ItemTemplate template = ShadowrunCore.getItem("shiawase_vendX");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		item.setUsedAsType(ItemType.DRONE_MEDIUM);
		item.setUsedAsSubType(ItemSubType.GROUND);
		ItemRecalculation.recalculate("", item);
		AvailableSlot slot = item.getSlot(ItemHook.VEHICLE_CF);
		assertNotNull(slot);
		assertEquals(2.0f, slot.getCapacity(), 0);
	}

	//-------------------------------------------------------------------
	@Test
	public void testCapacityChangeSR6_696_2() {
		ItemTemplate template = ShadowrunCore.getItem("cyberspace_halo");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		item.setUsedAsType(ItemType.DRONE_SMALL);
		item.setUsedAsSubType(ItemSubType.AIR);
		assertEquals(0,ItemRecalculation.recalculate("", item).size());
		assertEquals(5, item.getAsValue(ItemAttribute.BODY).getModifiedValue());
		AvailableSlot elec = item.getSlot(ItemHook.VEHICLE_ELECTRONICS);
		AvailableSlot chas = item.getSlot(ItemHook.VEHICLE_CHASSIS);
		assertEquals(5.0, chas.getCapacity(), 0f);
		assertEquals(5.0, elec.getCapacity(), 0f);
		
		ItemTemplate mod = ShadowrunCore.getItem("modslot_chassis_to_elec");
		assertNotNull(mod);
		CarriedItem modItem = new CarriedItem(mod);
		modItem.setRating(2);
		ItemRecalculation.recalculate("", modItem);
		System.out.println("\n\n\n\n\n\n---1-------------------------------------------------------------");
		item.addAccessory(ItemHook.VEHICLE_CHASSIS, modItem);
		System.out.println("\n\n\n\n\n\n---2-------------------------------------------------------------");
		ItemRecalculation.recalculate("", item);
		assertEquals(7.0, elec.getCapacity(), 0);
		//assertEquals(3.0, chas.getCapacity(), 0f);
	}

	//-------------------------------------------------------------------
	@Test
	public void testCapacityChangeSR6_696_3() {
		ItemTemplate template = ShadowrunCore.getItem("cyberspace_halo");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		item.setUsedAsType(ItemType.DRONE_SMALL);
		item.setUsedAsSubType(ItemSubType.AIR);
		assertEquals(0,ItemRecalculation.recalculate("", item).size());
		assertEquals(5, item.getAsValue(ItemAttribute.BODY).getModifiedValue());
		AvailableSlot body = item.getSlot(ItemHook.VEHICLE_BODY);
		AvailableSlot chas = item.getSlot(ItemHook.VEHICLE_CHASSIS);
		assertEquals(0.5, body.getCapacity(), 0f);
		assertEquals(5.0, chas.getCapacity(), 0f);
		
		ItemTemplate mod = ShadowrunCore.getItem("hardpoint_standard");
		CarriedItem modItem = new CarriedItem(mod);
		assertEquals(1, ItemRecalculation.recalculate("", modItem).size());
		System.out.println("\n\n\n\n\n\n---1-------------------------------------------------------------");
		item.addAccessory(ItemHook.VEHICLE_CHASSIS, modItem);
		System.out.println("\n\n\n\n\n\n---2-------------------------------------------------------------");
		ItemRecalculation.recalculate("", item);
		assertEquals(1.5, body.getCapacity(), 0f);
		assertEquals(5.0, chas.getCapacity(), 0f);
		assertEquals(1.0, chas.getUsedCapacity(), 0f);
	}

	//-------------------------------------------------------------------
	@Test
	public void testCapacityChangeSR6_696_4() {
		ItemTemplate template = ShadowrunCore.getItem("cyberspace_halo");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		item.setUsedAsType(ItemType.DRONE_SMALL);
		item.setUsedAsSubType(ItemSubType.AIR);
		assertEquals(0,ItemRecalculation.recalculate("", item).size());
		assertEquals(5, item.getAsValue(ItemAttribute.BODY).getModifiedValue());
		AvailableSlot body = item.getSlot(ItemHook.VEHICLE_BODY);
		AvailableSlot chas = item.getSlot(ItemHook.VEHICLE_CHASSIS);
		assertEquals(0.5, body.getCapacity(), 0f);
		assertEquals(5.0, chas.getCapacity(), 0f);
		
		ItemTemplate mod = ShadowrunCore.getItem("hardpoint_small");
		CarriedItem modItem = new CarriedItem(mod);
		assertEquals(1, ItemRecalculation.recalculate("", modItem).size());
		System.out.println("\n\n\n\n\n\n---1-------------------------------------------------------------");
		item.addAccessory(ItemHook.VEHICLE_CHASSIS, modItem);
		System.out.println("\n\n\n\n\n\n---2-------------------------------------------------------------");
		ItemRecalculation.recalculate("", item);
		assertEquals(1.0, body.getCapacity(), 0f);
		assertEquals(5.0, chas.getCapacity(), 0f);
		assertEquals(0.5, chas.getUsedCapacity(), 0f);
	}

}
