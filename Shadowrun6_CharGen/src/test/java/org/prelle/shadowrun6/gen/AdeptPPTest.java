/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.prefs.Preferences;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.MagicOrResonanceOption;
import org.prelle.shadowrun6.Priority;
import org.prelle.shadowrun6.PriorityType;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.PriorityAttributeController;
import org.prelle.shadowrun6.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

/**
 * @author prelle
 *
 */
public class AdeptPPTest {

	private NewPriorityCharacterGenerator charGen;
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
		SR6ConfigOptions.attachConfigurationTree(new ConfigContainerImpl(Preferences.userRoot(), "babylon/plugins/shadowrun6"));
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		GenerationEventDispatcher.clear();
		charGen = new NewPriorityCharacterGenerator();
		charGen.attachConfigurationTree(new ConfigContainerImpl(Preferences.userRoot(), "foo"));
//		charGen.attachConfigurationTree(new ConfigContainerImpl(Preferences.userNodeForPackage(AdeptPPTest.class), "shadowrun"));
		charGen.start(model);
	}

	//-------------------------------------------------------------------
	@Test
	public void testCatalystAdeptCharGen() {
		charGen.setPriority(PriorityType.METATYPE, Priority.D);
		charGen.setPriority(PriorityType.MAGIC, Priority.B);
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("elf"));

		// Select Magician
		assertNotNull(model.getMagicOrResonanceType());
		for (MagicOrResonanceOption opt : new ArrayList<>(charGen.getMagicOrResonanceController().getAvailable())) {
			if (opt.getType().getId().equals("adept"))
				charGen.getMagicOrResonanceController().select(opt);
		}

		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(3, charGen.getPowerController().getPowerPointsLeft(), 0.0);

		// Now increase magic with adjustment points
		assertTrue(((PriorityAttributeController)charGen.getAttributeController()).increaseAdjust(Attribute.MAGIC));
		assertTrue(((PriorityAttributeController)charGen.getAttributeController()).increaseAdjust(Attribute.MAGIC));
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(5, charGen.getPowerController().getPowerPointsLeft(), 0.0);

		// Now increase magic with karma 
		assertTrue(((PriorityAttributeController)charGen.getAttributeController()).increaseKarma(Attribute.MAGIC));
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(6, charGen.getPowerController().getPowerPointsLeft(), 0.0);
		
		// Now loose essence
		charGen.getEquipmentController().select(ShadowrunCore.getItem("bone_density_augmentation_1"));
		assertEquals(5.7, model.getEssence(), 0.05);
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(5, charGen.getPowerController().getPowerPointsLeft(), 0.0);
		
		// Now initiate with metamagic Power Point
		charGen.getMetamagicOrEchoController().select(ShadowrunCore.getMetamagicOrEcho("power_points"));
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(7, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		assertEquals(7, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(6, charGen.getPowerController().getPowerPointsLeft(), 0.0);
	}

	//-------------------------------------------------------------------
	@Test
	public void testCatalystMysticAdeptCharGen() {
		SR6ConfigOptions.MYSTADEPT_ADVANCE_RAISE_MAGIC_RAISE_PP.set(false);

		charGen.setPriority(PriorityType.METATYPE, Priority.D);
		charGen.setPriority(PriorityType.MAGIC, Priority.B);
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("elf"));

		// Select Magician
		assertNotNull(model.getMagicOrResonanceType());
		for (MagicOrResonanceOption opt : new ArrayList<>(charGen.getMagicOrResonanceController().getAvailable())) {
			if (opt.getType().getId().equals("mysticadept"))
				charGen.getMagicOrResonanceController().select(opt);
		}

		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		
		// Split magic
		assertTrue(charGen.getMagicOrResonanceController().mysticAdeptIncreasePowerPoint());
		assertTrue(charGen.getMagicOrResonanceController().mysticAdeptIncreasePowerPoint());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(2, charGen.getPowerController().getPowerPointsLeft(), 0.0);

		// Now increase magic with adjustment points
		assertTrue(((PriorityAttributeController)charGen.getAttributeController()).increaseAdjust(Attribute.MAGIC));
		assertTrue(((PriorityAttributeController)charGen.getAttributeController()).increaseAdjust(Attribute.MAGIC));
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(2, charGen.getPowerController().getPowerPointsLeft(), 0.0);

		// Now increase magic with karma 
		assertTrue(((PriorityAttributeController)charGen.getAttributeController()).increaseKarma(Attribute.MAGIC));
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(2, charGen.getPowerController().getPowerPointsLeft(), 0.0);
	}

	//-------------------------------------------------------------------
	@Test
	public void testPegasusMysticAdeptCharGen() {
		SR6ConfigOptions.MYSTADEPT_ADVANCE_RAISE_MAGIC_RAISE_PP.set(true);

		charGen.setPriority(PriorityType.METATYPE, Priority.D);
		charGen.setPriority(PriorityType.MAGIC, Priority.B);
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("elf"));

		// Select Magician
		assertNotNull(model.getMagicOrResonanceType());
		for (MagicOrResonanceOption opt : new ArrayList<>(charGen.getMagicOrResonanceController().getAvailable())) {
			if (opt.getType().getId().equals("mysticadept"))
				charGen.getMagicOrResonanceController().select(opt);
		}

		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		
		// Split magic
		assertTrue(charGen.getMagicOrResonanceController().mysticAdeptIncreasePowerPoint());
		assertTrue(charGen.getMagicOrResonanceController().mysticAdeptIncreasePowerPoint());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(2, charGen.getPowerController().getPowerPointsLeft(), 0.0);

		// Now increase magic with adjustment points
		assertTrue(((PriorityAttributeController)charGen.getAttributeController()).increaseAdjust(Attribute.MAGIC));
		assertTrue(((PriorityAttributeController)charGen.getAttributeController()).increaseAdjust(Attribute.MAGIC));
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(2, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(5, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(2, charGen.getPowerController().getPowerPointsLeft(), 0.0);

		// Now increase magic with karma 
		assertTrue(((PriorityAttributeController)charGen.getAttributeController()).increaseKarma(Attribute.MAGIC));
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getNaturalValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.MAGIC).getMaximum());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getNaturalValue());
		assertEquals(3, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getAugmentedValue());
		assertEquals(6, charGen.getCharacter().getAttribute(Attribute.POWER_POINTS).getMaximum());
		assertEquals(3, charGen.getPowerController().getPowerPointsLeft(), 0.0);
	}

}
