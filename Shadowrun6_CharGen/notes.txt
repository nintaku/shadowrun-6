Qualities in character advancement
==================================
1. Losing a positve quality you picked at character creation  
   a) because it was a mistake                            --> Refund Karma cost 
   b) because something happend to your char during play  --> no Karma refund
2. Losing a negative quality you picked at character creation 
   a) because it was a mistake                            --> Pay with simple Karma cost
   b) because something happend to your char during play  --> Pay 2*Cost Karma
   
3. Losing a positve quality you picked during character advancement
   a) because it was a mistake                            --> Refund Karma cost (*2) 
   b) because something happend to your char during play  --> no Karma refund
4.) Losing a negative quality you picked at character advancement 
   a) because it was a mistake                            --> Pay with simple 2* Karma cost
   b) because something happend to your char during play  --> Pay 2*Cost Karma