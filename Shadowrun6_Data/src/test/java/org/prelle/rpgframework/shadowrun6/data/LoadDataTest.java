/**
 *
 */
package org.prelle.rpgframework.shadowrun6.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.OnRoadOffRoadValue;
import org.prelle.shadowrun6.items.proc.ItemRecalculation;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

/**
 * @author prelle
 *
 */
public class LoadDataTest {

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("logdir", System.getProperty("user.home"));
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	@Test
	public void loadDataTest() {
		ItemTemplate ares = ShadowrunCore.getItem("ares_predator_vi");
		assertEquals(3,ares.getSlots().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void loadDataTest2() {
		
		ItemTemplate car = ShadowrunCore.getItem("dodge_rampart");
		assertNotNull(car);
		assertEquals(9,car.getSlots().size());
		
		CarriedItem container = new CarriedItem(car);
		ItemRecalculation.recalculate("", container);
		assertEquals(20, ((OnRoadOffRoadValue)container.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOnRoad());
		assertEquals(20, ((OnRoadOffRoadValue)container.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOffRoad());
		assertEquals(3, ((OnRoadOffRoadValue)container.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOnRoad());
		assertEquals(4, ((OnRoadOffRoadValue)container.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOffRoad());
		
		// Now add tires to car
		ItemTemplate tires = ShadowrunCore.getItem("tires_large_offroad");
		assertNotNull(tires);
		CarriedItem accessory = new CarriedItem(tires);
		container.addAccessory(ItemHook.VEHICLE_TIRES, accessory);
		ItemRecalculation.recalculate("", container);
		assertEquals(15, ((OnRoadOffRoadValue)container.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOnRoad());
		assertEquals(25, ((OnRoadOffRoadValue)container.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOffRoad());
		
		// Now add 
		ItemTemplate multi = ShadowrunCore.getItem("multi_terrain_static");
		assertNotNull(multi);
		CarriedItem accessory2 = new CarriedItem(multi);
		container.addAccessory(ItemHook.VEHICLE_CHASSIS, accessory2);
		ItemRecalculation.recalculate("", container);
		assertEquals(4, ((OnRoadOffRoadValue)container.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOnRoad());
		assertEquals(3, ((OnRoadOffRoadValue)container.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOffRoad());
		
	}

	//-------------------------------------------------------------------
	@Test
	public void loadVehicleSensors() {
		
		ItemTemplate car = ShadowrunCore.getItem("dodge_rampart_LEV");
		assertNotNull(car);
		assertEquals(9,car.getSlots().size());
		
		CarriedItem container = new CarriedItem(car);
		ItemRecalculation.recalculate("", container);
		assertEquals(4, container.getAsValue(ItemAttribute.SENSORS).getModifiedValue());
		
		// Now add enhanced sensors
		ItemTemplate multi = ShadowrunCore.getItem("enhanced_sensors");
		assertNotNull(multi);
		CarriedItem accessory2 = new CarriedItem(multi, 6);
		container.addAccessory(ItemHook.VEHICLE_ELECTRONICS, accessory2);
		ItemRecalculation.recalculate("", container);
		assertEquals(6, container.getAsValue(ItemAttribute.SENSORS).getModifiedValue());
		
	}

	//-------------------------------------------------------------------
	@Test
	public void loadPoweredBreakdown() {
		
		ItemTemplate drone = ShadowrunCore.getItem("gm-nissan_dobermann");
		assertNotNull(drone);
		assertEquals(9,drone.getSlots().size());
		
		CarriedItem container = new CarriedItem(drone);
		ItemRecalculation.recalculate("", container);
		assertEquals(4, container.getAsValue(ItemAttribute.BODY).getModifiedValue());
		AvailableSlot chassis = container.getSlot(ItemHook.VEHICLE_CHASSIS);
		assertEquals(4, chassis.getFreeCapacity(), 0f);
		
		// Now add powered_breakdown
		ItemTemplate access = ShadowrunCore.getItem("powered_breakdown");
		assertNotNull(access);
		CarriedItem accessory = new CarriedItem(access, 3);
		
		System.out.println("\n----------1-----------");
		container.addAccessory(ItemHook.VEHICLE_CHASSIS, accessory);
		System.out.println("\n----------2-----------");
		ItemRecalculation.recalculate("", container);
		assertEquals(2, chassis.getFreeCapacity(), 0f);
	}

	//-------------------------------------------------------------------
	@Test
	public void loadEasyAssembly() {
		
		ItemTemplate drone = ShadowrunCore.getItem("gm-nissan_dobermann");
		assertNotNull(drone);
		assertEquals(9,drone.getSlots().size());
		
		CarriedItem container = new CarriedItem(drone);
		ItemRecalculation.recalculate("", container);
		assertEquals(4, container.getAsValue(ItemAttribute.BODY).getModifiedValue());
		AvailableSlot chassis = container.getSlot(ItemHook.VEHICLE_CHASSIS);
		assertEquals(4, chassis.getFreeCapacity(), 0f);
		
		// Now add powered_breakdown
		ItemTemplate access = ShadowrunCore.getItem("easy_assembly");
		assertNotNull(access);
		CarriedItem accessory = new CarriedItem(access, 3);
		
		System.out.println("\n----------1-----------");
		container.addAccessory(ItemHook.VEHICLE_CHASSIS, accessory);
		System.out.println("\n----------2-----------");
		ItemRecalculation.recalculate("", container);
		assertEquals(3, chassis.getFreeCapacity(), 0f);
	}

	//-------------------------------------------------------------------
	@Test
	public void loadPreEquippedEasyAssembly() {
		
		ItemTemplate drone = ShadowrunCore.getItem("zodiac_whisper");
		assertNotNull(drone);
//		assertEquals(9,drone.getSlots().size());
		
		CarriedItem container = new CarriedItem(drone);
		assertEquals(4, container.getAsValue(ItemAttribute.BODY).getModifiedValue());
		AvailableSlot chassis = container.getSlot(ItemHook.VEHICLE_CHASSIS);
		assertEquals(0, chassis.getFreeCapacity(), 0f);
	}

	//-------------------------------------------------------------------
	@Test
	public void testImprovedAcceleration() {
		ItemTemplate drone = ShadowrunCore.getItem("harley-davidson_scorpion");
		assertNotNull(drone);
		
		CarriedItem container = new CarriedItem(drone);
		assertEquals(new OnRoadOffRoadValue(16), container.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue());
		
		// Now add enhanced sensors
		ItemTemplate multi = ShadowrunCore.getItem("improved_acceleration");
		assertNotNull(multi);
		CarriedItem accessory2 = new CarriedItem(multi, 2);
		container.addAccessory(ItemHook.VEHICLE_POWERTRAIN, accessory2);
		ItemRecalculation.recalculate("", container);
		assertEquals(new OnRoadOffRoadValue(26), container.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue());
	}

}
