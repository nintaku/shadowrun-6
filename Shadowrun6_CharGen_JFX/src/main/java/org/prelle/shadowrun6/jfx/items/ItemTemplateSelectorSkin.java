/**
 * 
 */
package org.prelle.shadowrun6.jfx.items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.chargen.jfx.listcells.ItemTemplateListCell2;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class ItemTemplateSelectorSkin extends SkinBase<ItemTemplateSelector> implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private CharacterController charGen;
	private EquipmentController controller;
	private VBox content;
	private TextField tfName;
	private ChoiceBox<ItemType> cbTypes;
	private ChoiceBox<ItemSubType> cbSubTypes;
	private ListView<ItemTemplate> lvItems;
	private Label lbNuyen;
	
	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public ItemTemplateSelectorSkin(ItemTemplateSelector control, CharacterController controller) {
		super(control);
		this.charGen = controller;
		this.controller = controller.getEquipmentController();
		
		initComponents(control.getAllowedTypes());
		initLayout();
		initInteractivity();
		cbTypes.getSelectionModel().select(0);
		cbSubTypes.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents(ItemType... allowedTypes) {
		lbNuyen = new Label(String.valueOf(charGen.getCharacter().getNuyen()));
		
		tfName = new TextField();
		tfName.setPromptText(UI.getString("itemselector.name.prompt"));

		cbTypes = new ChoiceBox<ItemType>();
		cbTypes.setMaxWidth(Double.MAX_VALUE);
		cbTypes.getItems().addAll(allowedTypes);
		cbTypes.setConverter(new StringConverter<ItemType>() {
			public String toString(ItemType data) {return data!=null?data.getName():"";}
			public ItemType fromString(String data) {return null;}
		});

		cbSubTypes = new ChoiceBox<ItemSubType>();
		cbSubTypes.setMaxWidth(Double.MAX_VALUE);
		cbSubTypes.setConverter(new StringConverter<ItemSubType>() {
			public String toString(ItemSubType data) {return data!=null?data.getName():"";}
			public ItemSubType fromString(String data) {return null;}
		});

		lvItems = new ListView<ItemTemplate>();
		lvItems.setCellFactory(new Callback<ListView<ItemTemplate>, ListCell<ItemTemplate>>() {
			public ListCell<ItemTemplate> call(ListView<ItemTemplate> param) {
				return new ItemTemplateListCell2(charGen.getCharacter(), controller, false, null, cbTypes.getValue());
			}
		});
		lvItems.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdNuyen = new Label(Resource.get(UI, "label.nuyen")+": ");
		hdNuyen.getStyleClass().add("base");
		HBox nuyenLine = new HBox(10, hdNuyen, lbNuyen);
		
		content = new VBox();
		content.setStyle("-fx-spacing: 0.5em");
		content.getChildren().addAll(nuyenLine, tfName, cbTypes, cbSubTypes, lvItems);
		VBox.setVgrow(lvItems, Priority.ALWAYS);

		lvItems.setStyle("-fx-pref-width: 30em");
		lvItems.setMaxHeight(Double.MAX_VALUE);

		getSkinnable().impl_getChildren().add(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				cbSubTypes.getItems().clear();
				lvItems.getItems().clear();
				if (getSkinnable().getAllowedSubTypes()!=null) {
					List<ItemSubType> allow = Arrays.asList(getSkinnable().getAllowedSubTypes());
					for (ItemSubType type : n.getSubTypes()) {
						if (allow.contains(type)) {
							cbSubTypes.getItems().add(type);							
						}
					}						
				} else {
					cbSubTypes.getItems().addAll(n.getSubTypes());
				}
				cbSubTypes.getSelectionModel().select(0);
			}
		});

		cbSubTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				lvItems.getItems().clear();
				lvItems.getItems().addAll(ShadowrunTools.filterByPluginSelection(
						ShadowrunCore.getItems(cbTypes.getValue(), cbSubTypes.getValue()).stream().filter(it -> !it.isSelectableByModificationOnly()).collect(Collectors.toList()),
								charGen.getCharacter()));
			}
		});

		lvItems.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			this.getSkinnable().setSelectedItems(lvItems.getSelectionModel().getSelectedItems());
		});

		tfName.setOnAction(event -> {
			String filter = tfName.getText();
			logger.info("Search "+filter);
			if (filter.length()==0) {
				if (cbTypes.getValue()==null) {
					cbTypes.setValue(cbTypes.getItems().get(0));
				}
				List<ItemTemplate> poss = ShadowrunTools.filterByPluginSelection(
						ShadowrunCore.getItems(cbTypes.getValue(), cbSubTypes.getValue()).stream().filter(it -> !it.isSelectableByModificationOnly()).collect(Collectors.toList()), charGen.getCharacter());
				lvItems.getItems().setAll(poss);
			} else {
				cbTypes.getSelectionModel().clearSelection();
				cbSubTypes.getSelectionModel().clearSelection();
				List<ItemTemplate> poss = new ArrayList<ItemTemplate>();
				for (ItemType type : cbTypes.getItems()) {
					poss.addAll(ShadowrunTools.filterByPluginSelection(ShadowrunCore.getItems(type, type.getSubTypes()).stream().filter(it -> !it.isSelectableByModificationOnly()).collect(Collectors.toList()), charGen.getCharacter()));
				}
				List<ItemTemplate> filtered = new ArrayList<>();
				// Filter those names that match search string
				for (ItemTemplate tmp : poss) {
					if (tmp.getName().toLowerCase().contains(filter.toLowerCase())) {
						filtered.add(tmp);
					}
				}
				lvItems.getItems().setAll(filtered);
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EQUIPMENT_AVAILABLE_CHANGED:
		case NUYEN_CHANGED:
			logger.debug("RCV "+event);
			lvItems.getItems().clear();
			lvItems.getItems().addAll(ShadowrunCore.getItems(cbTypes.getValue(), cbSubTypes.getValue()));
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	SelectionModel<ItemTemplate> getSelectionModel() {
		return lvItems.getSelectionModel();
	}

}
