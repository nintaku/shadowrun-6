package org.prelle.shadowrun6.jfx.items.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.items.AmmunitionSlot;
import org.prelle.shadowrun6.items.AmmunitionSlot.AmmoSlotType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.WeaponData;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.persist.AmmunitionConverter;
import org.prelle.shadowrun6.persist.FireModesConverter;
import org.prelle.shadowrun6.persist.WeaponDamageConverter;

import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class WeaponFormBlock implements FormBlock {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private Map<ItemAttribute, List<ItemAttributeModification>> mods;
	
	private CheckBox cbIsWeapon;
	private ChoiceBox<Skill> cbSkill;
	private ChoiceBox<SkillSpecialization> cbSpecial;
	private TextField tfAcc;
	private TextField tfDmg;
	private VBox     layout;
	private Label     modAcc;
	private Label     modDmg;
	// For firearms
	private TextField tfMode;
	private TextField tfAmmo;
	
	//-------------------------------------------------------------------
	public WeaponFormBlock() {
		mods = new HashMap<ItemAttribute, List<ItemAttributeModification>>();
		initComponents();
		initLayout();
		initInteractivity();
		
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		cbIsWeapon = new CheckBox(UI.getString("form.itemdata.isWeapon"));
		cbSkill    = new ChoiceBox<Skill>();
		cbSkill.getItems().addAll(ShadowrunCore.getSkills(SkillType.COMBAT));
		cbSkill.setConverter(new StringConverter<Skill>() {
			public Skill fromString(String value) { return null; }
			public String toString(Skill value) { return value.getName(); }
		});
		cbSpecial  = new ChoiceBox<SkillSpecialization>();
		cbSpecial.setConverter(new StringConverter<SkillSpecialization>() {
			public SkillSpecialization fromString(String value) { return null; }
			public String toString(SkillSpecialization value) { return value.getName(); }
		});
		
		tfAcc  = new TextField();
		tfDmg  = new TextField();
		tfMode = new TextField();
		tfAmmo = new TextField();
		
		
		modAcc = new Label();
		modDmg = new Label();
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblTitle = new Label(UI.getString("form.itemdata.weapon.title"));
		Label lblDesc  = new Label(UI.getString("form.itemdata.weapon.desc"));
		lblTitle.getStyleClass().add("text-subheader");
		
		Label lblSkil  = new Label(UI.getString("label.weaponskill"));
		Label lblSpec  = new Label(UI.getString("label.specialization"));
		Label lblAcc   = new Label(ItemAttribute.ATTACK_RATING.getShortName());
		Label lblDmg   = new Label(ItemAttribute.DAMAGE.getShortName());
		Label lblMode  = new Label(ItemAttribute.MODE.getShortName());
		Label lblAmmo  = new Label(ItemAttribute.AMMUNITION.getShortName());
		
		GridPane grid = new GridPane();
		grid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em");
		grid.add(lblSkil  , 0, 0, 2,1);
		grid.add(cbSkill  , 2, 0, 3,1);
		grid.add(lblSpec  , 6, 0, 2,1);
		grid.add(cbSpecial, 8, 0, 3,1);
		grid.add(lblAcc, 0, 1);
		grid.add( tfAcc, 1, 1);
		grid.add(modAcc, 2, 1);
		
		grid.add(lblDmg, 3, 1);
		grid.add( tfDmg, 4, 1);
		grid.add(modDmg, 5, 1);

		grid.add(lblMode, 0, 2);
		grid.add(tfMode , 1, 2);
		
		grid.add(lblAmmo, 6, 2);
		grid.add(tfAmmo , 7, 2);
		
		lblAcc.setStyle("-fx-min-width: "+lblAcc.getText().length()+"em");
		lblDmg.setStyle("-fx-min-width: "+lblDmg.getText().length()+"em");
		lblMode.setStyle("-fx-min-width: "+lblMode.getText().length()+"em");
		tfMode.setStyle("-fx-min-width: 6em");
		lblAmmo.setStyle("-fx-min-width: "+lblAmmo.getText().length()+"em");
//		tfDmg.setStyle("-fx-pref-width: 8em");
//		lblSkil.setStyle("-fx-pref-width: 25em");
		
		layout = new VBox(lblTitle, lblDesc, grid);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbIsWeapon.selectedProperty().addListener( (ov,o,n) -> {
			tfAcc.setDisable(!n);
			tfDmg.setDisable(!n);
		});
		
		cbSkill.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			cbSpecial.getItems().clear();
			if (n!=null) {
				cbSpecial.getItems().addAll(n.getSpecializations());
			}
		});
		
		cbIsWeapon.selectedProperty().addListener( (ov,o,n) -> {
			layout.setVisible(n);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#getContent()
	 */
	@Override
	public Parent getContent() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#getCheckBox()
	 */
	@Override
	public CheckBox getCheckBox() {
		return cbIsWeapon;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#readInto(org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public void readInto(ItemTemplate item) {
		if (cbIsWeapon.isSelected()) {
			WeaponData data = new WeaponData();
				data.setSkill(cbSkill.getValue());
				if (cbSpecial.getValue()!=null)
					data.setSpecialization(cbSpecial.getValue());
//				data.setAttackRating(Integer.parseInt(tfAcc.getText()));
				try { data.setDamage(  (new WeaponDamageConverter()).read( tfDmg.getText())); } catch (Exception e) {}
				if (!tfMode.getText().isEmpty())
					try { data.setFireModes(  (new FireModesConverter()).read( tfMode.getText())); } catch (Exception e) {}
				if (!tfAmmo.getText().isEmpty())
					data.setAmmunition(new AmmunitionSlot(Integer.parseInt(tfAmmo.getText()), AmmoSlotType.MAGAZINE));
					
				
				item.setWeaponData(data);
		} else {
			item.setWeaponData(null);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#fillFrom(org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public void fillFrom(ItemTemplate item) {
		WeaponData data = item.getWeaponData();
		if (data==null) {
			cbSkill.getSelectionModel().clearSelection();
			cbSpecial.getSelectionModel().clearSelection();
			tfAcc.setText(null);
			tfDmg.setText(null);
		} else {
			cbSkill.setValue(data.getSkill());
			if (data.getSpecialization()!=null)
				cbSpecial.setValue(data.getSpecialization());
//			tfAcc.setText(String.valueOf(data.getAccuracy()));
			try {
				tfDmg.setText(  (new WeaponDamageConverter()).write( data.getDamage()));
				tfMode.setText( (new FireModesConverter()).write(data.getFireModes()));
			} catch (Exception e) {
			}
			tfAmmo.setText(String.valueOf(data.getAmmunition()));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.jfx.items.input.ItemDataForm.FormBlock#checkInput()
	 */
	@Override
	public boolean checkInput() {
		if (!cbIsWeapon.isSelected()) {
			cbSkill.getStyleClass().remove(ERROR_CLASS);
			tfAcc.getStyleClass().remove(ERROR_CLASS);
			tfDmg.getStyleClass().remove(ERROR_CLASS);
			return true;
		}
		
		logger.debug("checkInput");
		boolean isOkay = true;
		
		// Skill
		if (cbSkill.getValue()==null) {
			if (!cbSkill.getStyleClass().contains(ERROR_CLASS))
				cbSkill.getStyleClass().add(ERROR_CLASS);
			logger.debug("Skill is not set");
			isOkay=false;
		} else
			cbSkill.getStyleClass().remove(ERROR_CLASS);

		// Accuracy
		try {
			Integer.parseInt(tfAcc.getText());
			tfAcc.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			logger.debug("Accuracy is invalid: "+e);
			if (!tfAcc.getStyleClass().contains(ERROR_CLASS))
				tfAcc.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Damage
		try {
			(new WeaponDamageConverter()).read( tfDmg.getText());
			tfDmg.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			logger.debug("Weapon damage is invalid: "+e);
			if (!tfDmg.getStyleClass().contains(ERROR_CLASS))
				tfDmg.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Fire mode
		if (!tfMode.getText().isEmpty()) {
			try {
				(new FireModesConverter()).read( tfMode.getText());
				tfMode.getStyleClass().remove(ERROR_CLASS);
			} catch (Exception e) {
				logger.debug("Mode is invalid: "+e);
				if (!tfMode.getStyleClass().contains(ERROR_CLASS))
					tfMode.getStyleClass().add(ERROR_CLASS);
				isOkay=false;
			}
		}

		// Ammunition
		if (!tfAmmo.getText().isEmpty()) {
			try {
				(new AmmunitionConverter()).read( tfAmmo.getText());
				tfMode.getStyleClass().remove(ERROR_CLASS);
			} catch (Exception e) {
				logger.debug("Ammunition is invalid: "+e);
				if (!tfMode.getStyleClass().contains(ERROR_CLASS))
					tfMode.getStyleClass().add(ERROR_CLASS);
				isOkay=false;
			}
		}
			
		logger.debug("isOkay = "+isOkay);
		return isOkay;
	}
	
	//-------------------------------------------------------------------
	private int getModificator(ItemAttribute attr) {
		List<ItemAttributeModification> list = mods.get(attr);
		if (list==null) 
			return 0;
		
		int sum = 0;
		for (ItemAttributeModification mod : list) {
			sum += mod.getValue();
		}
		return sum;
	}
	
	//-------------------------------------------------------------------
	private void updateModifier() {
		// Accuracy
		int mod = 0; //getModificator(ItemAttribute.ACCURACY);
		if (mod!=0)
			modAcc.setText(String.valueOf(mod));
		else
			modAcc.setText(null);
	}
	
	//-------------------------------------------------------------------
	public void addModification(ItemAttributeModification aMod) {
		List<ItemAttributeModification> list = mods.get(aMod.getModifiedItem());
		if (list==null) {
			list = new ArrayList<ItemAttributeModification>();
			mods.put(aMod.getModifiedItem(), list);
		}
		
		list.add(aMod);
		updateModifier();
	}
	
	//-------------------------------------------------------------------
	public void removeModification(ItemAttributeModification aMod) {
		List<ItemAttributeModification> list = mods.get(aMod.getModifiedItem());
		if (list==null) {
			return;
		}
		
		list.remove(aMod);
		updateModifier();
	}
	
}