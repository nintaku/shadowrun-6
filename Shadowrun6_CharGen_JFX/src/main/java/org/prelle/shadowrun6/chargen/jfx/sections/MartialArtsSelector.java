/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.MartialArts;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.MartialArtsController;
import org.prelle.shadowrun6.chargen.jfx.listcells.MartialArtsListCell;

import de.rpgframework.ResourceI18N;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class MartialArtsSelector implements IListSelector<MartialArts> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(MartialArtsSection.class.getName());
	
	private MartialArtsController ctrl;
	private NavigButtonControl btnControl;

	private ListView<MartialArts> lvAvailable;
	private Label hdAvailable;
	private Label hdInfo;
	private Button btnDelKnow;
	
	private VBox layout;

	//-------------------------------------------------------------------
	public MartialArtsSelector(MartialArtsController ctrl) {
		this.ctrl = ctrl;
		btnControl   = new NavigButtonControl();
		btnControl.setDisabled(CloseType.OK, true);

		initComponents();
		initLayout();
		initInteractivity();
		
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		hdAvailable = new Label(Resource.get(UI, "label.available"));
		hdInfo  = new Label(Resource.get(UI, "label.info"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdInfo.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-font-size: 120%");
		hdInfo.setStyle("-fx-font-size: 130%");

		/* Column 1 */
		lvAvailable = new ListView<MartialArts>();
		lvAvailable.setCellFactory(new Callback<ListView<MartialArts>, ListCell<MartialArts>>() {
			public ListCell<MartialArts> call(ListView<MartialArts> param) {
				MartialArtsListCell cell =  new MartialArtsListCell(ctrl);
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.select(cell.getItem());
				});
				return cell;
			}
		});
		
		/* Column 2 */
		btnDelKnow = new Button("\uE0C6");
		btnDelKnow.getStyleClass().add("mini-button");
		btnDelKnow.setDisable(false);

		
		Label phAvailable = new Label(ResourceI18N.get(UI,"martialartsselectpane.placeholder.available"));
		phAvailable.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-pref-width: 30em;");
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		Region grow1 = new Region();
		grow1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow1, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, lvAvailable);

		column1.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		
		layout = new VBox(20, column1);
		layout.setMaxHeight(Double.MAX_VALUE);
		
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refreshOkayButton());
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getAvailableMartialArts());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the selectedAvailable
	 */
	public MartialArts getSelected() {
		return lvAvailable.getSelectionModel().getSelectedItem();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return layout; // ??
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<MartialArts> getSelectionModel() {
		return lvAvailable.getSelectionModel();
	}

	//-------------------------------------------------------------------
	private void refreshOkayButton() {
		btnControl.setDisabled(CloseType.OK,
				lvAvailable.getSelectionModel().getSelectedItem()==null
				||
				!ctrl.canBeSelected(lvAvailable.getSelectionModel().getSelectedItem()));
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

}
