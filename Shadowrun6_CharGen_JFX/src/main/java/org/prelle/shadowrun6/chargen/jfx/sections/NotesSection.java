package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.scene.control.TextArea;

/**
 * @author Stefan Prelle
 *
 */
public class NotesSection extends SingleSection {

	protected final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());

	protected CharacterController control; 
	protected TextArea taNotes;;
	protected ScreenManagerProvider provider;

	//-------------------------------------------------------------------
	public NotesSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title, null);
		this.control = ctrl;
		taNotes = new TextArea();
		taNotes.setPromptText(Resource.get(RES, "notessection.placeholder"));
		super.setContent(taNotes);
		taNotes.setStyle("-fx-pref-height: 12em; -fx-pref-width: 25em;");
		refresh();
		
		taNotes.textProperty().addListener( (ov,o,n) -> control.getCharacter().setNotes(n));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		taNotes.setText(control.getCharacter().getNotes());
	}

}
