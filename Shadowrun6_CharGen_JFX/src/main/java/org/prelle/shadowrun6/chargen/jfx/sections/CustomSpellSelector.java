/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.Spell.Category;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.chargen.jfx.listcells.SpellListCell;
import org.prelle.shadowrun6.chargen.jfx.panels.SpellDescriptionPane;
import org.prelle.shadowrun6.chargen.jfx.panels.SpellSelectionDoublePane;

import de.rpgframework.ResourceI18N;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class CustomSpellSelector extends VBox {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(CustomSpellSelector.class.getName());
	
	private SpellController ctrl;

	private TextField tfName;
	private ChoiceBox<Spell.Category> cbCategory;
	private ChoiceBox<Spell.Type> cbEffect;
	private ChoiceBox<Spell.Range> cbRange;
	private ChoiceBox<Spell.Duration> cbDuration;
	private TextField tfDrain;
	private TextArea  taDescription;

	//-------------------------------------------------------------------
	public CustomSpellSelector(SpellController ctrl) {
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		cbCategory.getSelectionModel().select(Spell.Category.MANIPULATION);
		cbEffect.getSelectionModel().select(Spell.Type.PHYSICAL);
		cbRange.getSelectionModel().select(Spell.Range.TOUCH);
		cbDuration.getSelectionModel().select(Spell.Duration.SUSTAINED);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField("Stone Fist");
		
		/* Category */
		cbCategory = new ChoiceBox<>();
		for (Spell.Category cat : Spell.Category.values())
			cbCategory.getItems().add(cat);
		cbCategory.setConverter(new StringConverter<Spell.Category>() {
			public String toString(Category object) { return object.getName();}
			public Category fromString(String string) { return null; }
		});
		
		/* Effect */
		cbEffect = new ChoiceBox<>();
		for (Spell.Type cat : Spell.Type.values())
			cbEffect.getItems().add(cat);
		cbEffect.setConverter(new StringConverter<Spell.Type>() {
			public String toString(Spell.Type object) { return object.getName();}
			public Spell.Type fromString(String string) { return null; }
		});
		
		/* Range */
		cbRange = new ChoiceBox<>();
		for (Spell.Range cat : Spell.Range.values())
			cbRange.getItems().add(cat);
		cbRange.setConverter(new StringConverter<Spell.Range>() {
			public String toString(Spell.Range object) { return object.getName();}
			public Spell.Range fromString(String string) { return null; }
		});
		
		/* Duration */
		cbDuration = new ChoiceBox<>();
		for (Spell.Duration cat : Spell.Duration.values())
			cbDuration.getItems().add(cat);
		cbDuration.setConverter(new StringConverter<Spell.Duration>() {
			public String toString(Spell.Duration object) { return object.getName();}
			public Spell.Duration fromString(String string) { return null; }
		});

		/* Drain */
		tfDrain  = new TextField("5");
		tfDrain.setPrefColumnCount(4);
		
		taDescription = new TextArea();
		taDescription.setPromptText(ResourceI18N.get(UI, "placeholder.description"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdName     = new Label(Resource.get(UI, "label.name"));
		Label hdCategory = new Label(Resource.get(UI, "label.category"));
		Label hdEffect   = new Label(Resource.get(UI, "label.effect"));
		Label hdRange    = new Label(Resource.get(UI, "label.range"));
		Label hdDuration = new Label(Resource.get(UI, "label.duration"));
		Label hdDrain    = new Label(Resource.get(UI, "label.drain"));
		Label hdDescr    = new Label(Resource.get(UI, "label.description"));
		hdName.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdCategory.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdDrain.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdEffect.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdRange.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdDuration.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		
		FlowPane flow = new FlowPane();
		flow.setHgap(10);
		flow.getChildren().add(new VBox(5, hdCategory, cbCategory));
		flow.getChildren().add(new VBox(5, hdEffect, cbEffect));
		flow.getChildren().add(new VBox(5, hdRange, cbRange));
		flow.getChildren().add(new VBox(5, hdDuration, cbDuration));
		flow.getChildren().add(new VBox(5, hdDrain, tfDrain));
		
		VBox.setMargin(flow, new Insets(20, 0, 20, 0));
		getChildren().addAll(hdName, tfName, flow, hdDescr, taDescription);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbCategory.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refresh());
	}

	//--------------------------------------------------------------------
	public void refresh()  {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the selectedAvailable
	 */
	public Spell getSelected() {
		StringBuffer id = new StringBuffer();
		String foo = tfName.getText();
		for (int i=0; i<foo.length(); i++) {
			char c = foo.charAt(i);
			switch (c) {
			case '"': 
			case '<': 
			case '>': 
				continue;
			}
			id.append( (Character.isWhitespace(c))?'_':c);
		}
		
		Spell ret = new Spell(id.toString());
		ret.setCustomName(tfName.getText());
		ret.setCategory(cbCategory.getValue());
		ret.setType(cbEffect.getValue());
		ret.setDuration(cbDuration.getValue());
		ret.setRange(cbRange.getValue());
		ret.setDrain(Integer.parseInt(tfDrain.getText()));
		ret.setCustomDescription(taDescription.getText());
		return ret;
	}

}
