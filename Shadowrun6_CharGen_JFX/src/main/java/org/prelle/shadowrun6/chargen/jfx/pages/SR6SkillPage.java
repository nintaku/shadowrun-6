package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.SignatureManeuver;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.KnowledgeSkillSection;
import org.prelle.shadowrun6.chargen.jfx.sections.SignatureManeuverSection;
import org.prelle.shadowrun6.chargen.jfx.sections.SkillSection;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.character.CharacterHandle;

/**
 * @author Stefan Prelle
 *
 */
public class SR6SkillPage extends SR6ManagedScreenPage {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6SkillPage.class.getName());

	private SkillSection   skills;
	private KnowledgeSkillSection knowledge;
	private KnowledgeSkillSection languages;
	private SignatureManeuverSection maneuvers;

	private Section secLine2;
	private Section secLine3;

	//-------------------------------------------------------------------
	public SR6SkillPage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-skills");
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.skills"));
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;

		initComponents();

		try {
			descrBtnEdit.setVisible(false);
		} catch (Throwable e) {
		}
		refresh();
	}

	//-------------------------------------------------------------------
	private void initKnowledge() {
		knowledge = new KnowledgeSkillSection(Resource.get(UI,"section.knowledge"), control, provider, SkillType.KNOWLEDGE);
		languages = new KnowledgeSkillSection(Resource.get(UI,"section.language"), control, provider, SkillType.LANGUAGE);

		secLine2 = new DoubleSection(knowledge, languages);
		getSectionList().add(secLine2);

		// Interactivity
		knowledge.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		languages.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initSkills() {
		skills = new SkillSection(Resource.get(UI,"section.skills"), control, provider);

		getSectionList().add(skills);

		// Interactivity
		skills.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initManeuvers() {
		maneuvers = new SignatureManeuverSection(Resource.get(UI,"section.maneuvers"), control, provider);

		secLine3 = new DoubleSection(maneuvers, null);
		getSectionList().add(secLine3);

		// Interactivity
		//maneuvers.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initSkills();
		initKnowledge();
		initManeuvers();
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (getManager()==null)
			setManager(provider.getManager());
		try { descrBtnEdit.setVisible(data!=null); } catch (Throwable e) {}
		this.helpData = data;
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		super.refresh();
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.skills"));

//		knowledge.refresh();
//		languages.refresh();
//		skills.refresh();
		
		skills.getToDoList().clear();
		skills.getToDoList().addAll(control.getSkillController().getNormalToDos());
		secLine2.getToDoList().clear();
		secLine2.getToDoList().addAll(control.getSkillController().getKnowledgeToDos());
	}

}
