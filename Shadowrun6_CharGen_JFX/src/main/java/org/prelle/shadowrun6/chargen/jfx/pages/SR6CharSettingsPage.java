package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.CharPluginListSection;
import org.prelle.shadowrun6.chargen.jfx.sections.CharPluginModeSection;
import org.prelle.shadowrun6.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.CharacterHandle;

/**
 * @author Stefan Prelle
 *
 */
public class SR6CharSettingsPage extends SR6ManagedScreenPage {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6CharSettingsPage.class.getName());

	private CharPluginModeSection  mode;
	private CharPluginListSection  plugins;

	//-------------------------------------------------------------------
	public SR6CharSettingsPage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-settings");
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.charsettings"));

		initComponents();
		expLine = new SR6EquipmentFirstLine(mode, control, provider);
		getCommandBar().setContent(expLine);

		refresh();
	}

	//-------------------------------------------------------------------
	private void initElectronics() {
		plugins  = new CharPluginListSection(ResourceI18N.get(UI,"section.plugins"), control, provider);
		mode = new CharPluginModeSection(ResourceI18N.get(UI,"section.mode"), control, provider, plugins);

		DoubleSection combinedSec = new DoubleSection(mode, plugins);
		getSectionList().add(combinedSec);

		// Interactivity
//		electronics.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
//		otherGear.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initElectronics();
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		try { descrBtnEdit.setVisible(data!=null); } catch (Throwable e) {}
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.trace("refresh");
		super.refresh();
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.charsettings"));

		mode.refresh();
	}

}
