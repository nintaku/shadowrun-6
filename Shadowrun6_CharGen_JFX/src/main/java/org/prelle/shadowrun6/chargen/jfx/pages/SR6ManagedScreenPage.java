package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.AppBarButton;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author Stefan Prelle
 *
 */
public class SR6ManagedScreenPage extends CharacterDocumentView {

	protected static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	
	protected static PropertyResourceBundle UI = SR6Constants.RES;

	protected CharacterController control;
	protected CharacterHandle handle;

	protected AppBarButton cmdPrint;
	protected AppBarButton cmdWizard;
	protected AppBarButton cmdSave;
	
	protected SR6FirstLine expLine;
	protected ViewMode mode;
	protected AppBarButton cmdFinish;
	protected CharacterViewScreenSR6Fluent provider;

	protected BasePluginData helpData;

	//-------------------------------------------------------------------
	public SR6ManagedScreenPage(CharacterController charGen, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super();
		this.control = charGen;
		this.handle  = handle;
		this.mode    = mode;
		this.provider= provider;
		if (provider==null)
			throw new NullPointerException("Fuck!");
		System.err.println(getClass().getSimpleName()+": "+provider.getManager()+" / "+provider.getScreenManager()+" / "+provider.getClass());
		initPrivateComponents();
		setHandle(handle);
		initPrivateInteractivity();
	}

	//-------------------------------------------------------------------
	private void initPrivateComponents() {
		/*
		 * Exp & Co.
		 */
		setPointsNameProperty(UI.getString("label.karma"));
		setPointsFree(control.getCharacter().getKarmaFree());
		expLine = new SR6FirstLine(mode, control, provider);
		expLine.setData(control.getCharacter());
		
//		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdPrint  = new AppBarButton(Resource.get(UI,"command.primary.print"), new SymbolIcon("print"));
		cmdFinish = new AppBarButton(Resource.get(UI,"command.primary.finish"), new SymbolIcon("accept"));
		cmdFinish.setCompact(true);
		cmdWizard = new AppBarButton(Resource.get(UI,"command.primary.wizard"), new SymbolIcon("undo"));
		cmdWizard.setCompact(true);
		cmdSave = new AppBarButton(Resource.get(UI,"command.primary.save"), new SymbolIcon("save"));
		cmdSave.setCompact(true);
		
		getCommandBar().setContent(expLine);
		if (mode==ViewMode.GENERATION) {
			getCommandBar().getPrimaryCommands().add(cmdWizard);
			getCommandBar().getPrimaryCommands().add(cmdFinish);
			getCommandBar().getPrimaryCommands().add(cmdSave);
		} else {
			getCommandBar().getPrimaryCommands().add(cmdPrint);
		}
		getCommandBar().setOpen(false);
	}

	//-------------------------------------------------------------------
	protected void setHandle(CharacterHandle value) {
		if (this.handle==value)
			return;
		if (this.handle!=null) {
			getCommandBar().getPrimaryCommands().removeAll(cmdPrint);			
		}
		
		this.handle = value;
		if (handle!=null) {
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
		}
		
		if (mode==ViewMode.GENERATION) {
			boolean ready = ((CharacterGenerator)control).hasEnoughData();
			cmdFinish.setDisable(!ready);
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		expLine.setData(control.getCharacter());
		
		getSectionList().forEach(sect -> sect.refresh());
		setPointsFree(control.getCharacter().getKarmaFree());
		
		if (mode==ViewMode.GENERATION) {
			boolean ready = ((CharacterGenerator)control).hasEnoughData();
			cmdFinish.setDisable(!ready);
		}
	}

	//-------------------------------------------------------------------
	private final void initPrivateInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter());});
		cmdFinish.setOnAction( ev -> {GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.FINISH_REQUESTED, handle, control.getCharacter()));});
		cmdWizard.setOnAction( ev -> provider.reopenWizard());
		cmdSave.setOnAction( ev -> provider.saveInCreationMode());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.CharacterDocumentView#changeCustomTextTo(java.lang.String)
	 */
	@Override
	public void changeCustomTextTo(String newText) {
		logger.debug("change text for "+helpData);
		try {
			if (helpData!=null) {
				helpData.setCustomHelpText(newText);
			} else {
				getScreenManager().showAlertAndCall(AlertType.ERROR, "There is a problem", ResourceI18N.get(UI, "error.customText.nothing_selected"));
			}
		} catch (Exception e) {
			getScreenManager().showAlertAndCall(AlertType.ERROR, "There is a problem", ResourceI18N.get(UI, "error.customText.error_setting")+"\n"+e.toString());
		}
	}

}
