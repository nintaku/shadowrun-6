/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.actions.ShadowrunAction.Category;
import org.prelle.shadowrun6.actions.ShadowrunAction.Type;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author stefa
 *
 */
public class ActionSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSection.class.getName());

	private CharacterController ctrl;

	private GridPane display;

	private ObjectProperty<ShadowrunAction> showHelpFor = new SimpleObjectProperty<>();

	//---------------------------------------------------------
	public ActionSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		this.ctrl = ctrl;
		display = new GridPane();
		display.setHgap(30);
		display.setVgap(6);
		layoutOneColumn();

//		table1.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {if (n!=null) showHelpFor.set(n.getModifyable()); });
	}

	//-------------------------------------------------------------------
	private void layoutOneColumn() {
		refresh();
		setContent(display);
	}


	//-------------------------------------------------------------------
	public ObjectProperty<ShadowrunAction> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		display.getChildren().clear();
		List<Object>[] lists = ShadowrunTools.getBalancedActionTable();
		
		int max=0;
		for (int i=0; i<lists.length; i++) max = Math.max(max, lists[i].size());
		
		for (int i=0; i<max; i++) {
			for (int c=0; c<lists.length; c++) {
				if (i<lists[c].size()) {
					Object o = lists[c].get(i);
					if (o instanceof Category) {
						Label cell = new Label( ((Category)o).getName() );
						cell.setMaxWidth(Double.MAX_VALUE);
						cell.setStyle("-fx-background-color: derive(#d19cb8, -10%);; -fx-text-fill:white; -fx-padding: 0 5 0 5");
						display.add(cell, c, i);
					} else {
						ActionCell cell = new ActionCell(this,ctrl.getCharacter(), (ShadowrunAction)o );
						display.add(cell, c, i);						
					}
				}
			}
		}
	}

}

class ActionCell extends HBox {
	
	private Label name;
	private Label cost;
	
	public ActionCell(ActionSection sect, ShadowrunCharacter model, ShadowrunAction action) {
		name = new Label(action.getName());
		cost = new Label(String.valueOf(model.getCostOfAction(action)));
		cost.setPadding(new Insets(0,5,0,5));
		getChildren().addAll(name, cost);
		HBox.setHgrow(name, Priority.ALWAYS);
		name.setMaxWidth(Double.MAX_VALUE);
		if (model.isCostModified(action)) {
			this.setStyle("-fx-background-color: lightgreen; -fx-font-weight: bold");
		}
		
		name.setTooltip(new Tooltip(action.getShortDescription()));
		
		this.setOnMouseClicked( ev -> sect.showHelpForProperty().set(action));
		this.setOnMouseEntered( ev -> getStyleClass().add("hover"));
		this.setOnMouseExited( ev -> getStyleClass().remove("hover"));
	}
}
