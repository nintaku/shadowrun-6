/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.Spell.Category;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.chargen.jfx.listcells.SpellListCell;
import org.prelle.shadowrun6.chargen.jfx.panels.SpellDescriptionPane;
import org.prelle.shadowrun6.chargen.jfx.panels.SpellSelectionDoublePane;

import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SpellSelector extends OptionalDescriptionPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SpellSelectionDoublePane.class.getName());
	
	private SpellController ctrl;
	private boolean alchemistic;

	private ChoiceBox<Spell.Category> cbCategory;
	private ListView<Spell> lvAvailable;
	private SpellDescriptionPane description;
	private Label hdAvailable;
	private Label hdInfo;
	private Button btnDelKnow;

	//-------------------------------------------------------------------
	public SpellSelector(SpellController ctrl, boolean alchemistic) {
		this.ctrl = ctrl;
		this.alchemistic = alchemistic;

		initComponents();
		initLayout();
		initInteractivity();

		cbCategory.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		hdAvailable = new Label(Resource.get(UI, "label.available"));
		hdInfo  = new Label(Resource.get(UI, "label.info"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdInfo.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-font-size: 120%");
		hdInfo.setStyle("-fx-font-size: 130%");

		/* Column 1 */
		cbCategory = new ChoiceBox<>();
		for (Spell.Category cat : Spell.Category.values())
			cbCategory.getItems().add(cat);
		cbCategory.setConverter(new StringConverter<Spell.Category>() {
			public String toString(Category object) { return object.getName();}
			public Category fromString(String string) { return null; }
		});
		cbCategory.setMaxWidth(Double.MAX_VALUE);

		lvAvailable = new ListView<Spell>();
		lvAvailable.setCellFactory(new Callback<ListView<Spell>, ListCell<Spell>>() {
			public ListCell<Spell> call(ListView<Spell> param) {
				SpellListCell cell =  new SpellListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.select(cell.getItem(), alchemistic);
				});
				return cell;
			}
		});
		
		description = new SpellDescriptionPane();
		
		/* Column 2 */
		btnDelKnow = new Button("\uE0C6");
		btnDelKnow.getStyleClass().add("mini-button");
		btnDelKnow.setDisable(false);

		
		Label phAvailable = new Label(UI.getString("spellselectpane.placeholder.available"));
		phAvailable.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-pref-width: 24em;");
		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		Region grow1 = new Region();
		grow1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow1, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, cbCategory, lvAvailable);

		column1.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);

		setChildren(column1, description);

		description.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(description, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbCategory.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refresh());
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> description.setData(n));
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		if (cbCategory.getSelectionModel().getSelectedItem()==null)
			lvAvailable.getItems().addAll(ctrl.getAvailableSpells(alchemistic));
		else
			lvAvailable.getItems().addAll(ctrl.getAvailableSpells(cbCategory.getSelectionModel().getSelectedItem(), alchemistic));
	}

	//-------------------------------------------------------------------
	/**
	 * @return the selectedAvailable
	 */
	public Spell getSelected() {
		return lvAvailable.getSelectionModel().getSelectedItem();
	}

}
