package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCharacter.PluginMode;
import org.prelle.shadowrun6.charctrl.CharacterController;

import de.rpgframework.ResourceI18N;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class CharPluginModeSection extends SingleSection {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(CharPluginModeSection.class.getName());

	private CharacterController ctrl;
	private VBox layout;
	private RadioButton cbPluginModeAll;
	private RadioButton cbPluginModeLanguage;
	private RadioButton cbPluginModeSelected;
	private ToggleGroup toggleGroup;
	private CharPluginListSection plugSection;

	//-------------------------------------------------------------------
	/**
	 * @param provider
	 */
	public CharPluginModeSection(String title, CharacterController ctrl, ScreenManagerProvider provider, CharPluginListSection plugSection) {
		super(provider, title, new VBox(10));
		this.ctrl = ctrl;
		this.plugSection = plugSection;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		cbPluginModeAll      = new RadioButton(ResourceI18N.get(RES, "label.mode.all"));
		cbPluginModeAll.setUserData(PluginMode.ALL);
		cbPluginModeLanguage = new RadioButton(ResourceI18N.get(RES, "label.mode.language"));
		cbPluginModeLanguage.setUserData(PluginMode.LANGUAGE);
		cbPluginModeSelected = new RadioButton(ResourceI18N.get(RES, "label.mode.selected"));
		cbPluginModeSelected.setUserData(PluginMode.SELECTED);
		toggleGroup = new ToggleGroup();
		toggleGroup.getToggles().addAll(cbPluginModeAll, cbPluginModeLanguage, cbPluginModeSelected);

	}

	//-------------------------------------------------------------------
	private void initLayout() {
		layout = (VBox) getContent(); 
		layout.setStyle("-fx-padding: 1em");
		layout.getChildren().addAll(cbPluginModeAll, cbPluginModeLanguage, cbPluginModeSelected);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		toggleGroup.selectedToggleProperty().addListener( (ov,o,n) ->  {
			if (n==null) 
				return;
			PluginMode newMode = (PluginMode) n.getUserData();
			ctrl.getCharacter().setPluginMode(newMode);
			plugSection.setDisable(newMode!=PluginMode.SELECTED);
			ctrl.runProcessors();
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		ShadowrunCharacter model = ctrl.getCharacter();
		if (model.getPluginMode()==null || model.getPluginMode()==PluginMode.LANGUAGE) {
			cbPluginModeLanguage.setSelected(true);
		} else if (model.getPluginMode()==PluginMode.ALL) {
			cbPluginModeAll.setSelected(true);
		} else {
			cbPluginModeSelected.setSelected(true);
		}
	}

}
