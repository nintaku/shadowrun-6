package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.stream.Collectors;

import org.prelle.shadowrun6.MartialArts;
import org.prelle.shadowrun6.charctrl.MartialArtsController;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;

public class MartialArtsListCell extends ListCell<MartialArts> {

	private final static String NORMAL_STYLE = "skill-cell";

	private MartialArtsController control;
	private MartialArts data;

	private Label lineName;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblCategory;

	//-------------------------------------------------------------------
	public MartialArtsListCell(MartialArtsController ctrl) {
		this.control = ctrl;
		lblCategory = new Label();
		lblCategory.getStyleClass().add("type-label");
		StackPane.setAlignment(lblCategory, Pos.CENTER_RIGHT);

		// Content
		lineName = new Label();
		lineName.setStyle("-fx-font-weight: bold;");
		StackPane.setAlignment(lineName, Pos.CENTER_LEFT);

		// Recommended icon
		imgRecommended = new ImageView();
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblCategory, lineName, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MartialArts item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);			
		} else {
			lblCategory.setText( String.join(", ", item.getCategories().stream().map(cat -> cat.getName()).collect(Collectors.toList())) );
			lineName.setText(((MartialArts)item).getName());
//			imgRecommended.setVisible(control.isRecommended((MartialArts) item));
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("martart:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

}