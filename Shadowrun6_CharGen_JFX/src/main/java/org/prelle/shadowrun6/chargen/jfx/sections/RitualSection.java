package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.RitualValue;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.RitualValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.levelling.CharacterLeveller;

import de.rpgframework.ConfigOption;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class RitualSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	
	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(RitualSection.class.getName());

	private CharacterController ctrl;

	private ListView<RitualValue> list;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	/**
	 */
	public RitualSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setDeleteButton( new Button(null, new SymbolIcon("delete")) );
		setAddButton( new Button(null, new SymbolIcon("add")) );
		getDeleteButton().setDisable(true);
		
		list = new ListView<RitualValue>();
		list.setMaxWidth(Double.MAX_VALUE);
		list.setStyle("-fx-min-width: 20em; -fx-pref-width: 30em"); 
		list.setCellFactory(new Callback<ListView<RitualValue>, ListCell<RitualValue>>() {
			public ListCell<RitualValue> call(ListView<RitualValue> param) {
				RitualValueListCell cell =  new RitualValueListCell(ctrl.getRitualController());
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.getRitualController().deselect(cell.getItem());
				});
				return cell;
			}
		});
		if (ctrl instanceof CharacterLeveller)
			setSettingsButton( new Button(null, new SymbolIcon("setting")) );
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(list);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			getDeleteButton().setDisable(n==null); 
			if (n!=null) showHelpFor.set(n.getModifyable()); 
		});
		if (getSettingsButton()!=null)
			getSettingsButton().setOnAction(ev -> onSettings());
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening spell selection dialog");
		
		RitualSelector pane = new RitualSelector(ctrl.getRitualController());
		ManagedDialog dialog = new ManagedDialog(Resource.get(RES, "ritualsection.selector.title"), pane, CloseType.OK);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.info("Closed with "+close);
		if (close==CloseType.OK) {
			Ritual selected = pane.getSelected();
			logger.debug("Selected ritual: "+selected);
			if (selected!=null) {
				ctrl.getRitualController().select(selected);
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		RitualValue selected = list.getSelectionModel().getSelectedItem();
		logger.debug("Ritual to deselect: "+selected);
		ctrl.getRitualController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void onSettings() {
		VBox content = new VBox(20);
		for (ConfigOption<?> opt : ctrl.getSpellController().getConfigOptions()) {
			CheckBox cb = new CheckBox(opt.getName());
			cb.setSelected((Boolean)opt.getValue());
			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
			content.getChildren().add(cb);
		}
		
		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		list.getItems().clear();
		list.getItems().addAll(ctrl.getCharacter().getRituals());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public MultipleSelectionModel<RitualValue> getSelectionModel() {
		return list.getSelectionModel();
	}

}
