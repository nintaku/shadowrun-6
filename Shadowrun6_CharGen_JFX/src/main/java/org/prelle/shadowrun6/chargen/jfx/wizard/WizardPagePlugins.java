/**
/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCharacter.PluginMode;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.chargen.jfx.listcells.PluginListCell;
import org.prelle.shadowrun6.gen.CharacterGeneratorRegistry;
import org.prelle.shadowrun6.jfx.SR6Constants;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.RulePlugin;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class WizardPagePlugins extends WizardPage {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPagePlugins.class.getName());
	
	private ShadowrunCharacter model;
	
	private Label lbExplain;
	private RadioButton cbPluginModeAll;
	private RadioButton cbPluginModeLanguage;
	private RadioButton cbPluginModeSelected;
	private ToggleGroup toggleGroup;
	private CheckBox cbShowOnlyMyLanguagePlugins;
	private ListView<RulePlugin<ShadowrunCharacter>> options;

	//--------------------------------------------------------------------
	public WizardPagePlugins(Wizard wizard, ShadowrunCharacter model) {
		super(wizard);
		this.model = model;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		cbPluginModeLanguage.setSelected(true);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbExplain = new Label(ResourceI18N.get(UI, "wizard.selectPlugins.explain"));
		cbPluginModeAll      = new RadioButton(ResourceI18N.get(UI, "wizard.selectPlugins.mode.all"));
		cbPluginModeAll.setUserData(PluginMode.ALL);
		cbPluginModeLanguage = new RadioButton(ResourceI18N.get(UI, "wizard.selectPlugins.mode.language"));
		cbPluginModeLanguage.setUserData(PluginMode.LANGUAGE);
		cbPluginModeSelected = new RadioButton(ResourceI18N.get(UI, "wizard.selectPlugins.mode.selected"));
		cbPluginModeSelected.setUserData(PluginMode.SELECTED);
		toggleGroup = new ToggleGroup();
		toggleGroup.getToggles().addAll(cbPluginModeAll, cbPluginModeLanguage, cbPluginModeSelected);
		cbShowOnlyMyLanguagePlugins = new CheckBox(ResourceI18N.get(UI, "wizard.selectPlugins.showOnlyMyLanguage"));
		
		options= new ListView<RulePlugin<ShadowrunCharacter>>();
		options.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		options.setCellFactory(new Callback<ListView<RulePlugin<ShadowrunCharacter>>, ListCell<RulePlugin<ShadowrunCharacter>>>() {
			public ListCell<RulePlugin<ShadowrunCharacter>> call(ListView<RulePlugin<ShadowrunCharacter>> arg0) {
				return new PluginListCell(model);
			}
		});

		setTitle(ResourceI18N.get(UI,"wizard.selectPlugins.title"));
		// Fill with data
//		options.getItems().addAll(ShadowrunCore.getPlugins());
		
		String fName = "images/wizard/Type_of_Creation.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}
		refreshContent();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		options.setStyle("-fx-max-width: 25em");
		VBox layout = new VBox(10, lbExplain, cbPluginModeAll, cbPluginModeLanguage, cbPluginModeSelected, cbShowOnlyMyLanguagePlugins, options);
		VBox.setMargin(cbPluginModeAll, new Insets(20, 0, 0, 0));
		VBox.setMargin(cbShowOnlyMyLanguagePlugins, new Insets(20, 0, 0, 0));
		VBox.setMargin(options, new Insets(20, 0, 0, 0));
		
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		toggleGroup.selectedToggleProperty().addListener( (ov,o,n) ->  {
			if (n==null) 
				return;
			PluginMode newMode = (PluginMode) n.getUserData();
			model.setPluginMode(newMode);
			options.setDisable(newMode!=PluginMode.SELECTED);
			cbShowOnlyMyLanguagePlugins.setDisable(newMode!=PluginMode.SELECTED);
		});
		
		cbShowOnlyMyLanguagePlugins.selectedProperty().addListener( (ov,o,n) -> refreshContent());
	}

	//-------------------------------------------------------------------
	private void refreshContent() {
		List<RulePlugin<ShadowrunCharacter>> list = ShadowrunCore.getPlugins();
		if (cbShowOnlyMyLanguagePlugins.isSelected()) {
			for (RulePlugin<ShadowrunCharacter> plugin : new ArrayList<RulePlugin<ShadowrunCharacter>>(list)) {
				if (!plugin.getLanguages().contains(Locale.getDefault().getLanguage())) {
					list.remove(plugin);
					model.removePermittedPlugin(plugin.getID());
				}
			}
		}
		Collections.sort(list, new Comparator<RulePlugin<ShadowrunCharacter>>() {
			@Override
			public int compare(RulePlugin<ShadowrunCharacter> o1, RulePlugin<ShadowrunCharacter> o2) {
				System.out.println("Compare "+o1.getReadableName()+" with "+o2.getReadableName());
				return o1.getReadableName().compareTo(o2.getReadableName());
			}});
		options.getItems().setAll(list);
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft() {
		CharacterGeneratorRegistry.getSelected().runProcessors();
	}

}