package org.prelle.shadowrun6.chargen.jfx.dialogs;

import java.util.Collection;
import java.util.List;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;

import de.rpgframework.character.HardcopyPluginData;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class SelectPluginDataDialog<T extends HardcopyPluginData> extends ManagedDialog {
	
	private OptionalDescriptionPane paneWithDesc;
	private ListView<T> available;
	private DescriptionPane desc;
	private NavigButtonControl btnControl;

	//-------------------------------------------------------------------
	public SelectPluginDataDialog(String title, List<T> data, Callback<ListView<T>, ListCell<T>> cellFactory, CloseType... buttons) {
		super(title, null, buttons);

		initComponents();
		available.setCellFactory(cellFactory);
		initInteractivity();
		initLayout();
		
		available.getItems().addAll(data);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		btnControl = new NavigButtonControl();
		available = new ListView<T>();
		available.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		desc = new DescriptionPane();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		available.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			desc.setText(n);
		});
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		paneWithDesc = new OptionalDescriptionPane(available, desc);
		setContent(paneWithDesc);
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	public Collection<T> getSelection() {
		return available.getSelectionModel().getSelectedItems();
	}
}
