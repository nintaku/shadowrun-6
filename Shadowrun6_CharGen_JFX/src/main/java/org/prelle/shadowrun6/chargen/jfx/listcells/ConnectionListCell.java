package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.ConnectionsController;
import org.prelle.shadowrun6.chargen.jfx.sections.ConnectionSection;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;

public class ConnectionListCell extends ListCell<Connection> {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(ConnectionSection.class.getName());

	private final static String NORMAL_STYLE = "connection-cell";

	private ConnectionsController charGen;
	private ScreenManagerProvider provider;

	private HBox layout;
	private Label lblName;
	private Label lblType;
	private Button btnEdit;
	private Button btnDecInfl;
	private Label  lblValInfl;
	private Button btnIncInfl;
	private Button btnDecLoyl;
	private Label  lblValLoyl;
	private Button btnIncLoyl;

	//-------------------------------------------------------------------
	public ConnectionListCell(CharacterController control, ScreenManagerProvider provider) {
		this.charGen = control.getConnectionController();
		this.provider = provider;

		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_CENTER);

		// Content
		lblName  = new Label();
		lblType  = new Label();
		btnEdit = new Button("\uE1C2");
		btnEdit.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnDecInfl  = new Button("\uE0C9");
		lblValInfl  = new Label("?");
		btnIncInfl  = new Button("\uE0C8");

		btnDecLoyl  = new Button("\uE0C9");
		lblValLoyl  = new Label("?");
		btnIncLoyl  = new Button("\uE0C8");

		layout = new HBox(10);

		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		getStyleClass().add(NORMAL_STYLE);

		btnDecInfl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnIncInfl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnDecLoyl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnIncLoyl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		lblName.setStyle("-fx-font-weight: bold");
		lblType.setStyle("-fx-font-weight: bold");
		lblValInfl.getStyleClass().add("text-subheader");
		lblValLoyl.getStyleClass().add("text-subheader");

		btnEdit.setStyle("-fx-background-color: transparent");

		//		setStyle("-fx-pref-width: 24em");
//		layout.getStyleClass().add("content");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label heaName = new Label(Resource.get(UI,"label.name"));
		Label heaType = new Label(Resource.get(UI,"label.type"));
		Label heaInfl = new Label(Resource.get(UI,"label.influence.short"));
		Label heaLoyl = new Label(Resource.get(UI,"label.loyalty.short"));

		GridPane col1 = new GridPane();
		col1.setStyle("-fx-hgap: 0.4em; -fx-vgap: 0.4em;");
		col1.add(heaName, 0, 0);
		col1.add(lblName, 1, 0);
		col1.add(heaType, 0, 1);
		col1.add(lblType, 1, 1);
		col1.setMaxWidth(Double.MAX_VALUE);

		GridPane col2 = new GridPane();
		col2.setStyle("-fx-hgap: 0.2em; -fx-vgap: 0.2em;");
		col2.add(heaInfl   , 0, 0);
		col2.add(btnDecInfl, 1, 0);
		col2.add(lblValInfl, 2, 0);
		col2.add(btnIncInfl, 3, 0);
		col2.add(heaLoyl   , 0, 1);
		col2.add(btnDecLoyl, 1, 1);
		col2.add(lblValLoyl, 2, 1);
		col2.add(btnIncLoyl, 3, 1);

		HBox.setHgrow(col1, Priority.ALWAYS);
		layout.getChildren().addAll(col1, col2);

		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnIncInfl .setOnAction(event -> {
			charGen.increaseInfluence(ConnectionListCell.this.getItem());
			updateItem(ConnectionListCell.this.getItem(), false);
		});
		btnDecInfl .setOnAction(event -> {
			charGen.decreaseInfluence(ConnectionListCell.this.getItem());
			updateItem(ConnectionListCell.this.getItem(), false);
		});
		btnIncLoyl .setOnAction(event -> {
			charGen.increaseLoyalty(ConnectionListCell.this.getItem());
			updateItem(ConnectionListCell.this.getItem(), false);
		});
		btnDecLoyl .setOnAction(event -> {
			charGen.decreaseLoyalty(ConnectionListCell.this.getItem());
			updateItem(ConnectionListCell.this.getItem(), false);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Connection item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(item.getName());
			lblType.setText(item.getType());
			btnEdit.setText("\uE1C2");
			btnEdit.setTooltip(new Tooltip(Resource.get(UI,"connectionlistcell.tooltip.edit")));

			lblValInfl.setText(String.valueOf(item.getInfluence()));
			btnDecInfl.setDisable(!charGen.canDecreaseInfluence(item));
			btnIncInfl.setDisable(!charGen.canIncreaseInfluence(item));

			lblValLoyl.setText(String.valueOf(item.getLoyalty()));
			btnDecLoyl.setDisable(!charGen.canDecreaseLoyalty(item));
			btnIncLoyl.setDisable(!charGen.canIncreaseLoyalty(item));

			setGraphic(layout);
		}
	}

}