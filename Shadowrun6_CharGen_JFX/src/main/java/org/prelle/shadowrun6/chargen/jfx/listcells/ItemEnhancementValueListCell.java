package org.prelle.shadowrun6.chargen.jfx.listcells;

import org.prelle.shadowrun6.items.ItemEnhancementValue;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class ItemEnhancementValueListCell extends ListCell<ItemEnhancementValue> {

	private HBox layout;
	private Label lblSize;
	private Label lblName;

	//--------------------------------------------------------------------
	public ItemEnhancementValueListCell() {
		lblSize = new Label();
		lblSize.getStyleClass().add("text-subheader");
		lblName  = new Label();
		lblName.getStyleClass().add("text-small-subheader");

		layout = new HBox(5);
		layout.getChildren().addAll(lblName, lblSize);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		lblName.setMaxWidth(Double.MAX_VALUE);
		
//		getStyleClass().add("metatype-cell");
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ItemEnhancementValue item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(layout);
			if (item.getModifyable()==null) {
				lblName.setText("??Error??");
			} else {
				lblName.setText(item.getModifyable().getName());
				if (item.isAutoAdded())
					lblSize.setText(null);
				else
					lblSize.setText(""+item.getModifyable().getSize());
			}
		}
	}
}