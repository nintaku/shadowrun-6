/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.gen.CharacterGenerator;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class AttributePaneSecondaryGen extends GridPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(AttributePanePrimaryGen.class.getName());

	private CharacterController control;

	private Label headDeri, headDValue;
	private Map<Attribute, Label> values;

	//--------------------------------------------------------------------
	public AttributePaneSecondaryGen(CharacterController cntrl) {
		this.control = cntrl;
		assert control!=null;

		values = new HashMap<Attribute, Label>();
		
		doInit();
		doLayout();
		refresh();
	}

	//-------------------------------------------------------------------
	private void doInit() {
		super.setMaxWidth(Double.MAX_VALUE);
		super.getStyleClass().add("content");
		
		headDeri   = new Label(Resource.get(UI,"label.derived_values"));
		headDValue = new Label(Resource.get(UI,"label.value"));
		headDeri.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headDValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headDeri.getStyleClass().add("table-head");
		headDValue.getStyleClass().add("table-head");
		
		for (final Attribute attr : Attribute.secondaryValues()) {
			Label value = new Label();
			
			values  .put(attr, value);

			value.getStyleClass().add("result");
		}
		
	}

	//-------------------------------------------------------------------
	private void doLayout() {
		super.setVgap(2);
		super.setHgap(0);
		super.setMaxWidth(Double.MAX_VALUE);

		this.add(headDeri   , 0,0);
		this.add(headDValue , 1,0);

		/*
		 * Derived attributes
		 */
		int y=0;
		for (final Attribute attr : Attribute.derivedValues()) {
			y++;
			Label longName  = new Label(attr.getName());
			Label finVal    = values.get(attr);
			
			String lineStyle = ((y%2)==1)?"even":"odd";
			GridPane.setVgrow(longName, Priority.ALWAYS);
			longName.getStyleClass().add(lineStyle);
			finVal.getStyleClass().addAll(lineStyle);
			longName.setUserData(attr);

			longName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			GridPane.setVgrow(longName, Priority.ALWAYS);
			finVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			finVal.setAlignment(Pos.CENTER);
			
			this.add(longName , 0, y);
			this.add(finVal   , 1, y);
		}
	}

	//-------------------------------------------------------------------
	public void updateController(CharacterGenerator charGen) {
		this.control = charGen;
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		for (final Attribute attr : Attribute.secondaryValues()) {
			Label final_l = values.get(attr);

			AttributeValue data = control.getCharacter().getAttribute(attr);
			final_l.setText(Integer.toString(data.getModifiedValue()));
		}
	}

}
