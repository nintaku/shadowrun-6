package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.SIN.Quality;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.SINController;
import org.prelle.shadowrun6.chargen.jfx.dialogs.EditSINDialog;
import org.prelle.shadowrun6.chargen.jfx.sections.SINSection;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class SINListCell extends ListCell<SIN> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SINSection.class.getName());
	
	private CharacterController control;
	private SINController sinCtrl;
	private ScreenManagerProvider provider;
	
	private Button btnEdit;
	private Label  lbName;
	private Label  lbDesc;
	private Label  lbQual;
	
	private HBox layout;

	//-------------------------------------------------------------------
	public SINListCell(CharacterController control, ScreenManagerProvider provider) {
		this.control = control;
		this.sinCtrl = control.getSINController();
		this.provider= provider;

		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbName = new Label();
		lbName.getStyleClass().add("base");
		lbDesc = new Label();
		lbDesc.setWrapText(true);
		lbQual = new Label();
		lbQual.setStyle("-fx-font-size: 200%");
		btnEdit = new Button(null, new SymbolIcon("edit"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox colNameDesc = new VBox(lbName, lbDesc);
		colNameDesc.setStyle("-fx-spacing: 0.2em");
		colNameDesc.setMaxWidth(Double.MAX_VALUE);

		this.layout = new HBox(colNameDesc, lbQual, btnEdit);
		this.layout.setAlignment(Pos.CENTER_LEFT);
		this.layout.setStyle("-fx-spacing: 2em");
		HBox.setHgrow(colNameDesc, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.setOnAction(ev -> {
			EditSINDialog dialog = new EditSINDialog(control, this.getItem(), true);
			provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
			getListView().refresh();
		});
		

	}

	//-------------------------------------------------------------------
	public void updateItem(SIN item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			btnEdit.setVisible(item.getQuality()!=Quality.REAL_SIN);
			
			lbName.setText(item.getName());
			if (item.getDescription()!=null && item.getDescription().length()>50)
				lbDesc.setText(item.getDescription().substring(0,50));
			else
				lbDesc.setText(item.getDescription());
			if (item.getQuality()==Quality.REAL_SIN) {
				lbQual.setText(RES.getString("label.quality.real"));
			} else
				lbQual.setText(String.valueOf(item.getQuality().getValue()));
		}
	}

}
