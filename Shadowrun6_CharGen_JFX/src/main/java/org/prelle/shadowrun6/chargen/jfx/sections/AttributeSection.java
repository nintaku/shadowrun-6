/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.panels.DerivedAttributeTable;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.AttributeTable;
import org.prelle.shadowrun6.levelling.CharacterLeveller;

import de.rpgframework.ConfigOption;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class AttributeSection extends SingleSection {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(AttributeSection.class.getName());

	private CharacterController control;
	private ViewMode mode;

	private AttributeTable table;
	private DerivedAttributeTable derived;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public AttributeSection(String title, CharacterController ctrl, ViewMode mode, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		control = ctrl;
		this.mode = mode;
		if (ctrl instanceof CharacterLeveller)
			setSettingsButton( new Button(null, new SymbolIcon("setting")) );

		initComponents();
		initLayoutNormal();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		table = new AttributeTable(control, mode);
		derived = new DerivedAttributeTable(control);
	}

	//-------------------------------------------------------------------
	private void initLayoutNormal() {
		HBox layout = new HBox(table, derived);
		layout.setStyle("-fx-spacing: 1em; -fx-pref-width: 40em");
		
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
//		table.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {if (n!=null) showHelpFor.set(n.getAttribute());});
		if (getSettingsButton()!=null)
			getSettingsButton().setOnAction(ev -> onSettings());
	}

	//-------------------------------------------------------------------
	public void refresh() {
		table.refresh();
		derived.refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	private void onSettings() {
		CharacterLeveller ctrl = (CharacterLeveller) control;
		
		VBox content = new VBox(20);
		for (ConfigOption<?> opt : ctrl.getAttributeController().getConfigOptions()) {
			CheckBox cb = new CheckBox(opt.getName());
			cb.setSelected((Boolean)opt.getValue());
			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
			content.getChildren().add(cb);
		}
		
		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
	}

}
