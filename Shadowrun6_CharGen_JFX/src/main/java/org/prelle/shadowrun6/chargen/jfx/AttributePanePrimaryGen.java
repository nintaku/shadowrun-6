/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.prelle.rpgframework.jfx.NumericalValueField;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.AttributeController;
import org.prelle.shadowrun6.charctrl.AttributeController.PerAttributePoints;
import org.prelle.shadowrun6.charctrl.PriorityAttributeController;
import org.prelle.shadowrun6.chargen.PrioritySettings;
import org.prelle.shadowrun6.gen.CharacterGenerator;

import de.rpgframework.genericrpg.NumericalValueController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.TextAlignment;

/**
 * @author prelle
 *
 */
public class AttributePanePrimaryGen extends GridPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(AttributePanePrimaryGen.class.getName());
	
	private CharacterGenerator  charGen;
	private ShadowrunCharacter    model;

	private Label headAttr, headAdjust, headPoints, headKarma, headValue, headMax;
	private Map<Attribute, NumericalValueField<Attribute, AttributeValue>> adjust;
	private Map<Attribute, NumericalValueField<Attribute, AttributeValue>> normal;
	private Map<Attribute, NumericalValueField<Attribute, AttributeValue>> karma;
	private Map<Attribute, Label> distributed;
	private Map<Attribute, Label> maxValue;
	
	//--------------------------------------------------------------------
	public AttributePanePrimaryGen(CharacterGenerator charGen) {
		if (charGen==null) throw new NullPointerException("Control not set");
		this.charGen = charGen;
		this.model   = charGen.getCharacter();

		adjust      = new HashMap<Attribute, NumericalValueField<Attribute, AttributeValue>>();
		normal      = new HashMap<Attribute, NumericalValueField<Attribute, AttributeValue>>();
		karma       = new HashMap<Attribute, NumericalValueField<Attribute, AttributeValue>>();
		distributed = new HashMap<Attribute, Label>();
		maxValue    = new HashMap<Attribute, Label>();
		
		doInit();
		doLayout();
		doInteractivity();
		
		refresh();
	}

	//-------------------------------------------------------------------
	private void doInit() {
		super.setVgap(2);
		super.setMaxWidth(Double.MAX_VALUE);
		
		headAttr   = new Label(Resource.get(UI,"label.attribute"));
		headAdjust = new Label(Resource.get(UI,"label.adjust"));
		headPoints = new Label(Resource.get(UI,"label.points"));
		headKarma  = new Label(Resource.get(UI,"label.karma"));
		headValue  = new Label(Resource.get(UI,"label.value"));
		headMax    = new Label(Resource.get(UI,"label.maximum"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headAdjust.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headPoints.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headMax.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headKarma.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr  .getStyleClass().add("table-head");
		headAdjust.getStyleClass().add("table-head");
		headPoints.getStyleClass().add("table-head");
		headKarma .getStyleClass().add("table-head");
		headValue .getStyleClass().add("table-head");
		headMax   .getStyleClass().add("table-head");
		
		headAdjust.setAlignment(Pos.CENTER);
		headPoints.setAlignment(Pos.CENTER);
		headKarma .setAlignment(Pos.CENTER);
		headValue.setAlignment(Pos.CENTER);
		headAdjust.setTextAlignment(TextAlignment.CENTER);
		headPoints.setTextAlignment(TextAlignment.CENTER);
		headKarma .setTextAlignment(TextAlignment.CENTER);
		
		PriorityAttributeController control = (PriorityAttributeController) charGen.getAttributeController();
		for (final Attribute attr : Attribute.primaryAndSpecialValues()) {
			if (attr==Attribute.ESSENCE)
				continue;
			// Adjustment points
			NumericalValueField<Attribute, AttributeValue> adjVal = new NumericalValueField<Attribute, AttributeValue>(charGen.getCharacter().getAttribute(attr) , new NumericalValueController<Attribute, AttributeValue>() {
				public boolean canBeIncreased(AttributeValue value) {  return control.canIncreaseAdjust(value.getAttribute());  }
				public boolean canBeDecreased(AttributeValue value) {  return control.canDecreaseAdjust(value.getAttribute());  }
				public boolean increase(AttributeValue value) {  return control.increaseAdjust(value.getAttribute());  }
				public boolean decrease(AttributeValue value) {  return control.decreaseAdjust(value.getAttribute());  }
			}, 5, true);
			adjVal.setValueCallback(aVal -> model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(aVal.getAttribute()).adjust);
			// Attribute points
			NumericalValueField<Attribute, AttributeValue> norVal = new NumericalValueField<Attribute, AttributeValue>(charGen.getCharacter().getAttribute(attr) , new NumericalValueController<Attribute, AttributeValue>() {
				public boolean canBeIncreased(AttributeValue value) {  return control.canIncreaseAttrib(value.getAttribute());  }
				public boolean canBeDecreased(AttributeValue value) {  return control.canDecreaseAttrib(value.getAttribute());  }
				public boolean increase(AttributeValue value) {  return control.increaseAttrib(value.getAttribute());  }
				public boolean decrease(AttributeValue value) {  return control.decreaseAttrib(value.getAttribute());  }
			}, 5, true);
			norVal.setValueCallback(aVal -> model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(aVal.getAttribute()).regular);
			// Karma
			NumericalValueField<Attribute, AttributeValue> karVal = new NumericalValueField<Attribute, AttributeValue>(charGen.getCharacter().getAttribute(attr) , new NumericalValueController<Attribute, AttributeValue>() {
				public boolean canBeIncreased(AttributeValue value) {  return control.canIncreaseKarma(value.getAttribute());  }
				public boolean canBeDecreased(AttributeValue value) {  return control.canDecreaseKarma(value.getAttribute());  }
				public boolean increase(AttributeValue value) {  return control.increaseKarma(value.getAttribute());  }
				public boolean decrease(AttributeValue value) {  return control.decreaseKarma(value.getAttribute());  }
			}, 10, true);
			karVal.setValueCallback(aVal -> model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(aVal.getAttribute()).karma);
			karVal.setConverter( aVal -> {
				PerAttributePoints per = model.getTemporaryChargenSettings(PrioritySettings.class).perAttrib.get(aVal.getAttribute());
				if (per.getKarmaInvest()==0)
					return "-";
				String ret = per.getSumBeforeKarma() + "\u2192"+per.getSum()+"("+per.getKarmaInvest()+")";
				return ret;
			});

			
			Label value= new Label();
			value.setAlignment(Pos.CENTER);
			value.getStyleClass().add("result");
			Label maxVal= new Label();
			maxVal.setAlignment(Pos.CENTER);
			maxVal.getStyleClass().add("result");
			
			normal     .put(attr, norVal);
			adjust     .put(attr, adjVal);
			karma      .put(attr, karVal);
			maxValue   .put(attr, maxVal);
			distributed.put(attr, value);
		}
	}

	//-------------------------------------------------------------------
	private void doLayout() {
		setVgap(10);
		this.add(headAttr  , 0,0);
		this.add(headAdjust, 1,0);
		this.add(headPoints, 2,0);
		this.add(headKarma , 3,0);
		this.add(headValue , 4,0);
		this.add(headMax   , 5,0);
		
		int y=0;
		for (final Attribute attr : Attribute.primaryAndSpecialValues()) {
			if (attr==Attribute.ESSENCE)
				continue;
			y++;
			Label longName  = new Label(attr.getName());
			NumericalValueField<Attribute, AttributeValue> adjVal = adjust.get(attr);
			NumericalValueField<Attribute, AttributeValue> norVal    = normal.get(attr);
			NumericalValueField<Attribute, AttributeValue> karVal    = karma.get(attr);
			Label points    = distributed.get(attr);
			Label maxVal    = maxValue.get(attr);
			
			String lineStyle = ((y%2)==1)?"even":"odd";
			GridPane.setVgrow(longName, Priority.ALWAYS);
			longName .getStyleClass().add(lineStyle);
			points.getStyleClass().add(lineStyle);
			maxVal.getStyleClass().add(lineStyle);
			adjVal.getStyleClass().add(lineStyle);
			norVal.getStyleClass().add(lineStyle);
			karVal.getStyleClass().add(lineStyle);
			
			longName.setUserData(attr);

			longName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			norVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			adjVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			points.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			karVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			maxVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(   longName, 0, y);
			this.add(     adjVal, 1, y);
			this.add(     norVal, 2, y);
			this.add(     karVal, 3, y);
			this.add(     points, 4, y);
			this.add(     maxVal, 5, y);
			
			GridPane.setMargin(adjVal, new Insets(0,10,0,10));
			GridPane.setMargin(norVal, new Insets(0,10,0,10));
			GridPane.setMargin(karVal, new Insets(0,10,0,10));
			GridPane.setMargin(points, new Insets(0,10,0,10));
			// Add extra line after CHARISMA
			if (attr==Attribute.CHARISMA)
				y++;
		}
	}

	//-------------------------------------------------------------------
	private void doInteractivity() {
//		for (final Attribute attr : Attribute.primaryAndSpecialValues()) {
//			if (attr==Attribute.ESSENCE)
//				continue;
//			NumericalValueField<Attribute, AttributeValue> field = distributed.get(attr);
//			field.inc.setOnAction(new EventHandler<ActionEvent>() {
//				public void handle(ActionEvent event) {
//					control.increase(attr);
//				}
//			});
//			field.dec.setOnAction(new EventHandler<ActionEvent>() {
//				public void handle(ActionEvent event) {
//					control.decrease(attr);
//				}
//			});
//		}
	}

	//-------------------------------------------------------------------
	public void updateCharacterGenerator(CharacterGenerator charGen) {
		this.charGen = charGen;
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		for (final Attribute attr : Attribute.primaryAndSpecialValues()) {
			if (attr==Attribute.ESSENCE)
				continue;
			NumericalValueField<Attribute, AttributeValue> adjVal  = adjust.get(attr);
			NumericalValueField<Attribute, AttributeValue> norVal  = normal.get(attr);
			NumericalValueField<Attribute, AttributeValue> karVal  = karma .get(attr);
			Label field = distributed.get(attr);
			Label max_l = maxValue.get(attr);
			
			adjVal.setVisible(charGen.getAttributeController().isRacialAttribute(attr)  || attr.isSpecial());
			norVal.setVisible(attr.isPrimary());

			PerAttributePoints perAttrib = charGen.getAttributeController().getPerAttribute(attr);
			adjVal.refresh();
			norVal.refresh();
			karVal.refresh();
			
			AttributeValue data = model.getAttribute(attr);
			if (LogManager.getLogger("shadowrun6.jfx").isTraceEnabled())
				LogManager.getLogger("shadowrun6.jfx").trace("data = "+data+" / "+data.getPoints()+" /"+data.getModifications()+" \t=== "+perAttrib);

//			field.setText(Integer.toString(data.getModifiedValue()));
			field.setText(String.valueOf(perAttrib.getSum()));
			max_l.setText(String.valueOf(data.getMaximum()));
			//			start_l.setText(Integer.toString(data.getStart()));
//			cost_l .setText(Integer.toString(control.getIncreaseCost(attr)));
//			field.inc.setDisable(!control.canBeIncreased(attr));
//			field.dec.setDisable(!control.canBeDecreased(attr));
		}
	}

}
