package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.dialogs.VehicleGeneratorDialog;
import org.prelle.shadowrun6.chargen.jfx.listcells.DesignOptionAndModListCell;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.vehicle.DesignOption;
import org.prelle.shadowrun6.vehiclegen.VehicleGenerator;

import de.rpgframework.ResourceI18N;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class DesignOptionSelector extends VBox implements IListSelector<BasePluginData> {
	
	private enum Type {
		DESIGN_OPTION,
		MOD_CHASSIS,
		MOD_SKIN,
		MOD_POWER,
		MOD_ELEC,
		MOD_HARD		
	}
	
	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	private static PropertyResourceBundle RES = VehicleGeneratorDialog.RES;

	private CharacterController charGen;
	private VehicleGenerator control;
	private NavigButtonControl btnControl;

	private ChoiceBox<Type> cbType;
	private ListView<BasePluginData> list;
	
	private Label lbHint;
	private int selectedLevel;
	private Button btnDec, btnInc;
	private Label lbLevel;

	//-------------------------------------------------------------------
	/**
	 */
	public DesignOptionSelector(CharacterController charGen, VehicleGenerator control) {
		this.charGen = charGen;
		this.control = control;
		btnControl   = new NavigButtonControl();
		btnControl.setDisabled(CloseType.OK, true);

		initComponents();
		initLayout();
		initInteractivity();

		cbType.getSelectionModel().select(0);
//		cbSubTypes.getSelectionModel().select(0);
		refreshOkayButton();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		cbType = new ChoiceBox<>();
		cbType.getItems().addAll(Type.values());
		cbType.setConverter(new StringConverter<DesignOptionSelector.Type>() {
			public String toString(Type val) {
				return ResourceI18N.get(RES, "selector.designoption."+val.name().toLowerCase());
			}
			
			public Type fromString(String string) {return null;}
		});
		list = new ListView<BasePluginData>();
		list.setStyle("-fx-pref-width: 26em");
		list.setCellFactory( lv -> new DesignOptionAndModListCell());
		
		lbHint  = new Label();
		lbLevel = new Label();
		btnDec = new Button("\uE0C6");
		btnDec.setStyle("-fx-font-family: 'Segoe UI Symbol'");
		btnInc = new Button("\uE0C5");
		btnInc.getStyleClass().add("symbol-icon");
		btnInc.setStyle("-fx-font-family: 'Segoe UI Symbol'");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		
		HBox line = new HBox(5, btnDec, lbLevel, btnInc);
		line.setMaxWidth(Double.MAX_VALUE);
		line.setAlignment(Pos.CENTER_RIGHT);
		
		setSpacing(10);
		getChildren().addAll(cbType, list, lbHint, line);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			list.getItems().clear();
			if (n==null) return;
			switch (n) {
			case DESIGN_OPTION:
				list.getItems().addAll(control.getAvailableDesignOptions());
				break;
			default:
				List<ItemTemplate> all = ShadowrunCore.getItems(ItemType.valueOf(n.name()));
				all = all.stream().filter(tmp -> tmp!=null && (tmp.getBestAccessoryUsage(ItemType.ACCESSORY)==null || tmp.getBestAccessoryUsage(ItemType.ACCESSORY).getCapacity()>0) && !tmp.getId().startsWith("modslot_")).collect(Collectors.toList());
				list.getItems().addAll(all);
				break;
			}
		});
		
		list.getSelectionModel().selectedItemProperty().addListener( (ovo,o,n) -> { selectedLevel=1; refreshOkayButton(); });
		
		btnDec.setOnAction(ev -> { selectedLevel--; refreshOkayButton(); });
		btnInc.setOnAction(ev -> { selectedLevel++; refreshOkayButton(); });
	}

	//-------------------------------------------------------------------
	private void refreshOkayButton() {
		BasePluginData selected = list.getSelectionModel().getSelectedItem();
		if (selected==null) {
			lbHint.setText("");
			btnControl.setDisabled(CloseType.OK, false);
			btnInc.setVisible(false);
			lbLevel.setVisible(false);
			btnDec.setVisible(false);
			return;
		}
		lbHint.setText(selected.getHintText());
		
		
		boolean canBeSelected = false;
		if (cbType.getValue()!=null) {
			switch (cbType.getValue()) {
			case DESIGN_OPTION:
				DesignOption dOpt = (DesignOption) selected;
				btnInc.setVisible(true);
				lbLevel.setVisible(true);
				lbLevel.setText(String.valueOf(selectedLevel));
				btnDec.setVisible(true);
				
				int maxInt = 1;
				try {
					maxInt = Integer.parseInt( dOpt.getMax() );
				} catch (NumberFormatException e) {
					switch (dOpt.getMax()) {
					case "DOUBLE_BODY":
						maxInt = control.getVehicle().getVehicleData().getBody()*2;
						break;
					case "150PERCENT":
						maxInt = Math.round(control.getVehicle().getVehicleData().getSeats()*1.5f);
						break;
					case "MAXSIZE":
						maxInt = 20 - control.getVehicle().getVehicleData().getSize();
						break;
					default:
						logger.error("Don't understand '"+dOpt.getMax()+"'");
					}
				}
				btnInc.setDisable(selectedLevel>=maxInt);
				btnDec.setDisable(selectedLevel<=-maxInt);
				
				canBeSelected = control.canBeSelected( dOpt, selectedLevel);
				break;
			default:
				ItemTemplate modif = (ItemTemplate)selected;
				if (modif.hasRating()) {
					btnInc.setVisible(true);
					lbLevel.setVisible(true);
					lbLevel.setText(String.valueOf(selectedLevel));
					btnDec.setVisible(true);
					
					maxInt = 1;
					maxInt = modif.getMaximumRating();
					btnInc.setDisable(selectedLevel>=maxInt);
					btnDec.setDisable(selectedLevel<=1);
				} else {
					btnInc.setVisible(false);
					lbLevel.setVisible(false);
					btnDec.setVisible(false);
				}
				canBeSelected = true;
			}
		}
		btnControl.setDisabled(CloseType.OK, 
				list.getSelectionModel().getSelectedItem()==null || 
				!canBeSelected);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<BasePluginData> getSelectionModel() {
		return list.getSelectionModel();
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	public boolean isDesignOption() {
		return list.getSelectionModel().getSelectedItem() instanceof DesignOption;
	}

	//-------------------------------------------------------------------
	public DesignOption getSelectedDesignOption() {
		return (DesignOption) list.getSelectionModel().getSelectedItem();
	}

	//-------------------------------------------------------------------
	public ItemTemplate getSelectedModification() {
		return (ItemTemplate) list.getSelectionModel().getSelectedItem();
	}

	//-------------------------------------------------------------------
	public int getSelectedLevel() {
		return selectedLevel;
	}

}
