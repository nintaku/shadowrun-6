package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.io.InputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MetaTypeOption;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class MetatypeListCell extends ListCell<MetaTypeOption> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".metatype");

	private HBox layout;
	private Label lblKarma;
	private ImageView ivMetatype;
	private Label lblName;

	//--------------------------------------------------------------------
	public MetatypeListCell() {
		lblKarma = new Label();
		lblKarma.getStyleClass().add("text-subheader");
		lblName  = new Label();
		lblName.getStyleClass().add("text-small-subheader");
		ivMetatype = new ImageView();
		ivMetatype.setFitHeight(64);
		ivMetatype.setFitWidth(64);

		layout = new HBox(5);
		layout.getChildren().addAll(ivMetatype, lblName, lblKarma);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		lblName.setMaxWidth(Double.MAX_VALUE);
		
		getStyleClass().add("metatype-cell");
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MetaTypeOption item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
			ivMetatype.setImage(null);
		} else {
			setGraphic(layout);
			lblName.setText(item.getType().getName());
			lblKarma.setText(""+item.getAdditionalKarmaKost());

			String metaID = (item.getType().getVariantOf()!=null)?item.getType().getVariantOf().getId():item.getType().getId();
			String fName = "images/metaicon_"+metaID+"_rike.png";
			InputStream in = SR6Constants.class.getResourceAsStream(fName);
			if (in==null) {
				logger.warn("Missing "+fName);
			} else {
				Image img = new Image(in);
				ivMetatype.setImage(img);
			}
		}
	}
}