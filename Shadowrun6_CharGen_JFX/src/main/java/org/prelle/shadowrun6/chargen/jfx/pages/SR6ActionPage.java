/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.ActionSection;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.CharacterHandle;

/**
 * @author prelle
 *
 */
public class SR6ActionPage extends SR6ManagedScreenPage {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6ActionPage.class.getName());

	private ActionSection actions;

	//---------------------------------------------------------
	public SR6ActionPage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		initEdgeActions();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initEdgeActions() {
		actions = new ActionSection(ResourceI18N.get(UI,"section.edge_actions"), control, provider);

		getSectionList().add(actions);

		// Interactivity
		actions.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//--------------------------------------------------------------------
	public void refresh() {
//		logger.debug("START refresh");
		super.refresh();
		this.setTitle(control.getCharacter().getName()+" - "+ResourceI18N.get(UI,"title.actions"));
	}

	//-------------------------------------------------------------------
	private void updateHelp(ShadowrunAction data) {
		logger.info("updateHelp: "+data);
		if (getManager()==null)
			setManager(provider.getManager());
		try { descrBtnEdit.setVisible(data!=null); } catch (Throwable e) {}
		this.helpData = data;
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

}
