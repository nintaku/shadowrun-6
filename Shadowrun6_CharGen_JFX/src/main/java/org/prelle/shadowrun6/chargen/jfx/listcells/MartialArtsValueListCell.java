package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.MartialArtsValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.dialogs.EditMartialArtsValueDialog;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class MartialArtsValueListCell extends ListCell<MartialArtsValue> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".martial");

	private final static String NORMAL_STYLE = "martialart-cell";

	private CharacterController control;
	private ScreenManagerProvider provider;

	private Label lineName;
	private Label lineDescription;
	private VBox  layout;
	private Button btnEdit;
	private HBox  outerLayout;

	//-------------------------------------------------------------------
	public MartialArtsValueListCell(CharacterController ctrl, ScreenManagerProvider provider) {
		this.control = ctrl;
		this.provider= provider;
		// Content
		lineName = new Label();
		lineName.setStyle("-fx-font-weight: bold;");
		lineDescription = new Label();
		lineDescription.setWrapText(true);
		layout   = new VBox(3);
		layout.getChildren().addAll(lineName, lineDescription);
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);

		// Buttons
		btnEdit   = new Button(null, new FontIcon("\uE17E\uE104"));
		btnEdit.getStyleClass().add("mini-button");
		btnEdit.setOnAction(event -> openEditDialog(getItem()));

		VBox bxButtons = new VBox();
		bxButtons.getChildren().addAll(btnEdit);
		bxButtons.setStyle("-fx-spacing: 1.0em; -fx-padding: 0.4em 0.55em 0em 0.55em;");
		
		outerLayout = new HBox(layout, bxButtons);
		HBox.setHgrow(layout, Priority.ALWAYS);			
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MartialArtsValue item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);			
		} else {
//			lblType.setText(String.valueOf(item.getCost()));
			lineName.setText(item.getMartialArt().getName());
			List<String> techniqueNames = control.getCharacter().getTechniques(item.getMartialArt()).stream().map(tech -> tech.getTechnique().getName()).collect(Collectors.toList());
			lineDescription.setText(String.join(",", techniqueNames));
//			imgRecommended.setVisible(control.isRecommended((MartialArts) item));
			setGraphic(outerLayout);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (getItem()==null)
			return;
		content.putString("martart:"+getItem().getMartialArt().getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

	//-------------------------------------------------------------------
	private void openEditDialog(MartialArtsValue data) {
		logger.debug("User clicked edit button");
		logger.debug("edit martial arts "+data);
		EditMartialArtsValueDialog dialog = new EditMartialArtsValueDialog(control, data, provider);
		CloseType result = (CloseType) provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());

		if (result==CloseType.OK) {
			this.requestLayout();
		}
	}

}