/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Technique;
import org.prelle.shadowrun6.TechniqueValue;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromTechniques implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc.power");
	
	//-------------------------------------------------------------------
	public GetModificationsFromTechniques() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Calculate effective modifications from available techniques
			for (TechniqueValue ref :model.getTechniquesAll()) {
				Technique techn = ref.getTechnique();
				logger.debug("  add from technique "+techn.getId()+" / "+ref);
				// Calculate modifications
				ref.clearModifications();
				for (Modification mod : ref.getTechnique().getModifications()) {
					Modification realMod = ShadowrunTools.instantiateModification(mod, ref.getChoice(), 0);
					logger.debug("  instantiated mod "+realMod);
					ref.addModification(realMod);
				}
				
				
				if (ref.getModifications()!=null && !ref.getModifications().isEmpty()) {
					logger.debug(" - "+ref.getTechnique().getId()+" has modifications: "+ref.getModifications());
					for (Modification mod : ref.getModifications()) {
						mod.setSource(ref.getTechnique());
					}
//					logger.debug(" - add modifications: "+ref.getModifications());
					unprocessed.addAll(ref.getModifications());
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
