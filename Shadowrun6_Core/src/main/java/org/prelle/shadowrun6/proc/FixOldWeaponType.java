/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemType;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class FixOldWeaponType implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc.items");
	
	//-------------------------------------------------------------------
	public FixOldWeaponType() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			List<String> names = new ArrayList<>();
			for (CarriedItem ref :model.getItemsAddedByUser()) {
				if (ref.getUsedAsType()==ItemType.WEAPON) {
					ref.setUsedAsType(ref.getItem().getUseAs().get(0).getType());
					names.add(ref.getName());
				}
			}
			if (!names.isEmpty()) {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 0, "I found some old versions of weapons in your character that had an deprecated item type and may have been invisible in the application.\n* "+String.join("\n* ", names)+"\nI fixed that. Please check your weapon list and remove items if nesessary.");
			}
			
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
