/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.SignatureManeuver;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ConnectSignatureManeuvers implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6");
	
	//-------------------------------------------------------------------
	public ConnectSignatureManeuvers() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		try {
			for (QualityValue qVal : model.getQualities()) {
				if (qVal.getChoice() instanceof SignatureManeuver) {
					SignatureManeuver sig = (SignatureManeuver)qVal.getChoice();
					sig.setQuality(qVal);
					logger.debug("Connect quality with signature maneuver "+sig.getName());
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+previous.size()+" modifications still to process");
		}
		return previous;
	}

}
