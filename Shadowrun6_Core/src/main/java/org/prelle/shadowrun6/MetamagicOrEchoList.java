/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="metaechoes")
@ElementList(entry="metaecho",type=MetamagicOrEcho.class,inline=true)
public class MetamagicOrEchoList extends ArrayList<MetamagicOrEcho> {

	private static final long serialVersionUID = -5648925612149808658L;

	//-------------------------------------------------------------------
	/**
	 */
	public MetamagicOrEchoList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public MetamagicOrEchoList(Collection<? extends MetamagicOrEcho> c) {
		super(c);
	}

}
