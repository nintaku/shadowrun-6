/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.UUID;

import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.persist.ActionConverter;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

/**
 * @author prelle
 *
 */
public class SignatureManeuver {
	
	@Attribute
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	
	@Attribute
	@AttribConvert(ActionConverter.class)
	private ShadowrunAction action1;
	@Attribute
	@AttribConvert(ActionConverter.class)
	private ShadowrunAction action2;
	
	@Element
	private UUID uuid;
	
	@Element
	private String name;
	
	private transient QualityValue quality;

	//---------------------------------------------------------
	public SignatureManeuver() {
		uuid = UUID.randomUUID();
	}

	//---------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	//---------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	//---------------------------------------------------------
	/**
	 * @return the action1
	 */
	public ShadowrunAction getAction1() {
		return action1;
	}

	//---------------------------------------------------------
	/**
	 * @param action1 the action1 to set
	 */
	public void setAction1(ShadowrunAction action1) {
		this.action1 = action1;
	}

	//---------------------------------------------------------
	/**
	 * @return the action2
	 */
	public ShadowrunAction getAction2() {
		return action2;
	}

	//---------------------------------------------------------
	/**
	 * @param action2 the action2 to set
	 */
	public void setAction2(ShadowrunAction action2) {
		this.action2 = action2;
	}

	//---------------------------------------------------------
	/**
	 * @return the uuid
	 */
	public UUID getUuid() {
		return uuid;
	}

	//---------------------------------------------------------
	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	//---------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//---------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//---------------------------------------------------------
	/**
	 * @return the quality
	 */
	public QualityValue getQuality() {
		return quality;
	}

	//---------------------------------------------------------
	/**
	 * @param quality the quality to set
	 */
	public void setQuality(QualityValue quality) {
		this.quality = quality;
	}

}
