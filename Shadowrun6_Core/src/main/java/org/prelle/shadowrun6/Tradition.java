/**
 * 
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.MissingResourceException;

import javax.naming.directory.ModificationItem;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

/**
 * @author prelle
 *
 */
public class Tradition extends BasePluginData implements Comparable<Tradition> {
	
	@Attribute
	private String id;
	@Attribute(name="drain1")
	private org.prelle.shadowrun6.Attribute drainAttribute1;
	@Attribute(name="drain2")
	private org.prelle.shadowrun6.Attribute drainAttribute2;
	@Element
	private ModificationItem modifications;

	//-------------------------------------------------------------------
	public Tradition() {
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null) 
			return id;
		try {
			return i18n.getString("tradition."+id);
		} catch (MissingResourceException e) {
			if (!reportedKeys.contains(e.getKey())) {
				reportedKeys.add(e.getKey());
				logger.warn(String.format("key missing:    %s   %s", i18n.getBaseBundleName(), e.getKey()));
				if (MISSING!=null) {
					MISSING.println(e.getKey()+"=");
				}
			}
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "tradition."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "tradition."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Tradition o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the drainAttribute1
	 */
	public org.prelle.shadowrun6.Attribute getDrainAttribute1() {
		return drainAttribute1;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the drainAttribute2
	 */
	public org.prelle.shadowrun6.Attribute getTraditionAttribute() {
		return drainAttribute2;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public ModificationItem getModifications() {
		return modifications;
	}

}
