package org.prelle.shadowrun6.modifications;

import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.persist.AttackRatingConverter;
import org.prelle.shadowrun6.persist.ItemAttributeObjectConverter;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ItemAttributeModification extends ModificationBase<ItemAttribute> implements HasOptionalCondition {

	@Attribute
    private ItemAttribute attr;
	@org.prelle.simplepersist.Attribute
	private ModificationType modType;
	@Attribute
    private int val;
	@Attribute(name="objVal")
	@AttribConvert(ItemAttributeObjectConverter.class)
    private Object objectValue;
	@Attribute(name="cond",required=false)
    private Boolean conditional = null;
	@Attribute(name="condindex",required=false)
    private int conditionIndex = 1;
	/**
	 * Item type to limit this modification
	 */
	@Attribute(name="type",required=false)
    private ItemType type;
	@Attribute(name="subtype",required=false)
    private ItemSubType subtype;
	@Attribute
    private String limitToItem;
	/**
	 * Skill an item must use when this skill shall apply
	 */
	@Attribute(name="skill",required=false)
	@AttribConvert(SkillConverter.class)
    private Skill itemSkill;
	/**
	 * Item hook to limit this modification
	 */
	@Attribute(name="slot",required=false)
    private ItemHook slot;
	@org.prelle.simplepersist.Attribute(name="ar",required=false)
	@AttribConvert(value=AttackRatingConverter.class)
	private int[] attackRating;

	private transient Boolean included = null;
	   
    //-----------------------------------------------------------------------
    public ItemAttributeModification() {
    	modType = ModificationType.RELATIVE;
    }

    //-----------------------------------------------------------------------
    public ItemAttributeModification(ItemAttribute attr, int val) {
        this.attr = attr;
        this.val  = val;
    	modType = ModificationType.RELATIVE;
    }

    //-----------------------------------------------------------------------
    public ItemAttributeModification(ItemAttribute attr, Object objVal) {
        this.attr = attr;
        this.objectValue  = objVal;
    	modType = ModificationType.RELATIVE;
    }

//    //-------------------------------------------------------------------
//    /**
//     * @see org.prelle.shadowrun6.modifications.ModificationBase#clone()
//     */
//    @Override
//	public ValueModification<ItemAttribute> clone() {
//   		return new ItemAttributeModification(attr, val);
//     }

	//-------------------------------------------------------------------
 	public ItemAttributeModification clone() {
//    	try {
    		return (ItemAttributeModification) super.clone();
//    	} catch ( CloneNotSupportedException e ) {
//    		throw new InternalError();
//    	}
    }

    //-----------------------------------------------------------------------
    public String toString() {
    	String effect = attr+"/"+((val<0)?(" "+val):(" +"+val));
    	if (modType==ModificationType.MULTIPLY)
    		effect = attr+"/*"+((val<0)?(" "+val):(" +"+val));
    	if (objectValue!=null) {
    		effect = attr+"+"+objectValue;
        	if (modType==ModificationType.MULTIPLY)
        		effect = attr+"*"+objectValue;
   		if (objectValue.getClass()==int[].class) {
        		try { effect = attr+"/"+(new AttackRatingConverter()).write((int[]) objectValue); } catch (Exception e) { e.printStackTrace(); }
    		}
    	}
    	
    	if (conditional!=null && conditional)
    		effect+=" (cond.)";
    	if (limitToItem!=null)
    		effect +=" limit to "+limitToItem;
    	if (type!=null)
    		effect += " type="+type;
    	if (subtype!=null)
    		effect += " subtype="+subtype;
    	if (included!=null && included)
    		effect += " already included";
   		effect += " from "+source;
    	
    	return effect;
    }

    //-----------------------------------------------------------------------
    public ItemAttribute getAttribute() {
        return attr;
    }

    //-----------------------------------------------------------------------
    public void setAttribute(ItemAttribute attr) {
        this.attr = attr;
    }

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
	 */
	@Override
	public ItemAttribute getModifiedItem() {
		return attr;
	}

    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }

    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }

//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new AttributeModification(type, attr, val);
//    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof ItemAttributeModification) {
            ItemAttributeModification amod = (ItemAttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return (amod.getValue()==val);
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof ItemAttributeModification) {
            ItemAttributeModification amod = (ItemAttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof ItemAttributeModification))
            return toString().compareTo(obj.toString());
        ItemAttributeModification other = (ItemAttributeModification)obj;
        if (attr!=other.getAttribute())
            return (Integer.valueOf(attr.ordinal())).compareTo(Integer.valueOf(other.getAttribute().ordinal()));
        return 0;
    }

	//--------------------------------------------------------------------
	/**
	 * @return the conditional
	 */
	public boolean isConditional() {
		if (conditional==null) return false;
		return conditional;
	}

	//--------------------------------------------------------------------
	/**
	 * @param conditional the conditional to set
	 */
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the included
	 */
	public boolean isIncluded() {
		return (included!=null)?included:false;
	}

	//-------------------------------------------------------------------
	/**
	 * @param included the included to set
	 */
	public void setIncluded(Boolean included) {
		this.included = included;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ItemType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(ItemType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the subtype
	 */
	public ItemSubType getSubtype() {
		return subtype;
	}

	//-------------------------------------------------------------------
	/**
	 * @param subtype the subtype to set
	 */
	public void setSubtype(ItemSubType subtype) {
		this.subtype = subtype;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemHook getSlot() {
		return slot;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the objectValue
	 */
	public Object getObjectValue() {
		return objectValue;
	}

	//-------------------------------------------------------------------
	public void setObjectValue(Object val) {
		objectValue = val;;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.HasOptionalCondition#hasCondition()
	 */
	@Override
	public boolean hasCondition() {
		if (conditional==null)
			return false;
		return conditional;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.HasOptionalCondition#setCondition(boolean)
	 */
	@Override
	public void setCondition(boolean val) {
		this.conditional = val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.HasOptionalCondition#getConditionIndex()
	 */
	@Override
	public int getConditionIndex() {
		return conditionIndex;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modType
	 */
	public ModificationType getModificationType() {
		return modType;
	}

	//-------------------------------------------------------------------
	/**
	 * @param modType the modType to set
	 */
	public void setModificationType(ModificationType modType) {
		this.modType = modType;
	}

}// ItemAttributeModification
