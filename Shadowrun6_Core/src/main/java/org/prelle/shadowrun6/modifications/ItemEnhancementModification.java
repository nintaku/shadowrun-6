package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.persist.ItemEnhancementConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="moditemmod")
public class ItemEnhancementModification implements Modification {

	@Attribute(name="ref")
	@AttribConvert(ItemEnhancementConverter.class)
    private ItemEnhancement itemMod;
	private transient Object source;
//	@Attribute
//    private Boolean included;

    //-----------------------------------------------------------------------
    public ItemEnhancementModification() {
    }

    //-----------------------------------------------------------------------
    public ItemEnhancementModification(ItemEnhancement item, Object source) {
        this.itemMod = item;
        this.source = source;
    }

    //-----------------------------------------------------------------------
    public String toString() {
           return "Add mod "+itemMod.getId();
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof ItemEnhancementModification) {
            ItemEnhancementModification amod = (ItemEnhancementModification)o;
             return (amod.getItemEnhancement()==itemMod);
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof ItemEnhancementModification))
            return toString().compareTo(obj.toString());
        ItemEnhancementModification other = (ItemEnhancementModification)obj;
        return itemMod.compareTo(other.getItemEnhancement());
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#clone()
	 */
	@Override
	public Modification clone() {
		return new ItemEnhancementModification(itemMod, source);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the item
	 */
	public ItemEnhancement getItemEnhancement() {
		return itemMod;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @return the included
//	 */
//	public boolean isIncluded() {
//		return (included!=null)?included:false;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @param included the included to set
//	 */
//	public void setIncluded(Boolean included) {
//		this.included = included;
//	}

}
