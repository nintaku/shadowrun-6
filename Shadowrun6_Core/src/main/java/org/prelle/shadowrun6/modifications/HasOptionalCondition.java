package org.prelle.shadowrun6.modifications;

/**
 * @author prelle
 *
 */
public interface HasOptionalCondition {

	public boolean hasCondition();
	public void setCondition(boolean val);
	
	public int getConditionIndex();
		
}
