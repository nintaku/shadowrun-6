/**
 * 
 */
package org.prelle.shadowrun6;

import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.persist.MartialArtsConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name = "martialartval")
public class MartialArtsValue extends ModifyableImpl implements Modifyable, Comparable<MartialArtsValue> {
	
	@Attribute(name="ref")
	@AttribConvert(MartialArtsConverter.class)
	private MartialArts martialArt;

	//-------------------------------------------------------------------
	public MartialArtsValue() {
		modifications = new ModificationList();
	}

	//-------------------------------------------------------------------
	public MartialArtsValue(MartialArts learnedIn) {
		this();
		this.martialArt = learnedIn;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return (martialArt!=null)?martialArt.getName():"?";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the martialArt
	 */
	public MartialArts getMartialArt() {
		return martialArt;
	}

	//-------------------------------------------------------------------
	/**
	 * @param martialArt the martialArt to set
	 */
	public void setMartialArt(MartialArts martialArt) {
		this.martialArt = martialArt;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MartialArtsValue other) {
		return martialArt.compareTo(other.martialArt);
	}

}
