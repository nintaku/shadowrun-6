/**
 * 
 */
package org.prelle.shadowrun6;

/**
 * @author prelle
 *
 */
public enum Sense {

	VISION,
	HEARING,
	TASTE,
	TOUCH,
	SMELL,
	ASTRAL_VISION,
	;
	
	public String getName() {
        return Resource.get(ShadowrunCore.getI18nResources(), "sense."+this.name().toLowerCase());
	}
}
