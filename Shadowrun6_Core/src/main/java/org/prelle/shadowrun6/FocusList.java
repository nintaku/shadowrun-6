/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="foci")
@ElementList(entry="focus",type=Focus.class,inline=true)
public class FocusList extends ArrayList<Focus> {

	private static final long serialVersionUID = 8014713686299120291L;

	//-------------------------------------------------------------------
	/**
	 */
	public FocusList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public FocusList(Collection<? extends Focus> c) {
		super(c);
	}

}
