/**
 * 
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.MissingResourceException;

import org.prelle.shadowrun6.requirements.RequirementList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

/**
 * @author prelle
 *
 */
public class LicenseType extends BasePluginData implements Comparable<LicenseType> {

	@Attribute(required=true)
	private String id;
	@Element(name="requires")
	private RequirementList requirements;

	//-------------------------------------------------------------------
	public LicenseType() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return null;
		try {
			return i18n.getString("licensetype."+id);
		} catch (MissingResourceException e) {
			logger.warn("Missing "+e.getKey()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"=");
			return e.getKey();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "licensetype."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "licensetype."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(LicenseType o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the requirements
	 */
	public RequirementList getRequirements() {
		return requirements;
	}

	//-------------------------------------------------------------------
	/**
	 * @param requirements the requirements to set
	 */
	public void setRequirements(RequirementList requirements) {
		this.requirements = requirements;
	}

}
