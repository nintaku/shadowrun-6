/**
 * 
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.MissingResourceException;

import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.persist.TechniqueCategoryConverter;
import org.prelle.shadowrun6.requirements.RequirementList;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class Technique extends BasePluginData implements Comparable<Technique> {

	public enum Category {
		GENERAL,
		GRAPPLING,
		MOBILITY,
		RANGED,
		STRIKING,
		WEAPON,
		;

		public String getName() {
			return ResourceI18N.get(ShadowrunCore.getI18nResources(),"technique.category."+name().toLowerCase());
		}
	}
	
	@Attribute
	private String id;
	@Attribute(name="cat")
	@AttribConvert(TechniqueCategoryConverter.class)
	private List<Category> categories;
	@Element(name="requires")
	private RequirementList requirements;
	@Attribute
	private ChoiceType choice;
	@Element(name="modifications")
	protected ModificationList modifications;

	//-------------------------------------------------------------------
	public Technique() {
		modifications = new ModificationList();
		requirements = new RequirementList();
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
    	if (id==null)
    		return "Tradition(id=null)";
    	String key = "technique."+id.toLowerCase()+".title";
    	if (i18n==null)
    		return key;
        try {
			return i18n.getString(key);
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18n.getBaseBundleName());
			if (MISSING!=null) 
				MISSING.println(mre.getKey()+"=");
		}
        return key;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "technique."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "technique."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Technique o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public Collection<Category> getCategories() {
		return new ArrayList<Technique.Category>(categories);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the requirements
	 */
	public RequirementList getRequirements() {
		return requirements;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public ChoiceType getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	public List<Modification> getModifications() {
		return new ArrayList<Modification>(modifications);
	}

}
