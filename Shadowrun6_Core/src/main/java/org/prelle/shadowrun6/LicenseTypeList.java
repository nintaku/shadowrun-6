/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="licensetypes")
@ElementList(entry="licensetype",type=LicenseType.class)
public class LicenseTypeList extends ArrayList<LicenseType> {

}
