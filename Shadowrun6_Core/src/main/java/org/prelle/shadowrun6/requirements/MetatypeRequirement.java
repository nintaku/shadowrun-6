package org.prelle.shadowrun6.requirements;

import org.prelle.shadowrun6.MetaType;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "metareq")	
public class MetatypeRequirement extends Requirement {

	@org.prelle.simplepersist.Attribute
//	@AttribConvert(MetaTypeConverter.class)
//    private MetaType ref;
	private String ref;
  
    //-----------------------------------------------------------------------
    public MetatypeRequirement() {
    }
    
    //-----------------------------------------------------------------------
    public MetatypeRequirement(MetaType attr) {
        this.ref = attr.getId();
     }
    
    //-----------------------------------------------------------------------
    public MetatypeRequirement(String attr) {
        this.ref = attr;
     }
    
    //-----------------------------------------------------------------------
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
        return ref+"";
    }
    
    //-----------------------------------------------------------------------
    public String getMetaTypeId() {
        return ref;
    }
 
    //-----------------------------------------------------------------------
    public MetaType getMetaType() {
        return ShadowrunCore.getMetaType(ref);
    }
    
    //-----------------------------------------------------------------------
    public void setMetaType(MetaType attr) {
        this.ref = attr.getId();
    }
     
    //-----------------------------------------------------------------------
    public Object clone() {
        return new MetatypeRequirement(ref);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof MetatypeRequirement) {
            MetatypeRequirement amod = (MetatypeRequirement)o;
            return (amod.getMetaTypeId()==ref);
        } else
            return false;
    }
   
}
