/**
 * 
 */
package org.prelle.shadowrun6.requirements;

import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.persist.CapacityConverter;
import org.prelle.shadowrun6.persist.ItemHookConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="slotreq")
public class ItemHookRequirement extends Requirement {
	
	@Attribute(required=true)
	@AttribConvert(ItemHookConverter.class)
	private ItemHook slot;
	@Attribute
	@AttribConvert(CapacityConverter.class)
	private float capacity;
	
	//-------------------------------------------------------------------
	public ItemHookRequirement() {
	}
	
	//-------------------------------------------------------------------
	public ItemHookRequirement(ItemHook hook, float capa) {
		this.slot = hook;
		this.capacity = capa;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "requires "+slot+" with capacity "+capacity;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemHook getSlot() {
		return slot;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the capacity
	 */
	public float getCapacity() {
		return capacity;
	}

	//-------------------------------------------------------------------
	/**
	 * @param slot the slot to set
	 */
	public void setSlot(ItemHook slot) {
		this.slot = slot;
	}

	//-------------------------------------------------------------------
	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(float capacity) {
		this.capacity = capacity;
	}

}
