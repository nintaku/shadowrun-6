package org.prelle.shadowrun6.requirements;

import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "magresreq")	
public class MagicOrResonanceRequirement extends Requirement {

	@org.prelle.simplepersist.Attribute
	private String type;
 
    //-----------------------------------------------------------------------
    public MagicOrResonanceRequirement() {
    }
    
    //-----------------------------------------------------------------------
    public MagicOrResonanceRequirement(MagicOrResonanceType attr) {
        this.type = attr.getId();
     }
    
    //-----------------------------------------------------------------------
    public MagicOrResonanceRequirement(String ref) {
        this.type = ref;
     }
    
    //-----------------------------------------------------------------------
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
        return type+"";
    }

    //-----------------------------------------------------------------------
    public MagicOrResonanceType getMagicOrResonanceType() {
        return ShadowrunCore.getMagicOrResonanceType(type);
    }

    //-----------------------------------------------------------------------
    public String getMagicOrResonanceTypeID() {
        return type;
    }
    
    //-----------------------------------------------------------------------
    public void setMagicOrResonanceType(MagicOrResonanceType attr) {
        this.type = attr.getId();
    }
     
    //-----------------------------------------------------------------------
    public Object clone() {
        return new MagicOrResonanceRequirement(type);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof MagicOrResonanceRequirement) {
            MagicOrResonanceRequirement amod = (MagicOrResonanceRequirement)o;
            return (amod.getMagicOrResonanceTypeID().equals(type));
        } else
            return false;
    }
   
}
