package org.prelle.shadowrun6.requirements;

import org.prelle.shadowrun6.Technique;
import org.prelle.shadowrun6.persist.TechniqueConverter;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "qualreq")
public class TechniqueRequirement extends Requirement {

	@Attribute(name="ref")
	private String id;
    private transient Technique resolved;
   
    //-----------------------------------------------------------------------
    public TechniqueRequirement() {
    }
    
    //-----------------------------------------------------------------------
    public TechniqueRequirement(Technique attr) {
        this.resolved = attr;
        this.id  = attr.getId();
     }
    
    //-----------------------------------------------------------------------
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
        return id;
    }
 
    //-----------------------------------------------------------------------
    public Technique getTechnique() {
    	if (resolved!=null)
    		return resolved;

    	if (resolve())
    		return resolved;
    	return null;
    }
    
    //-----------------------------------------------------------------------
    public void setTechnique(Technique attr) {
        this.resolved = attr;
    }
     
    //-----------------------------------------------------------------------
    public Object clone() {
        return new TechniqueRequirement(resolved);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof TechniqueRequirement) {
            TechniqueRequirement amod = (TechniqueRequirement)o;
            return (amod.getTechnique()==resolved);
        } else
            return false;
    }

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.requirements.Requirement#resolve()
	 */
	@Override
	public boolean resolve() {
		if (resolved!=null)
			return true;
		if (id==null)
			return false;

		try {
			resolved = (new TechniqueConverter()).read(id);
			return true;
		} catch (Exception e) {
			logger.error(e);
		}

		return false;
	}
 
}
