/**
 * 
 */
package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.Focus;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class FocusConverter implements StringValueConverter<Focus> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Focus value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Focus read(String idref) throws Exception {
		Focus data = ShadowrunCore.getFocus(idref);
		if (data==null)
			throw new ReferenceException(ReferenceType.FOCUS, idref);

		return data;
	}

}
