package org.prelle.shadowrun6.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.LicenseType;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

public class LicenseTypeConverter implements StringValueConverter<LicenseType> {
	
	private final static Logger logger = LogManager.getLogger("shadowrun6.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public LicenseType read(String v) throws Exception {
		LicenseType item = ShadowrunCore.getLicenseType(v);
		if (item==null) {
			logger.error("Unknown license type: '"+v+"'");
			throw new ReferenceException(ReferenceType.LICENSE_TYPE, v);
		}
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(LicenseType v) throws Exception {
		return v.getId();
	}
	
}