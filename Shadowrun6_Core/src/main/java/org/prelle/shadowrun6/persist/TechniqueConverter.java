/**
 * 
 */
package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.Technique;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class TechniqueConverter implements StringValueConverter<Technique> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Technique value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Technique read(String idref) throws Exception {
		Technique skill = ShadowrunCore.getTechnique(idref);
		if (skill==null)
			throw new ReferenceException(ReferenceType.TECHNIQUE, idref);

		return skill;
	}

}
