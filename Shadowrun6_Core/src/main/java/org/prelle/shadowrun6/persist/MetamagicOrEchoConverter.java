/**
 * 
 */
package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.MetamagicOrEcho;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class MetamagicOrEchoConverter implements StringValueConverter<MetamagicOrEcho> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(MetamagicOrEcho value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public MetamagicOrEcho read(String idref) throws Exception {
		MetamagicOrEcho data = ShadowrunCore.getMetamagicOrEcho(idref);
		if (data==null)
			throw new ReferenceException(ReferenceType.METAMAGIC_ECHO, idref);

		return data;
	}

}
