package org.prelle.shadowrun6.persist;

import java.util.Arrays;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

/**
 * @author Stefan Prelle
 *
 */
public class PriceTableConverter implements XMLElementConverter<int[]> {

	@Override
	public void write(XmlNode node, int[] value) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int[] read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		XMLEvent nextEv = evRd.nextEvent();
		if (!nextEv.isCharacters()) {
			throw new IllegalArgumentException("Expect CHARACTERS after "+ev+", but got "+nextEv.getEventType());
		}

		String[] splitted = nextEv.asCharacters().getData().split(",");
		int[] ret = new int[splitted.length];
		for (int i=0; i<splitted.length; i++) {
			ret[i] = Integer.parseInt(splitted[i]);
		}
		return ret;
	}

}
