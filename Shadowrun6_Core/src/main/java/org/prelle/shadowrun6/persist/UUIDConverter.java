/**
 * 
 */
package org.prelle.shadowrun6.persist;

import java.util.UUID;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author Stefan
 *
 */
public class UUIDConverter implements StringValueConverter<UUID> {

	//--------------------------------------------------------------------
	/**
	 */
	public UUIDConverter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String write(UUID value) throws Exception {
		return value.toString();
	}

	@Override
	public UUID read(String v) throws Exception {
		return UUID.fromString(v);
	}

}
