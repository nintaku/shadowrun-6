package org.prelle.shadowrun6.vehicle;

import java.text.Collator;
import java.util.MissingResourceException;

import org.prelle.shadowrun6.BasePluginData;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class QualityFactor extends BasePluginData implements Comparable<QualityFactor> {

	@Attribute(required=true)
	private String id;
	@Attribute
	private float multi;

	//-------------------------------------------------------------------
	public QualityFactor() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QUALITYF:"+id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return null;
		try {
			return i18n.getString("qualityfactor."+id);
		} catch (MissingResourceException mre) {
			if (!reportedKeys.contains(mre.getKey())) {
				reportedKeys.add(mre.getKey());
				logger.error("Missing property '"+mre.getKey()+"' in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(mre.getKey()+"=");
			}
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "qualityfactor."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "qualityfactor."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(QualityFactor other) {
		if (getName()==null) return 0;
		if (other.getName()==null) return 0;
		
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//-------------------------------------------------------------------
	public float getMultiplier() {
		return multi;
	}

}
