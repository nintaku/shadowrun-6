/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.items.CarriedItem;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ResetItemModifications implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.items.proc");

	//-------------------------------------------------------------------
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> previous) {
		String prefix = indent+model.getItem().getId()+": ";
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace(prefix+"START: process");
		try {
			// Set calculations on item back to zero
			model.clear();
			model.getSlots().forEach(slot -> slot.clearBonusCapacity());
		} finally {
			logger.trace(prefix+"STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}
	
}
