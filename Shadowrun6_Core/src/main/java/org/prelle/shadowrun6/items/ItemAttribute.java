/**
 *
 */
package org.prelle.shadowrun6.items;

import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;

/**
 * @author Stefan
 *
 */
public enum ItemAttribute {

	ACCELERATION,
	AMMUNITION,
	ARMOR,
	ATTACK_RATING,
	AVAILABILITY,
	BODY,
	CAPACITY,
	CONCEALABILITY,
	DAMAGE,
	DAMAGE_REDUCTION,
	DEFENSE_RATING,
	DEVICE_RATING,
	ESSENCECOST,
	HANDLING,
	HAS_RATING,
	MODE,
	/** Maximum rating of embedded items */
	MAX_SENSOR_RATING,
	MODIFICATION_SLOTS,
	PILOT,
	PRICE,
	QUALITY,
	SEATS,
	SENSORS,
	SKILL,
	SKILL_SPECIALIZATION,
	SOCIAL,
	SPEED_INTERVAL,
	SPEED,
	SIZE,
	CARGO,

	ATTACK,
	SLEAZE,
	DATA_PROCESSING,
	FIREWALL,
	CONCURRENT_PROGRAMS,
	;

	//--------------------------------------------------------------------
	public String getName() {
		return Resource.get(ShadowrunCore.getI18nResources(),"itemattribute."+ItemAttribute.this.name().toLowerCase());
	}

	//--------------------------------------------------------------------
	public String getShortName() {
		return Resource.get(ShadowrunCore.getI18nResources(),"itemattribute."+ItemAttribute.this.name().toLowerCase()+".short");
	}

}
