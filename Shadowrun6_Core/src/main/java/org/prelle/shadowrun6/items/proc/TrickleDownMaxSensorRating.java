/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemAttributeNumericalValue;
import org.prelle.shadowrun6.items.ItemAttributeValue;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class TrickleDownMaxSensorRating implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	/**
	 */
	public TrickleDownMaxSensorRating() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(java.lang.String, org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> unprocessed) {
		String prefix = indent+model.getItem().getId()+": ";
		logger.debug("check "+model);
		if (model.hasAttribute(ItemAttribute.MAX_SENSOR_RATING)) {
			ItemAttributeNumericalValue value = model.getAsValue(ItemAttribute.MAX_SENSOR_RATING);
			/*
			 * Walk through all accessories
			 */
			for (CarriedItem tmp : model.getUserAddedAccessories()) {
				logger.info(prefix+"Copy "+value.getModifyable()+" to "+tmp.getItem().getId());
//				tmp.setAttribute(value);
				tmp.addAutoModification(new ItemAttributeModification(ItemAttribute.MAX_SENSOR_RATING, value.getModifiedValue()));
			}
		}

		return unprocessed;
	}

}
