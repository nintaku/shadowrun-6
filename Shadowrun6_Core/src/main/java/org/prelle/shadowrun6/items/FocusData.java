/**
 *
 */
package org.prelle.shadowrun6.items;

import org.prelle.shadowrun6.ChoiceType;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class FocusData {

	@Attribute(name="karma")
	private int karmaMultiplier;
	@Attribute(name="choice")
	private ChoiceType choice;

	//-------------------------------------------------------------------
	/**
	 */
	public FocusData() {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the karmaMultiplier
	 */
	public int getKarmaMultiplier() {
		return karmaMultiplier;
	}

	//-------------------------------------------------------------------
	/**
	 * @param karmaMultiplier the karmaMultiplier to set
	 */
	public void setKarmaMultiplier(int karmaMultiplier) {
		this.karmaMultiplier = karmaMultiplier;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public ChoiceType getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(ChoiceType choice) {
		this.choice = choice;
	}

}
