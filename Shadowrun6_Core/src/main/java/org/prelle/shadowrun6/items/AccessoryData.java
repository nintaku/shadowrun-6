/**
 *
 */
package org.prelle.shadowrun6.items;

import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author Stefan
 *
 */
@Root(name="accessory")
public class AccessoryData {

	/**
	 * For which items is the accessory usable
	 */
	@Attribute
	private List<ItemTemplate> usewith;
	@Attribute(name="size")
	private int capacitySize;

	//--------------------------------------------------------------------
	public AccessoryData() {
//		usewith  = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "AccessoryData(capSize="+capacitySize+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the capacitySize
	 */
	public int getCapacitySize() {
		return capacitySize;
	}

	//-------------------------------------------------------------------
	/**
	 * @param capacitySize the capacitySize to set
	 */
	public void setCapacitySize(int capacitySize) {
		this.capacitySize = capacitySize;
	}

}
