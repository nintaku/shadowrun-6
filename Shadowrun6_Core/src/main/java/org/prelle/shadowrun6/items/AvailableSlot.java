package org.prelle.shadowrun6.items;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author prelle
 *
 */
public class AvailableSlot {

	@Attribute
	private ItemHook ref;
	@Attribute
	private float capacity;
	
	private transient List<CarriedItem> embedded;
	private transient float bonusCap;
//	private transient List<CarriedItem> resetProofEmbedded;
	
	//-------------------------------------------------------------------
	public AvailableSlot() {
		embedded = new ArrayList<CarriedItem>();
//		resetProofEmbedded = new ArrayList<CarriedItem>();
	}
	
	//-------------------------------------------------------------------
	public AvailableSlot(ItemHook hook) {
		this();
		this.ref = hook;
		if (hook.hasCapacity)
			throw new IllegalArgumentException("Hook has capacity - use other constructor");
	}
	
	//-------------------------------------------------------------------
	public AvailableSlot(ItemHook hook, float capacity) {
		this();
		this.ref = hook;
		this.capacity = capacity;
		if (capacity<0)
			throw new IllegalArgumentException("Negative capacities are not allowed");
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getAllEmbeddedItems() {
		return new ArrayList<>(embedded);
	}

	//-------------------------------------------------------------------
	public String toString() {
		return ref+"(cap="+capacity+", items="+embedded+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @param accessory the accessory to set
	 */
	public boolean addEmbeddedItem(CarriedItem accessory, boolean ignoreCapacity, CarriedItem container) {
		if (embedded.contains(accessory) && !accessory.getItem().isMultipleUsably())
			return true;
		float cap = 1;
		if (ref.hasCapacity)
			cap = getCapacity();
		if (!ignoreCapacity && capacity>0 && getUsedCapacity()>cap) {
			LogManager.getLogger("shadowrun6.items.proc").error("Used cap = "+getUsedCapacity());
			LogManager.getLogger("shadowrun6.items.proc").error("Cannot add any more items. Already have "+embedded.size()+" of "+capacity+"/"+ref+" : "+embedded);
			List<String> names = embedded.stream().map(item -> item.getName()).collect(Collectors.toList());
			LogManager.getLogger("shadowrun6.items.proc").error( "Your character file is bugged. There is a piece of gear ("+container.getName()+") with a maximum capacity of "+cap+" and the following contents:\n"+String.join("\n", names)+" \nwhere the accessory '"+accessory.getName()+"' should be added, what would exceed the capacity. \nIt will be removed.");
//			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 0, "Your character file is bugged. There is a piece of gear ("+container.getName()+") with a maximum capacity of "+cap+" and the following contents:\n"+String.join("\n", names)+" \nwhere the accessory '"+accessory.getName()+"' should be added, what would exceed the capacity.");
			//return false;
//			throw new IllegalStateException("Cannot add any more items. Already have "+embedded.size()+" of "+capacity+"/"+ref+" : "+embedded);
		}
		
		embedded.add(accessory);
//		if (!resetProofEmbedded.contains(accessory) && !accessory.isCreatedByModification())
//			resetProofEmbedded.add(accessory);
		return true;
	}

	//-------------------------------------------------------------------
	public boolean removeEmbeddedItem(CarriedItem accessory) {
//		resetProofEmbedded.remove(accessory);
		return embedded.remove(accessory);		
	}

	//-------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemHook getSlot() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the capacity
	 */
	public float getCapacity() {
		return capacity + bonusCap;
	}

	//-------------------------------------------------------------------
	public void clearBonusCapacity() {
		bonusCap = 0;
	}
	
	//-------------------------------------------------------------------
	public void addBonusCapacity(float cap) {
		this.bonusCap+=cap;
	}

	//-------------------------------------------------------------------
	public float getUsedCapacity() {
		float sum = 0;
		for (CarriedItem item : embedded) {
			if (item.isIgnoredForCalculations())
				continue;
			float val = item.getAsFloat(ItemAttribute.CAPACITY).getModifiedValue();
			sum+= val;
		}
		return sum;
	}

	//-------------------------------------------------------------------
	public float getFreeCapacity() {
		return getCapacity() - getUsedCapacity();
	}

	//-------------------------------------------------------------------
	public void clear() {
		embedded.clear();
//		embedded.addAll(resetProofEmbedded);
	}

}
