package org.prelle.shadowrun6.items;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun6.persist.ItemEnhancementConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

public class ItemEnhancementValue extends ModifyableImpl {

	@Attribute
	@AttribConvert(ItemEnhancementConverter.class)
	private ItemEnhancement ref;
	private transient boolean autoAdded;

	//--------------------------------------------------------------------
	public ItemEnhancementValue() {
	}

	//--------------------------------------------------------------------
	public ItemEnhancementValue(ItemEnhancement ref) {
		this.ref = ref;
	}

	//--------------------------------------------------------------------
	public ItemEnhancementValue(ItemEnhancement ref, boolean auto) {
		this.ref = ref;
		this.autoAdded = auto;
	}

	//--------------------------------------------------------------------
	public String toString() {
		return String.valueOf(ref);
	}

	//--------------------------------------------------------------------
	public ItemEnhancement getModifyable() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifyableImpl#getModifications()
	 */
	@Override
	public List<Modification> getModifications() {
		if (ref==null) return new ArrayList<Modification>();
		return ref.getModifications();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the autoAdded
	 */
	public boolean isAutoAdded() {
		return autoAdded;
	}

}
