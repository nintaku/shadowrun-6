/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CapacityDefinitions;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.UseAs;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ApplyUserAddedAccessories implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> previous) {
		String prefix = indent+model.getItem().getId()+": ";

		try {
			for (CarriedItem accessory : new ArrayList<>(model.getUserAddedAccessories())) {
				ItemHook hook = accessory.getSlot();
				if (hook==null) {
					model.addToDo(new ToDoElement(Severity.WARNING, Resource.format(
							ShadowrunCore.getI18nResources(), 
							"todos.carrieditem.accessory_without_slot", 
							accessory.getName())));
				} else {
					AvailableSlot slot = model.getSlot(hook);
					if (slot==null) {
						if (hook==ItemHook.FIREARMS_EXTERNAL) {
							
						} else {
							// Assigned to non-existing slot
							logger.debug(prefix+accessory.getItem().getId()+" is assigned to non-existing slot "+accessory.getSlot());
							model.addToDo(new ToDoElement(Severity.WARNING, Resource.format(
									ShadowrunCore.getI18nResources(), 
									"todos.carrieditem.accessory_in_invalid_slot", 
									accessory.getName(), hook.getName())));
						}
					} else {
						// Slot exists
						logger.info("Assign user-accessory "+accessory.getItem().getId()+" to slot "+hook+" of "+model.getName()+": "+slot+"\n\n");
						accessory.refreshVirtual();
						UseAs usage = accessory.getItem().getUseAs(accessory);
						if (usage!=null && usage.getCapacity()>980) {
							float cap = usage.getCapacity();
							switch ( (int)usage.getCapacity()) {
							case (int)CapacityDefinitions.ARMOR_DIV_4:
								cap = Math.round(model.getAsValue(ItemAttribute.ARMOR).getModifiedValue()/4);
								accessory.setAttributeOverride(ItemAttribute.CAPACITY, cap);
								logger.info("Set capacity requirement of "+accessory.getItem().getId()+" to ARMOR/4 = "+cap);
								break;
							case (int)CapacityDefinitions.BODY_DIV_2:
								cap = Math.round(model.getAsValue(ItemAttribute.BODY).getModifiedValue()/2);
								accessory.setAttributeOverride(ItemAttribute.CAPACITY, cap);
								logger.info("Set capacity requirement of "+accessory.getItem().getId()+" to BODY/2 = "+cap);
								break;
							case (int)CapacityDefinitions.BODY_DIV_3:
								cap = Math.round(model.getAsValue(ItemAttribute.BODY).getModifiedValue()/3);
								accessory.setAttributeOverride(ItemAttribute.CAPACITY, cap);
								logger.info("Set capacity requirement of "+accessory.getItem().getId()+" to BODY/3 = "+cap);
								break;
							case (int)CapacityDefinitions.RATING_MINUS_1:
								cap = Math.max(model.getRating()-1,0);
								accessory.setAttributeOverride(ItemAttribute.CAPACITY, cap);
								logger.info("Set capacity requirement of "+accessory.getItem().getId()+" to RATING-1 = "+cap);
								break;
							}
						}
						
						if (model.getUniqueId()!=null)
							accessory.setEmbeddedIn(model.getUniqueId());
						if (!slot.addEmbeddedItem(accessory, false, model)) {
							model.removeAccessory(accessory);
						}
					}						
				}
			}
		} finally {
			
		}
		return previous;
	}

}
