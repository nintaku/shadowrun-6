/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.items.CarriedItem;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ItemRecalculation {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");
	
	static CarriedItemProcessor[] STEPS = new CarriedItemProcessor[] {
			new ResetItemModifications(),
			new CopyTemplateData(),
			new ApplyOverwrittenAttributes(),
			new ApplyItemEnhancementModifications(),
			new GetModificationsFromEnhancements(),
			new ApplyItemHookModifications(),
			new ApplyAccessoryModification(),
			new ApplyItemHookModifications(),
			new ApplyUserAddedAccessories(),
			new GetModificationsFromAccessories(),
			new ApplyItemAttributeModification(),
			new VerifyAugmentationGrades(),
			new TrickleDownMaxSensorRating(),
	};

	//--------------------------------------------------------------------
	public static List<Modification> recalculate(String indent, CarriedItem item) {
		String prefix = indent+item.getItem().getId()+": ";
		logger.debug(prefix+"START:----------------"+item);
		List<Modification> unprocessed = new ArrayList<>();
		for (CarriedItemProcessor step : STEPS) {
			logger.trace(prefix+"  run "+step.getClass().getSimpleName());
			try {
				unprocessed = step.process(indent, item, unprocessed);
			} catch (Exception e) {
				logger.error("Error calculating "+item.getItem(),e);
			}
			logger.debug(prefix+"  after "+step.getClass().getSimpleName()+" = "+unprocessed);
		}
		logger.debug(prefix+"  unprocessed = "+unprocessed);
//		logger.debug(prefix+item.dump());
		unprocessed.forEach(mod -> item.addModification(mod));
		logger.debug(prefix+"STOP :----------------"+item);

		return unprocessed;
	}

}
