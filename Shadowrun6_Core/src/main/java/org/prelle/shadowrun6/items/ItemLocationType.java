package org.prelle.shadowrun6.items;

import org.prelle.shadowrun6.ShadowrunCore;

/**
 * Created by rupp on 28.09.2014.
 */
public enum ItemLocationType {
    BODY,
    CONTAINER,
    SOMEWHEREELSE;

    public String getName() {
        return ShadowrunCore.getI18nResources().getString("itemlocationtype."+name().toLowerCase());
    }
}
