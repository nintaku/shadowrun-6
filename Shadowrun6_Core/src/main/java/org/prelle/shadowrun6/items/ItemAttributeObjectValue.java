package org.prelle.shadowrun6.items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.modifications.ModificationBase.ModificationType;

import de.rpgframework.genericrpg.modification.Modification;

public class ItemAttributeObjectValue extends ItemAttributeValue  {

	/** Item instance this object belongs to */
	protected CarriedItem model;
	protected Object value;

	//--------------------------------------------------------------------
	public ItemAttributeObjectValue(ItemAttribute attr, Object val, List<Modification> mods, CarriedItem model) {
		super(attr, mods);
		value = val;
		this.model = model;
	}

	//--------------------------------------------------------------------
	public ItemAttributeObjectValue(ItemAttribute attr, Object val, CarriedItem model) {
		super(attr, new ArrayList<>());
		value = val;
		this.model = model;
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (value instanceof int[])
			return Arrays.toString((int[])value)+" ("+modifications+")";
		return value+" ("+modifications+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return
	 */
	public Object getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	public void setValue(Object value) {
		this.value = value;
	}

	//-------------------------------------------------------------------
	public boolean isModified() {
		if (attribute==ItemAttribute.ATTACK_RATING)
			return isModifiedAttackRating( (int[])value);
		if (attribute==ItemAttribute.HANDLING)
			return isModifiedOnRoadOffRoad( (OnRoadOffRoadValue)value);
		if (attribute==ItemAttribute.ACCELERATION)
			return isModifiedOnRoadOffRoad( (OnRoadOffRoadValue)value);
		if (attribute==ItemAttribute.SPEED_INTERVAL)
			return isModifiedOnRoadOffRoad( (OnRoadOffRoadValue)value);
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return
	 */
	public Object getModifiedValue() {
		if (attribute==ItemAttribute.ATTACK_RATING)
			return getModifiedAttackRating( (int[])value);
		if (attribute==ItemAttribute.HANDLING)
			return getModifiedOnRoadOffRoad( (OnRoadOffRoadValue)value);
		if (attribute==ItemAttribute.ACCELERATION)
			return getModifiedOnRoadOffRoad( (OnRoadOffRoadValue)value);
		if (attribute==ItemAttribute.SPEED_INTERVAL)
			return getModifiedOnRoadOffRoad( (OnRoadOffRoadValue)value);
		return value;
	}

	//-------------------------------------------------------------------
	private int[] getModifiedAttackRating(int[] oldAR) {
		if (model.getItem().getId().equals("ares_light_fire_75")) {
			System.err.println("ItemAttributeObjectValue.getModifiedAttackRating");
		}
		
		int[] newAR = new int[5];
		for (int p=0; p<oldAR.length; p++) {
			newAR[p] = oldAR[p];
		}
		// Now apply modifications
		for (Modification tmp : getModifications()) {
			ItemAttributeModification iMod = (ItemAttributeModification)tmp;
			if (iMod.isConditional() && !model.assumesCondition(iMod))
				continue;
			if (iMod.isIncluded())
				continue;
			if (iMod.getObjectValue()!=null && iMod.getObjectValue().getClass()==int[].class) {
				int[] modAR = (int[]) iMod.getObjectValue();
				for (int i=0; i<newAR.length && i<modAR.length; i++) {
					if (newAR[i]>0) {
						newAR[i] += modAR[i];
					}
				}
			} else if (iMod.getValue()>0) {
				// Apply one value to all distances
				for (int i=0; i<newAR.length && i<newAR.length; i++) {
					if (newAR[i]>0) {
						newAR[i] += iMod.getValue();
					}
				}
			}
		}
		return newAR;
	}

	//-------------------------------------------------------------------
	private OnRoadOffRoadValue getModifiedOnRoadOffRoad(OnRoadOffRoadValue oldAR) {
		OnRoadOffRoadValue newAR = new OnRoadOffRoadValue();
		newAR.set(oldAR.getOnRoad(), oldAR.getOffRoad());
		// Now apply modifications
		for (Modification tmp : getModifications()) {
			ItemAttributeModification iMod = (ItemAttributeModification)tmp;
			if (iMod.isConditional() && !model.assumesCondition(iMod))
				continue;
			if (iMod.isIncluded())
				continue;
			if (iMod.getObjectValue()!=null && iMod.getObjectValue().getClass()==OnRoadOffRoadValue.class) {
				OnRoadOffRoadValue modAR = (OnRoadOffRoadValue)iMod.getObjectValue();
				if (iMod.getModificationType()==ModificationType.RELATIVE) {
					newAR.set(
							newAR.getOnRoad()+modAR.getOnRoad(), 
							newAR.getOffRoad()+modAR.getOffRoad()
							);
				} else if (iMod.getModificationType()==ModificationType.MULTIPLY) {
					newAR.set(
							((OnRoadOffRoadValue)value).getOnRoad() + (float)Math.ceil(((OnRoadOffRoadValue)value).getOnRoad()*modAR.getOnRoadFloat()), 
							((OnRoadOffRoadValue)value).getOffRoad() + (float)Math.ceil(((OnRoadOffRoadValue)value).getOffRoad()*modAR.getOffRoadFloat())
							);
				}
			}
		}
		return newAR;
	}

	//-------------------------------------------------------------------
	private boolean isModifiedAttackRating(int[] oldAR) {
		int[] newAR = new int[5];
		for (int p=0; p<oldAR.length; p++) {
			newAR[p] = oldAR[p];
		}
		// Now apply modifications
		for (Modification tmp : getModifications()) {
			ItemAttributeModification iMod = (ItemAttributeModification)tmp;
			if (iMod.isConditional() && !model.assumesCondition(iMod))
				continue;
			if (iMod.isIncluded())
				continue;
			if (iMod.getObjectValue()!=null && iMod.getObjectValue().getClass()==int[].class) {
				int[] modAR = (int[]) iMod.getObjectValue();
				for (int i=0; i<newAR.length && i<modAR.length; i++) {
					if (newAR[i]>0) {
						return true;
					}
				}
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	private boolean isModifiedOnRoadOffRoad(OnRoadOffRoadValue oldAR) {
		OnRoadOffRoadValue newAR = new OnRoadOffRoadValue();
		newAR.set(oldAR.getOnRoad(), oldAR.getOffRoad());
		// Now apply modifications
		for (Modification tmp : getModifications()) {
			ItemAttributeModification iMod = (ItemAttributeModification)tmp;
			if (iMod.isConditional() && !model.assumesCondition(iMod))
				continue;
			if (iMod.isIncluded())
				continue;
			if (iMod.getObjectValue()!=null && iMod.getObjectValue().getClass()==OnRoadOffRoadValue.class) {
				OnRoadOffRoadValue modAR = (OnRoadOffRoadValue)iMod.getObjectValue();
//				newAR.set(
//						newAR.getOnRoad()+modAR.getOnRoad(), 
//						newAR.getOffRoad()+modAR.getOffRoad());
//				int[] modAR = (int[]) iMod.getObjectValue();
//				for (int i=0; i<newAR.length && i<modAR.length; i++) {
//					if (newAR[i]>0) {
//						return true;
//					}
//				}
				return true;
			}
		}
		return false;
	}

}
