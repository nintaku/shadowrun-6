/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class GetModificationsFromAccessories implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	/**
	 */
	public GetModificationsFromAccessories() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(java.lang.String, org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> unprocessed) {
		String prefix = indent+model.getItem().getId()+": ";
		
		for (CarriedItem accessory : model.getEffectiveAccessories()) {
			List<Modification> mods = accessory.getModifications();
			if (mods.isEmpty()) {
				logger.debug(prefix+"No modifications from "+accessory.getItem().getId());
			} else {
				logger.info(prefix+"Modifications from "+accessory.getItem().getId()+" = "+mods);
				// For an empty skill modification, apply choice
				for (Modification mod : mods) {
					if (accessory.getChoice()!=null) {
						Modification newMod = ShadowrunTools.instantiateModification(mod, accessory.getChoice(), accessory.getRating());
						unprocessed.add(newMod);
					} else if (mod instanceof ItemAttributeModification) {
						ItemAttributeModification aMod = (ItemAttributeModification)mod;
						if (aMod.getAttribute()!=ItemAttribute.CAPACITY) {
							logger.debug("------------Apply "+mod+"---------------");
							if (model.hasAttribute(aMod.getAttribute())) {
								logger.debug("Apply here: "+aMod);
								model.getAttribute(aMod.getAttribute()).addModification(aMod);
							} else {
								logger.warn("Don't know how to apply: "+aMod);
								unprocessed.add(mod);
							}
						} else
							unprocessed.add(mod);
					} else {
						unprocessed.add(mod);
					}
				}
			}
		}
		return unprocessed;
	}

}
