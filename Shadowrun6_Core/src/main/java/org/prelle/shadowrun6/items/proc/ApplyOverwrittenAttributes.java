/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemAttributeFloatValue;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ApplyOverwrittenAttributes implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	/**
	 */
	public ApplyOverwrittenAttributes() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> unprocessed) {
		String prefix = indent+model.getItem().getId()+": ";
		for (Entry<ItemAttribute, Object> entry : model.overwrittenBaseAttributes()) {
			switch (entry.getKey()) {
			case ATTACK_RATING: 
				model.setAttribute(entry.getKey(), entry.getValue());
				logger.info(prefix+"overwrite "+entry.getKey()+" with "+Arrays.toString((int[])entry.getValue()));
				break;
			case CAPACITY: 
				logger.info(prefix+"overwrite "+entry.getKey()+" with "+((Float)entry.getValue()));
				model.setAttribute(new ItemAttributeFloatValue(entry.getKey(), (Float)entry.getValue()));
				break;
			default:
				logger.warn(prefix+"Don't know how to override "+entry.getKey());
				System.err.println("ApplyOverwrittenAttributes: Don't know how to override "+entry.getKey());
			}
		}

		return unprocessed;
	}

}
