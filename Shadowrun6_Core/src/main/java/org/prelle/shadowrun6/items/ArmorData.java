/**
 *
 */
package org.prelle.shadowrun6.items;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class ArmorData {

	@Attribute(name="rating")
	private int rating;
	@Attribute
	private int social;
	@Attribute(name="dr")
	private int damageReduction;
	@Attribute(name="add")
	private boolean addsToMain;

	//-------------------------------------------------------------------
	/**
	 */
	public ArmorData() {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public int getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the addsToMain
	 */
	public boolean addsToMain() {
		return addsToMain;
	}

	//-------------------------------------------------------------------
	/**
	 * @param addsToMain the addsToMain to set
	 */
	public void setAddsToMain(boolean addsToMain) {
		this.addsToMain = addsToMain;
	}

	//-------------------------------------------------------------------
	public int getSocial() {
		return social;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the damageReduction
	 */
	public int getDamageReduction() {
		return damageReduction;
	}

}
