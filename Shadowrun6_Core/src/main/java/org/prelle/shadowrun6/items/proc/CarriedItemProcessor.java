/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.List;

import org.prelle.shadowrun6.items.CarriedItem;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public interface CarriedItemProcessor {

	/**
	 * @param unprocessed Unprocessed modifications from previous steps
	 * @return Unprocessed modification
	 */
	public List<Modification> process(String indent, CarriedItem model, List<Modification> unprocessed);
	
}
