/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.UUID;

import org.prelle.shadowrun6.persist.LicenseTypeConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class LicenseValue {
	
	@Attribute(name="sin")
	private UUID sin;
	@Attribute
	@AttribConvert(LicenseTypeConverter.class)
	private LicenseType type;
	@Attribute(required=true)
	private SIN.Quality rating;

	//-------------------------------------------------------------------
	public LicenseValue() {
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "LicenseValue("+type+", "+rating+")";
	}

	//-------------------------------------------------------------------
	public String getName() {
		return type.getName()+" "+rating.getValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sin
	 */
	public UUID getSIN() {
		return sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sin the sin to set
	 */
	public void setSIN(UUID sin) {
		this.sin = sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public SIN.Quality getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rating the rating to set
	 */
	public void setRating(SIN.Quality rating) {
		this.rating = rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public LicenseType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(LicenseType type) {
		this.type = type;
	}

}
