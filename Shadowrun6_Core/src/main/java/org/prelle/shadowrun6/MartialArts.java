/**
 * 
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.MissingResourceException;

import org.prelle.shadowrun6.Technique.Category;
import org.prelle.shadowrun6.persist.TechniqueCategoryConverter;
import org.prelle.shadowrun6.persist.TechniqueConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class MartialArts extends BasePluginData implements Comparable<MartialArts> {
	
	@Attribute
	private String id;
	@Attribute(name="cat")
	@AttribConvert(TechniqueCategoryConverter.class)
	private List<Category> categories;
	@Attribute(name="sign")
	@AttribConvert(TechniqueConverter.class)
	private Technique signatureTechnique;

	//-------------------------------------------------------------------
	public MartialArts() {
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
    	if (id==null)
    		return "MartialArtsStyle(id=null)";
    	String key = "martialart."+id.toLowerCase()+".title";
    	if (i18n==null)
    		return key;
        try {
			return i18n.getString(key);
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18n.getBaseBundleName());
			if (MISSING!=null) 
				MISSING.println(mre.getKey()+"=");
		}
        return key;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "martialart."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "martialart."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MartialArts o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public Collection<Category> getCategories() {
		return new ArrayList<Technique.Category>(categories);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the signatureTechnique
	 */
	public Technique getSignatureTechnique() {
		return signatureTechnique;
	}

	//-------------------------------------------------------------------
	/**
	 * @param signatureTechnique the signatureTechnique to set
	 */
	public void setSignatureTechnique(Technique signatureTechnique) {
		this.signatureTechnique = signatureTechnique;
	}

}
