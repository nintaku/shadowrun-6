package de.rpgframework.shadowrun6.print.json.model;

public class JSONContact {
    public String name;
    public String type;
    public int loyalty;
    public int influence;
    public String description;
    public int favors;
}
