package de.rpgframework.shadowrun6.print.json.model;

public class JSONMetaMagicOrEcho {
    public String name;
    public String page;
    public String description;
}
