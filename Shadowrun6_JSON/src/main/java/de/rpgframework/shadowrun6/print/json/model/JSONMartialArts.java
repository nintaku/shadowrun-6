package de.rpgframework.shadowrun6.print.json.model;

import java.util.List;

public class JSONMartialArts {
    public String name;
    public List<String> techniques;
    public String page;
    public String description;
}
