package de.rpgframework.shadowrun6.print.json.model;

import java.util.List;

public class JSONArmor {
    public String name;
    public int rating;
    public int socialrating;
    public List<JSONItemAccessory> accessories;
    public boolean isIgnored;
    public String page;
    public String description;
    public boolean primary;
}
