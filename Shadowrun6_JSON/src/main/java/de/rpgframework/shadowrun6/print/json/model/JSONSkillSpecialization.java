package de.rpgframework.shadowrun6.print.json.model;

public class JSONSkillSpecialization {
    public String name;
    public String id;
    public boolean expertise;
    public String attribute;
    public int pool;
    public String description;
}
