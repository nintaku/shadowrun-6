package de.rpgframework.shadowrun6.print.json.model;

public class JSONLicense {
    public String name;
    public String sin;
    public String type;
    public String rating;
}
