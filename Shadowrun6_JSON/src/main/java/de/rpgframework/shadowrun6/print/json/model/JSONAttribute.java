package de.rpgframework.shadowrun6.print.json.model;

public class JSONAttribute {
    public String name;
    public String id;
    public int points;
    public int modifiedValue;
}
