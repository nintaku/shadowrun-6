package de.rpgframework.shadowrun6.print.json.model;

public class JSONAugmentation {
    public String name;
    public String level;
    public float essence;
    public String page;
    public String quality;
    public String description;
}
