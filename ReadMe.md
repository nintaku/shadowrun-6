This is the source code and the XML data for the *Shadowrun 6* plugin frm the **RPGFramework** project.

This plugin is a fan project, which is included in the character generation software **Genesis**.
We welcome anyone who is willing to [support us](https://rpgframework.atlassian.net/wiki/spaces/GD/pages/307167443/Helping+the+project)- as a developer or data editor.

With the exception of the data taken from the Shadowrun publications, this is an open source project. You may use the program code or product data for your own open source projects. It may be possible to use the XML data from the publications as well for your open source project, but you are responsible to prevent copyright violations, which is why we recommend that you contact us first. 
